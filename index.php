<?php
// Preparación ////////////////////////////

// Autorizaciones
//include_once "app/lib/mx_/mx_Autorizaciones.php";

// iniciar sesión
//session_set_cookie_params(3600,"/",false,true);



$starttime = microtime(true);
if(file_exists('_access.log')){
	$access_log=array();
	$access_log[]=date("Y-m-d H:i:s");
	$access_log[]=$_SERVER['HTTP_X_FORWARDED_FOR'];
	$access_log[]=$_SERVER['REMOTE_ADDR'];
	$access_log[]=$_SERVER['REQUEST_URI'];
	$access_log[]=session_id();
	$endtime = microtime(true);
	//$timediff = $endtime - $starttime;
	//$access_log[]=$timediff;
	file_put_contents('_access.log',"\n".implode("\t",$access_log), FILE_APPEND);
}
// Control number of requests per second
/*$timeCurrent = strtotime(date('Y-m-d H:i:s'));
$differenceInSeconds = 0;
	
if(isset($_SESSION['last_request'])){
	$timeFirst  = strtotime($_SESSION['last_request']);
	$differenceInSeconds = $timeCurrent - $timeFirst;
	
}
$_SESSION['last_request']=$timeCurrent;
*/

// cargar librerías
require_once "lib/index.php";

// cargar configuración
require_once "cnf/index.php";

// cargar template
require_once "template/index.php";

// connect to db
$db=new php_mysqli($dbconn);

if(isset($_REQUEST['_s'])){
	@session_id($_REQUEST['_s']);
}

session_start();


// load login
global $user;
$user = new php_AccessControl();

if(!$user->is_logged()){
	if($_COOKIE['user']&&$_COOKIE['token']){
		$user->login($_COOKIE['user'],$_COOKIE['token']);
	}
}

trace("Enter_page");

// Nombres de Clase ///////////////////

// c = clase
if(file_exists("app/Main/index.php")){
	$c = "Main";
}else{
	die("No main Proc found");
	//$c = "Instalar";
}
if($_GET['c']){
	$c = str_replace(" ","_",ucwords(str_replace("_"," ",$_GET['c'])));
}

// m = método
$m = "index";
if($_GET['m']){
	$m = $_GET['m'];
}
// hacking
if(!preg_match('/^[a-z_0-9]*$/i',$m)){
	trace("Anti-hack: ".$m."\n".var_export($_SERVER,true));
	@mail("guillermo.morales@foxconn.com","INTENTO HACKEO","Se intentó hacer hack:".$m."\n".var_export($_SERVER,true));
	die("El nombre de el método es inválido.");
}




// Ejecución de clase ///////////////////////

// consolidación de parámetros
$p =  array();

// parámetros GET
foreach($_GET as $k=>$v){
	if(is_array($v)){
		foreach($v as &$sa){
			$sa=addslashes($sa);		
		}
		$p[$k]=$v;
	}else{	
		$p[$k]=addslashes($v);	
	}
}

// parámetros POST
foreach($_POST as $k=>$v){
	if(is_array($v)){
		foreach($v as &$sa){
			$sa=addslashes($sa);		
		}
		$p[$k]=$v;
	}else{	
		$p[$k]=addslashes($v);	
	}
}

// parámetros de SESSION
if(isset($_SESSION['mx_Controller'])){
	if(isset($_SESSION['mx_Controller']['parameters'])){
		trace("Extracting mx_Controller::parameters: \n".var_export($_SESSION['mx_Controller']['parameters'],true));
		foreach($_SESSION['mx_Controller']['parameters'] as $k=>$v){
			$p[$k]=$v;	
		}
	}		
}

// Authentication

// Bins requires outside authentication
if(php_Auth::is_proxy(php_Auth::$DOIT_WEBPAGE_PROXY)){
	if(strcasecmp(trim($c),"Bins")==0){
		if(!php_Auth::is_logged()){
			if(!$p['requested_m']){
				$p['requested_m']=$m;
			}
			if(!$p['requested_c']){
				$p['requested_c']=$c;
				$m="login";
			}
			
		}
	}else{
		
	}	
}



// TODO: Config protect
if(strcasecmp($c,'config')==0){
	
	if(isset($_SERVER['HTTP_X_FORWARDED_HOST'])){
		//$c="Main";
	}
}
// instanciar la clase
if(class_exists($c)){
	$controlador = new $c($p);
}else{
	trace("Class ".$c." does not exist");
	
	if(count($db->describe($c))){
		// Instantiate default controller
		$controlador = new php_Controller($p);
		trace("Instantiated default controller.");
	}
}
// template
if($_SESSION['template']){
	//$controlador->set_tipo($_SESSION['template']);	
}

// hacking
if(!preg_match('/^[a-z_0-9]*$/i',$_GET['e'])){
	trace("Anti-hack: ".$m."\n".var_export($_SERVER,true));
	@mail("guillermo.morales@foxconn.com","INTENTO HACKEO","Se intentó hacer hack:".$m."\n".var_export($_SERVER,true));
	die("El nombre de el método es inválido.");
}
$e = $_GET['e'];

if($e){
//	$controlador->set_tipo('export');
//	$controlador->set_pages(array($e));
}


if(method_exists($controlador,$m)){
	if($controlador->has_access){
		// trace
		trace("Call_method ".$c."->".$m."(".json_encode($p).")");
		// ejecutar el método y obtener el resultado
		$r = $controlador->{$m}($p);
		$title=$controlador->title;
		if(!$title){
			$title=ucwords(str_replace("_"," ",$m))." - ".ucwords(str_replace("_"," ",$c));
		}
	}else{
		$r=array();
		$title="Access denied";
	}
}else{
	trace("No method '".$m."' exists in '".$c."', calling view");
	$r=array("controller_method"=>"No method '".$m."' exists in '".$c."', calling view");
}



// determinar el manejo de resultado

if(is_string($r)){
	// si el resultado es string, entonces es una redirección
	if(strpos($r,"?")===false){
		$ajax="?mx_ajax_enabled=".$p['mx_ajax_enabled'];
	}else{
		$ajax="&mx_ajax_enabled=".$p['mx_ajax_enabled'];
	}
	header("Location: ".$r);//.$ajax);
	trace("Redirection ".$r);
	die();
}/*else if($controlador->redirected_to()!=""){
	// si se redireccionó, entonces redireccionar
	if(strpos($controlador->redirected_to(),"?")===false){
		$ajax="?mx_ajax_enabled=".$p['mx_ajax_enabled'];
	}else{
		$ajax="&mx_ajax_enabled=".$p['mx_ajax_enabled'];
	}
	header("Location: ".$controlador->redirected_to().$ajax);
	trace("Redirection ".$controlador->redirected_to());
	die();
}*/



// extraer variables de return
if(is_array($r)){
	// si es array extraer variables por ejemplo $d['mis_datos'] -> $mis_datos
	//foreach($r as $k=>$v){
	//	${$k} = $v;
	//}
	$data = array("result"=>$r);
	unset($r);
	extract($data,EXTR_OVERWRITE);	
}

// destruir sesion de parametros
	unset($_SESSION['mx_Controller']);
trace("Removed session variable mx_Controller");
	
// de otra manera, mostrar el contenido

// cargar scripts html
//include "app/html/index.php";

// incluir archivos
$vista_default="app/_default/views/".$m.".php";
$metodo = $m;
$args = $p;

trace("Starting_view ".$c."->".$m."(".json_encode($p).")");

if(!isset($_GET['_data'])){
	if($controlador->template){
		require("template/".$controlador->template."/header.php");
	}else{
		require("template/header.php");
	}
}
if(isset($_GET['_data'])){
	if($controlador->has_access){		
		$view=$_GET['v'];
		if(preg_match('/^[a-z_0-9]*$/i',$view)){
			$view=$_GET['_data'];
		}else{
			//HACKEO
			@mail("guillermo.morales@foxconn.com","Hacking attempt",var_export($_SERVER,true));
		}
		if(file_exists("app/_default/data/".$view.".php")){
			trace("Loading Default View "."app/_default/data/".$view.".php");
			require("app/_default/data/".$view.".php");
		}else{
			echo "No data view configured '$view'.";
		}
	}else{
		echo $controlador->_generate_messages();
	}
}else{
	if($controlador->has_access || !$controlador){
		if(isset($_GET['v']) && preg_match('/^[a-z_0-9]*$/i',$_GET['v'])){
			$m=$_GET['v'];
		}
		if($controlador){
			echo $controlador->_generate_messages();
		}
		if(file_exists("app/".$c."/views/".$m.".php")){
			trace("Loading Custom View "."app/".$c."/views/".$m.".php");
			require ("app/".$c."/views/".$m.".php");
		}else{			
			if(file_exists("app/_default/views/".$m.".php") && $controlador){
				trace("Loading Default View "."app/_default/views/".$m.".php");
				require("app/_default/views/".$m.".php");
			}else{
				echo "<div class='alert alert-warning'>No view configured for method $c->$m.</div>";
			}
		
		}
	}else{
		echo $controlador->_generate_messages();	
		require("app/Main/views/login.php");
	}
}
if(!isset($_GET['_data'])){
	if($controlador->template){
		require("template/".$controlador->template."/footer.php");
	}else{
		require("template/footer.php");
	}
}

trace("Finished_view ".$c." ".$m);

mysqli_close($dbconn);
if(file_exists('_times.log')){
	$access_log=array();
	$access_log[]=date("Y-m-d H:i:s");
	$access_log[]=$_SERVER['HTTP_X_FORWARDED_FOR'];
	$access_log[]=$_SERVER['REMOTE_ADDR'];
	$access_log[]=$_SERVER['REQUEST_URI'];
	$access_log[]=session_id();
	$endtime = microtime(true);
	$timediff = $endtime - $starttime;
	$access_log[]=$timediff;
	file_put_contents('_times.log',"\n".implode("\t",$access_log), FILE_APPEND);
}
?>