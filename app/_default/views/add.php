<?php


if(!isset($hidden)){
	if(strcasecmp($result['table_description']=$db->describe($args['c'])['id']['EXTRA'],"auto_increment")==0){
		$hidden=array("id","created","created_by__users_id","modified","modified_by__users_id");
	}	
}
if(!isset($field_type)){
	$field_type=array("id"=>"");
}

if(!isset($links)){
	$links=array(
		"List"=>"?c=".$args['c']
	);
}
include "app/_default/form.php";