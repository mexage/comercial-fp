<?php
function add_optional_args($args){
	$optional_args="";
	foreach ($args as $name=>$value) {
		if($name!="c" && $name!="m" && $name!="dm" && $name!="_data" && $name!="query"){
			$optional_args=$optional_args."&".$name."=".$value;
		}
	}
	return $optional_args;
}
?>