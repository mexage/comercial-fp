<?php

include("app/_default/pre_form.php");

//Config deafult method view();
// * Read only, action edit, comments hidden.

$actions=array(
	"Edit"=>"danger"
);

$field_type=array();

$field_descriptions=$db->describe(strtolower($args['c']));

foreach($field_descriptions as $field){
	$field_type[$field['COLUMN_NAME']] = "readonly";
	}

$links= array(
		"List"=>"?c=".$args['c']
);

//More configuration specific;
