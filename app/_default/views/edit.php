<?php
if(!isset($field_type)){
	$field_type=array(
		"id"=>"readonly",
		"created"=>"readonly",
		"created_by__users_id"=>"label",
		"modified"=>"readonly",
		"modified_by__users_id"=>"label"
	);
}

include("app/_default/form.php");