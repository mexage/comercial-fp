<?php //if(strcasecmp($args['ajax'],"noheader")==0){ ?>
<table class='table table-striped text-left'><?php
if($result['files']){ 
	echo "<tr><th>File</th><th>Size</th><th>Date</th><th>Action</th></tr>";
	foreach($result['files'] as $file){
		echo "<tr>";
			echo "<td>";
				echo "<a href='?c=".$file['c']."&id=".$file['id']."&name=".$file['name']."&_data=attachment'><span class='glyphicon glyphicon-file'></span> ".(($file['friendly_name'])? $file['friendly_name']:$file['name'] )."</a>";
			echo "</td>";
		echo "<td>";
			echo number_format($file['size']/1024,1)." Kb";
		echo "</td>";
		echo "<td>";
			echo date("Y-m-d H:i",$file['modified']);
		echo "</td>";
		echo "<td>";
			echo "<a class='btn btn-danger' href='?c=".$file['c']."&m=unattach&id=".$file['id']."&name=".$file['name']."'><span class='glyphicon glyphicon-trash'></span> Delete</a>";
		echo "</td>";
		echo "</tr>";
	}
	
}else{
	echo "<div class='well'>No data</div>";
}
?></table>
<form action='?c=<?php echo $args['c'];?>&m=attach&id=<?php echo $args['id']; ?>' method="post" enctype="multipart/form-data">
	<input type='hidden' name='c' value='<?php echo $args['c'];?>' />
	<input type='hidden' name='m' value='attach' />
	<input type='hidden' name='id' value='<?php echo $args['id'];?>' />
	<input type='file' name='attachment' />
	<input class='btn btn-primary' type='submit' name='action' value='Upload' />
</form>
<?php //} ?>