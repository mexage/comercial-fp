<?php
function add_optional_args($args){
	$optional_args="";
	foreach ($args as $name=>$value) {
		if($name!="c" && $name!="m" && $name!="dm" && $name!="_data" && $name!="query"){
			$optional_args=$optional_args."&".$name."=".$value;
		}
	}
	return $optional_args;
}
?>
<h2><?php 
if(!isset($title)){
	echo ucwords(str_ireplace("_"," ",$args["c"])); 
}else{
	echo $title;
}
?></h2>
<?php echo php_Chart::copy_button("report");?>
<div id="report">Loading...</div>

<div class="panel panel-info">
<div class="panel-heading"><h3>Design Data</h3></div>
<div class="panel-body">
<div class="form-group">
  <label for="code">Codigo de configuracion:</label>
  <textarea id="code" class="form-control"><?php echo stripslashes($args['query']);?></textarea>
</div>

<button class="btn btn-success" onclick="load();return false;">Load</button>
</div>
</div>

<script>

function load(){
	var cnf_str=$("#code").val();
	if(!cnf_str.trim())cnf_str='{}';
	var cnf = JSON.parse(cnf_str);
	cnf.onRefresh=function(conf){
		var o = {
			"cols":conf['cols'],
			"rows":conf['rows'],
			"vals":conf['vals'],
			"onRefresh":conf['onRefresh'],
			"inclusions":conf['inclusions'],
			"exclusions":conf['exclusions'],
			"rendererName":conf['rendererName'],
			"aggregatorName":conf['aggregatorName'],
			"rendererOptions":conf['rendererOptions'],
		};
		$("#code").val(JSON.stringify(o));
	};

	$.ajax({
		url:"?c=<?php echo $args["c"]; ?>&m=<?php echo (isset($args["dm"]))?$args["dm"]:"json"; ?>&_data=json<?php echo add_optional_args($args) ?>",
		success:function(d){
			try{
				var data=JSON.parse(d);
				
				$.extend($.pivotUtilities.renderers,
					$.pivotUtilities.c3_renderers,$.pivotUtilities.d3_renderers,$.pivotUtilities.custom_renderers,$.pivotUtilities.export_renderers);
				$("#report").pivotUI(data,
					cnf,true);
				if($("#report").text()=="An error occurred rendering the PivotTable UI."){
					$("#report").addClass("alert");
					$("#report").addClass("alert-danger");
				}
			}catch(e){
				$("#report").addClass("alert");
				$("#report").addClass("alert-danger");
				
				$("#report").html('<h3>Data source error: ' + e.toString()+"</h3><h3>Dump:</h3><pre>"+data+"</pre>");
			}
		}	
		
	});
}
load();
</script>