<?php
if(strcasecmp($args['datasheet'],"jExcel")==0 && !isset($dynamic)){
	$dynamic='jExcel';
}

if(!isset($panel_class)){
	$panel_class='primary';
}
if(!isset($collapse)){
	$collapse=false;
}
if(!isset($table_name)){
	$table_name = strtolower($args['c']);
}
if(!isset($id)){
	$id="panel-".$table_name;
}else{
	$id = str_replace(" ","-",$id);
}
$human_name = ucwords(trim(str_replace("_"," ",$table_name)));
if(!isset($class_name)){
	$class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));
}
if(!isset($links)){
	$links = array(
		"View:danger:eye-open"=>"?c=".$class_name."&m=view&id=<?=id?>",		
	);
}
if(!isset($actions)){
	$actions = array(
		"Add"=>"?c=".$class_name."&m=add",		
	);
}
if(!isset($hidden)){
	$hidden = array();
}

$field_descriptions = $db->describe($table_name);

if(!isset($configuration)){
	$configuration = array("c"=>$class_name,"m"=>"edit");
}
$editable = array();


foreach($field_descriptions as $field){
	if(strcasecmp(substr($field['COLUMN_NAME'],-3),"_id")==0){
		continue;
	}
	if(strcasecmp(substr($field['COLUMN_NAME'],0,2),"id")==0){
		continue;
	}
	$editable[]=$field['COLUMN_NAME'];
	
}




if(count($editable)){
	$configuration['editable']	= $editable;
}

?>
<div class='panel panel-<?php echo $panel_class; ?>'>
<div class='panel-heading'>
<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id;?>' ><?php if($title)echo "".$title."" ;?></a></h4>
</div>
<div id="<?php echo $id; ?>" class='panel-collapse collapse'>
<div class='panel-body' style='overflow:auto;'>
<?php 
if(isset($dynamic)){
	$f_tables=array();
	switch(strtolower($dynamic)){
		case "jexcel":{
			if(!isset($configuration['columns'])){
				$configuration['columns']=array();
				foreach($field_descriptions as $field){
					$column=$field['COLUMN_NAME'];
					if(!isset($f_tables[$column])){
						$f_tables[$column]=$db->ftable($column);
					}
					$new_field=array();
					$new_field['name']=$column;
					$new_field['title']=ucwords(str_replace("_"," ",$column));
					if(strcasecmp($column,'id')==0){
						if(strcasecmp($field['EXTRA'],"auto_increment")==0){					
							$new_field['readOnly']=true;						
						}
					}
					if($f_tables[$column]){
						$new_field["type"]="dropdown";
						$new_field["url"]='?c='.str_replace(" ","_",ucwords(str_replace("_"," ",$f_tables[$column]))).'&m=_list&_data=json';
					}
					if(array_search($column,$hidden)!==FALSE){
						$new_field['width']=1;
					}
					$configuration['columns'][]=$new_field;
				}
			}
			if(!isset($configuration['events'])){
				$configuration['events']=array();
			}
			if(!isset($configuration['defaults'])){
				$configuration['defaults']=array();
			}
			
			
			
			
			$jexcel_id=php_Grid::jExcel($table_name,$result[$table_name], $configuration['columns'],$configuration['events'] ,$configuration['defaults']);
			echo "<script>";
			echo '$("#'.$jexcel_id.'_div").closest(".panel-body").css("overflow","initial");';
			echo "</script>";
		}break;
		default:{
			echo php_Grid::dataTable($result[$table_name], $configuration , $table_name);
		}break;
	}
	
}else{
	echo php_Grid::draw($result[$table_name], $links, $hidden);
}
?>
</div>
<div class='panel-footer'>
<div class='row'>
	<div class='col-md-4'></div>
	<div class='col-md-4'></div>
	<div class='col-md-4 text-right'>
	<?php
	if(count($actions)){
		foreach($actions as $action=>$url){
	?>
		<a class='btn btn-primary' href='<?php echo $url; ?>'><?php echo $action;?></a>
	<?php
		}
	}
	?>	
	</div>
</div>
</div>
</div>
</div>
<?php
if(!$collapse){
?>
	<script>$(function(){$(<?php echo '"#'.$id.'"';?>).collapse("show").resize();});</script>
<?php
}
?>

<?php
unset($table_name);
unset($class_name);
