<?php

if(!isset($actions)){
	$actions=array(
		"Edit"=>"danger"
	);
}

if(!isset($field_type)){
	$field_type=array();

	$field_descriptions=$db->describe(strtolower($args['c']));

	foreach($field_descriptions as $field){
		$field_type[$field['COLUMN_NAME']] = "readonly";
	}
}
if(!isset($links)){
	$links= array(
		"List"=>"?c=".$args['c']
	);
}
include("app/_default/form.php");

