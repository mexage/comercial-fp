
<div class='panel panel-info'>
<div class='panel-heading panel-info'>
<?php if($title)echo "<h3>".$title."</h3>" ;?>
</div>
<div class='panel-content'>
<table class='table table-bordered table-striped'>
<tbody>
<?php
$table_name = strtolower($args['c']);
$human_name = ucwords(trim(str_replace("_"," ",$table_name)));
$class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));

if(!isset($actions)){
	$actions = array(
		"List"=>"?c=".$class_name."",		
		"Edit"=>"?c=".$class_name."&m=edit&id=<?=id?>",		
	);
}
if(!isset($hidden)){
	$hidden = array();
}

$fields = $db->describe($table_name);


foreach($fields as $field){
	if(array_search($field['COLUMN_NAME'],$hidden)===false){
		$column=$field['COLUMN_NAME'];
		echo "<tr>";
		echo "<th>".ucwords(str_replace("_"," ",$column))."</th>";
		$d=$result[$table_name][$column];
		if($f_table=$db->ftable($column)){
			$d=$db->get($f_table,$d);
			echo "<td><a class='btn btn-info' href='?c=".$f_table."&m=view&id=".$d['id']."'> ".$d['name']." (".$d['id'].")</a></td>";
		}else{
			echo "<td>".$d."</td>";
		}
		echo "</tr>";
	}
}


?>
</tbody>
</table>
</div>

<?php
	if(count($actions)){
		echo "<div class='panel-footer'>";
		echo '<div class="btn-group">';
		foreach($actions as $t=>$u){
			echo "<a class='btn btn-danger' href='".php_Grid::replace($u,$result[$table_name])."'>".$t."</a>";
		}
		echo '</div>';
		echo '</div>';
	}
?>