<?php 
$table_name = strtolower($args['c']);
$human_name = ucwords(trim(str_replace("_"," ",$table_name)));
$class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));

if(!isset($result[$table_name])){
	$result[$table_name]=$args;
}

if(!isset($actions)){
	$actions=array(
		"Save"=>"primary"
	);
}
if(!isset($links)){
	$links = array(
		"List"=>"?c=".$class_name."",		
		"View"=>"?c=".$class_name."&m=view&id=<?=id?>",		
	);
}

if(!isset($field_type)){
	$field_type=array(
		"id"=>"readonly"
	);
}

if(!isset($hidden)){
	$hidden = array(
	);
}
if(!isset($slave_tables)){
	$slave_tables=array();
}

if(!isset($f_tables)){
	$f_tables=array();
}

