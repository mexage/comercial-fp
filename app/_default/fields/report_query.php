<?php


echo "<tr>";
echo "<td colspan='2'>";
echo "<div class='row'>"; 
echo "<div class='col-lg-12'>"; 
echo "<label>".ucwords(str_replace("_"," ",$column))."</label>";

echo php_Chart::div($column."_chart");

$json_str = ($result[$table_name][$column])?$result[$table_name][$column]:$args[$column];
if(!$json_str){
	echo "<span class='label label-danger'>JSON object not present. Need {url:...[,cnf:...]}";
}else{

	$json=json_decode($json_str,true);

	if(json_last_error()){
		echo "<span class='label label-danger'>".json_last_error_msg()."</span>";
		echo "<div class='well'>".$json_str."</div>";

	}else{
		if(!$json['url']){
			echo "<span class='label label-danger'>Required url variable not found. Need {url:...}";
		}else{
			if(!$json['cnf'])$json['cnf']=array();
			if(!$json['cnf']['vals'])$json['cnf']['vals']=array();
			if(!$json['cnf']['aggregatorName'])$json['cnf']['aggregatorName']="Count";

			$cnf_string=json_encode($json['cnf']);

			$cnf_string=substr($cnf_string,0,strlen($cnf_string)-1);
			$cnf_string.=',onRefresh:function(conf){
				var inc={};
				var exc={};
				
				for(var i in conf["inclusions"]){
					if(typeof(conf["exclusions"][i])!=="undefined"){
						if(conf["exclusions"][i].length<conf["inclusions"][i].length){
							exc[i]=conf["exclusions"][i];
						}else{
							inc[i]=conf["inclusions"][i];
						}
					}else{
						inc[i]=conf["inclusions"][i];						
					}
				}
				
				for(var i in conf["exclusions"]){
					if(typeof(conf["inclusions"][i])!=="undefined"){
						if(conf["inclusions"][i].length<=conf["exclusions"][i].length){
							inc[i]=conf["inclusions"][i];
						}else{
							exc[i]=conf["exclusions"][i];
						}
					}else{
						exc[i]=conf["exclusions"][i];						
					}
				}
				
				var c = {					
					"cols":conf["cols"],
					"rows":conf["rows"],
					"vals":conf["vals"],
					"inclusions":inc,
					"exclusions":exc,
					"rendererName":conf["rendererName"],
					"aggregatorName":conf["aggregatorName"],
					"rendererOptions":conf["rendererOptions"],
				};
				var o={
					"url":"'.$json['url'].'",
					"cnf":c
				};
				$("#'.$column.'").val(JSON.stringify(o));
			}}';

			echo php_Chart::ajax($json['url'],
				php_Chart::javascript($column."_chart",$cnf_string,true)
			);

		}
	}
}
/*echo "<script>";
echo "var reload__".$column."__report =";
echo "var ".$column."__report = ".((isset($result[$table_name][$column]))?$result[$table_name][$column]:(($args[$column])?$args[$column]:"{}")).";";
echo "";

echo "</script>";
echo php_Chart::ajax('"+query.url+"',php_Chart::javascript($column,'"+'.$column.'__report"+'),"ajax_data",$column."__report");
*/
echo "</div>";
echo "</div>";
echo php_Field::textarea(ucwords(str_replace("_"," ",$column)),json_encode($json['cnf']));
echo '<script>setTimeout(function(){$(window).resize();},1000);</script>';
echo "</td>";
echo "</tr>";