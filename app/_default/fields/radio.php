<?php
echo "<tr>";
echo "<th>".ucwords(str_replace("_"," ",$column))."</th>";	
//$d=$db->get($f_table,$d);
if($f_tables[$column] || strcasecmp($field_type[$column],"radio")==0){
	if(!isset($lists[$f_tables[$column]])){
		if(isset($result[$f_tables[$column]])){
			$lists[$f_tables[$column]]=$result[$f_tables[$column]];					
		}else{
			$lists[$f_tables[$column]]=$db->hash($f_tables[$column],"id");
		}
	}
	if(strcasecmp($field_type[$column],"radio")==0){
		echo "<td>".php_Field::dropdown($column,$lists[$f_tables[$column]],(isset($result[$table_name][$column]))?$result[$table_name][$column]:$args[$column],0)."</td>";
	}else{
		echo "<td><a class='btn btn-info' href='?c=".$f_tables[$column]."&m=view&id=".(isset($result[$table_name][$column]))?$result[$table_name][$column]:$args[$column]."'> ".$lists[$f_tables[$column]][(isset($result[$table_name][$column]))?$result[$table_name][$column]:$args[$column]]['name']." (".(isset($result[$table_name][$column]))?$result[$table_name][$column]:$args[$column].")</a>";
	}
}else{
	if(strcasecmp($column,"id")==0){
		echo "<td>".php_Field::text($column,(isset($result[$table_name][$column]))?$result[$table_name][$column]:$args[$column],0,"","",array("type"=>"hidden")).(isset($result[$table_name][$column]))?$result[$table_name][$column]:$args[$column]."</td>";
	}else{
		echo "<td>".(isset($result[$table_name][$column]))?$result[$table_name][$column]:$args[$column];
	}
}

?>
<script>

//Get Exising Select Options    
$('select#<?php echo strtolower(str_ireplace(" ","_",$column));?>').each(function(i, select){
    var $select = $(select);
	
    $select.find('option').each(function(j, option){
        var $option = $(option);
        // Create a radio:
        var $radio = $('<input type="radio" />');
        // Set name and value:
        $radio.attr('name', $select.attr('name')).attr('value', $option.val()).attr('id', $select.attr('name')+'-'+$option.val());
		<?php //if(isset($args['location'])){?>
			// Set checked if the option was selected
			if ($option.is(':selected')) $radio.attr('checked', 'checked');
		<?php //} ?>
		// Insert radio before select box:
        $select.before($radio);
        // Insert a label:
        $select.before(
          $("<label />").attr('for', $select.attr('name')+'-'+$option.val()).html("&nbsp;"+$option.text())
        );
        // Insert a <br />:
        //$select.before("<div style='display:inline-block;width:20px;'></div>");
    });
    $select.remove();
});
$("input[name=<?php echo strtolower(str_ireplace(" ","_",$column));?>]").checkboxradio({icon:true});
</script>
<?php
echo "</td>";
echo "</tr>";

?>