<?php
function isAssoc(array $arr){
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}
if(is_array($result)){
	$assoc=isAssoc($result);
	$first=true;
	if($assoc){
		trace("Using memory efficient assoc array algorithm");
		echo "{";
		foreach($result as $k=>$v){
			if($first){
				$first=false;
			}else{
				echo ",";
			}
			
			$r= json_encode($v,JSON_UNESCAPED_UNICODE|JSON_PARTIAL_OUTPUT_ON_ERROR);
			if($r){
				echo '"'.addslashes($k).'":'.$r;
			}
		}
		echo "}";
	}else{
		trace("Using memory efficient numeric array algorithm");
		echo "[";
		foreach($result as $k=>$v){
			if($first){
				$first=false;
			}else{
				echo ",";
			}
			
			$r= json_encode($v,JSON_UNESCAPED_UNICODE|JSON_PARTIAL_OUTPUT_ON_ERROR);
			if($r){
				echo $r;
			}
		}
		echo "]";
	}
}else{
	trace("Using full json_encode");
	echo json_encode($result,JSON_UNESCAPED_UNICODE|JSON_PARTIAL_OUTPUT_ON_ERROR);
}
if(json_last_error()){
	//echo json_last_error_msg();
}
