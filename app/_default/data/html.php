<?php

if(!$result){
	echo "<table><tr><td>Sorry, no data was found.</td></tr></table>";
}else{
	foreach($result as $r){
		$header=array_keys($r);
		break;
	}
	echo "<table border='1'>";
	// Header
	echo "<tr>";
	foreach($header as $fields){
		echo "<th>".$fields."</th>";
	}
	echo "</tr>";
	// Data
	foreach($result as $row){
		echo "<tr>";
		foreach($header as $field){
			echo "<td>".htmlentities($row[$field])."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";
}