<?php

// this view will not show header and footer, only html.
if(isset($_GET['v']) && preg_match('/^[a-z_0-9]*$/i',$_GET['v'])){
	$m=$_GET['v'];
}
if(file_exists("app/".$c."/views/".$m.".php")){
	trace("Loading Custom View "."app/".$c."/views/".$m.".php");
	require ("app/".$c."/views/".$m.".php");
}else{
	
	if(file_exists("app/_default/views/".$m.".php")){
		trace("Loading Default View "."app/_default/views/".$m.".php");
		require("app/_default/views/".$m.".php");
	}else{
		echo "No view configured for method $c->$m.";
	}
}
?>