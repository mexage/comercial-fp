<?php
	require_once("template/header.php");
?>
<div class="col-lg-12" id="pivotJS"></div>

<script type="text/javascript">
	<?php
	function get_path(&$data,$path){
		if($path){
			// If we still have more path keys
			$keys = explode("/",$path);
			$current = array_shift($keys); // Pop first element from array
			if(!isset($data[$current])) return null;
			return get_path($data[$current],implode("/",$keys));
		}else{ // If already at the end of the path
			if(!isset($data)) return null;
			return $data;
		}
	}
	$final_result=get_path($result,$args['_path']);
	$rows=array();
	if($args['cols']){
		if($args['cols']=="All"){			
			foreach ($final_result[0] as $key => $value) {
				$rows[]=$key;
			}
		}else{
			$rows=explode(",",$args['cols']);
		}
		$row_string='"'.implode('","',$rows).'"';
	}else{
		$row_string="";
	}
		echo php_Chart::javascript("pivotJS",'{"cols":[],"rows":['.$row_string.'],"vals":[],"inclusions":{},"exclusions":{},"sorters":{},"rendererName":"Table","aggregatorName":"Count","rendererOptions":{"localeStrings":{"renderError":"An error occurred rendering the PivotTable results.","computeError":"An error occurred computing the PivotTable results.","uiRenderError":"An error occurred rendering the PivotTable UI.","selectAll":"Select All","selectNone":"Select None","tooMany":"(too many to list)","filterResults":"Filter values","apply":"Apply","cancel":"Cancel","totals":"Totals","vs":"vs","by":"by"}}}',true,json_encode($final_result));
	?>
</script>
<?php
	require_once("template/footer.php");
?>