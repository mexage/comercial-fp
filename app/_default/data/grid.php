<?php 

// path = test/123/abc/3
// 
// $data = $result
//get_path($result,test/123/abc/3)
// get_path(data['test'], 123/abc/3)
// 	get_path(data['123'], abc/3)
//		get_path(data['abc'],3)
//         get_path(data['3'],"")
//				return $data['3']; 
//
//  returns $result['test']['123']['abc']['3']

function get_path(&$data,$path){
	if($path){
		// If we still have more path keys
		$keys = explode("/",$path);
		$current = array_shift($keys); // Pop first element from array
		if(!isset($data[$current])) return null;
		return get_path($data[$current],implode("/",$keys));
	}else{ // If already at the end of the path
		if(!isset($data)) return null;
		return $data;
	}
	
	
}

echo php_Grid::draw(get_path($result,$args['_path']));

/*
if(!$result){
	echo "<table><tr><td>Sorry, no data was found.</td></tr></table>";
}else{
	if(!$args['_path']){
		echo php_Grid::draw($result);
	}else{
		$keys = explode("/",$args['_path']);
		$arrays=implode("']['",$keys);
		$command="echo php_Grid::draw(\$result['".$arrays."']);";
		echo eval($command);
	}

}
*/