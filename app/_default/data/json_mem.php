<?php
echo "MEM PEAK USE:". memory_get_peak_usage()."<br />";
echo "MEM USAGE:". memory_get_usage(true)."<br />";

//unset($r);
echo "<h1>Globals</h1>";
echo "<pre>";
unset($r);
foreach($GLOBALS as $k=>$v){
	if(strcasecmp($k,"GLOBALS")!=0){
		echo '<strong>' . $k . '</strong>: ';
		echo count($v) . " | " ;
		echo 	strlen(serialize($v))  . '<br />';
	}
}
echo "</pre>";
/*
echo "<h1>Variables</h1>";
echo '<pre>';
$vars = get_defined_vars();
foreach($vars as $name=>$var)
{
	if(strcasecmp($name,"GLOBALS")!=0){
		echo '<strong>' . $name . '</strong>: ';
		echo count($var) . " | ";
		echo strlen(serialize($var)) . '<br />';
	}
}
echo '</pre>';
*/


function isAssoc(array $arr)
{
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}

$assoc=isAssoc($result);
$first=true;
if($assoc){
	echo "{";
}else{
	echo "[";
}

foreach($result as $row){
	if($first){
		$first=false;
	}else{
		echo ",";
	}
	echo json_encode($row);
}

if($assoc){
	echo "}";
}else{
	echo "]";
}