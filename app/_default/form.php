<?php 
include("app/_default/pre_form.php");
echo php_Field::start_form($args['c'],$args['m'],array("id"=>$args['id']));
?>
<div class='panel panel-primary'>
<div class='panel-heading'>
<h4 class='panel-title'><?php if($title)echo "".$title."" ;?></h4>
</div>
<div class='panel-body'>
<table class='table table-bordered table-striped'>
<tbody>
<?php
$result['field_type']=$field_type;
$result['ftables']=$f_tables;
if(!isset($fields)){
	$field_descriptions=$db->describe($table_name);
	
	$fields = array();
	
	foreach($field_descriptions as $field){
		$fields[]=$field['COLUMN_NAME'];
		if(isset($custom_fields)){
			if(isset($custom_fields[$field['COLUMN_NAME']])){
				if(is_array($custom_fields[$field['COLUMN_NAME']])){
					foreach($custom_fields[$field['COLUMN_NAME']] as $cust_fld){
						$fields[]=$cust_fld;
					}
				}else{
					$fields[]=$custom_fields[$field['COLUMN_NAME']];
				}
			}
		}
	}
}

foreach($fields as $field){
	if(array_search($field,$hidden)===false){
		$column=$field;
		
		if(!isset($f_tables[$column])){
			$f_tables[$column]=$db->ftable($column);
		}
		
		// Determine field_type if empty
		if(!isset($field_type[$column]) || !$field_type[$column]){
			if($f_tables[$column]){
				$field_type[$column]="dropdown";
			}else{
				$field_type[$column]="text";				
			}
		}
		
		// Don't include hidden type into the table; add them to the footer.
		if(strcasecmp($field_type[$column],"hidden")!=0){			
			if(strcasecmp(substr($field_type[$column],0,1),"/")==0){
				include(substr($field_type[$column],1));
			}else{
				
				if(file_exists("app/".$args['c']."/fields/".$field_type[$column].".php")){
					include("app/".$args['c']."/fields/".$field_type[$column].".php");
				}else if(file_exists("app/_default/fields/".$field_type[$column].".php")){
					include("app/_default/fields/".$field_type[$column].".php");
				}else{
					include("app/_default/fields/default.php");
				}
			}
			
		}
	}
}

foreach($fields as $field){
	$column=$field;
	if(strcasecmp($field_type[$column],"hidden")==0){	
		echo php_Field::hidden($column,$result[$table_name][$column],0);
	}
}
if(count($pass_args)){
	foreach($pass_args as $k=>$v){
		echo php_Field::hidden($k,$v);
	}
}

?>
</tbody>
</table>
</div>
<?php
	
	if(count($links) || count($actions)){
		echo "<div class='panel-footer'>";
		echo "<div class='row'>";
		echo "<div class='col-md-4'></div>";
		echo "<div class='col-md-4'>";
		
		echo '<div class="btn-group">';
		if(count($links)){
			foreach($links as $t=>$u){
				echo "<a class='btn btn-default' href='".php_Grid::replace($u,$result[$table_name])."'>".$t."</a>";
			}
		}
		echo '</div>';
		echo '</div>';
		echo "<div class='col-md-4 text-right'>";
		echo "<a class='btn btn-default' href='#' onclick='window.history.back();return false;'>Back</a> ";
		if(count($actions)){	
			foreach($actions as $action=>$class){
				echo "<input class='btn btn-".$class."' type='submit' name='action' value='".$action."' />";
			}
		}
		echo "</div>";
		echo "</div>";
		echo '</div>';
	}
?>
</div>
<?php 
echo php_Field::end_form();


if(isset($result['slave_tables'])){
	if(count($result['slave_tables'])){
		
		foreach($result['slave_tables'] as $id=>$slave){
			
			$table_name=$slave['description']['TABLE_NAME']." ".$slave['description']['COLUMN_NAME'];
			$class_name="".str_replace(" ","_",ucwords(str_replace("_"," ",$slave['description']['TABLE_NAME'])));
			if(!isset($slave['panel_class'])){
				$panel_class="info";
			}else{
				$panel_class=$slave['panel_class'];
			}
			if(!isset($slave['hidden'])){
				$hidden=array();
			}else{
				$hidden=$slave['hidden'];
			}
			
			if(!isset($slave['title'])){
				$title = ucwords(str_replace("_"," ",$slave['description']['TABLE_NAME']))." (via ".ucwords(str_replace("_"," ",$slave['description']['COLUMN_NAME'])).")";
			}else{
				$title=$slave['title'];
			}
			if(!isset($slave['actions'])){
				$actions=array(
					"Add"=>"?c=".$class_name."&m=add&".$slave['description']['COLUMN_NAME']."=".$args['id']
				);
			}else{
				$actions=$slave['actions'];
			}
			if(!isset($slave['links'])){					
				$links=array(
					"Edit"=>"?c=".$class_name."&m=edit&id=<?=id?>"				
				);
			}else{
				$links=$slave['links'];
			}
			$collapse = count($result[$table_name])>30;
			if(isset($slave['dynamic'])){
				if($slave['dynamic']){
					$dynamic=$slave['dynamic'];
				}else{
					unset($dynamic);
				}
			}
			if(strcasecmp($slave['dynamic'],"jExcel")==0){
				if(isset($slave['jExcel_configuration'])){
					$configuration=$slave['jExcel_configuration'];
				}
			}
			include("app/_default/views/index.php");			
		}
	}
}