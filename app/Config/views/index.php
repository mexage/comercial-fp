<div class="row content-fluid">
	<div class="col-md-12">
	<div class="panel  panel-info text-left">
		<div class='panel-heading'><h4 class='panel-title'><a data-toggle='collapse' href='#categories' >Categories</a></h4></div>
		<div id="categories" class='panel-collapse collapse'>
				<div class="panel-body">
				<?php
				php_Grid::draw($result['categories'],array(
					"Edit"=>"?c=Categories&m=edit&id=<?=id?>",
					//"Remove"=>"?c=Categories&m=remove&id=<?=id>"
				));
				?>
				</div>
				<div class="panel-footer"><a href='?c=Categories&m=add'>Add</a></div>
				
		</div>
	</div>
	</div>
</div>
<div class="panel panel-info text-left">
<div class="panel-heading">Owners</div>
<div class="panel-body">
<?php
php_Grid::draw($result['owners'],array(
	"Edit"=>"?c=Owners&m=edit&id=<?=id?>",
	//"Remove"=>"?c=Owners&m=remove&id=<?=id>",
	"Test"=>"?c=Config&m=test_whatsapp&group=<?=whatsapp?>&message=Group+<?=name?>"
));
?>
		</div>
<div class="panel-footer"><a href='?c=Owners&m=add'>Add</a></div>

</div>


<div class="panel  panel-info text-left">
<div class="panel-heading">Areas</div>
<div class="panel-body">
<?php
php_Grid::draw($result['areas'],array(
	"Edit"=>"?c=Areas&m=edit&id=<?=id?>",
	//"Remove"=>"?c=Areas&m=remove&id=<?=id>"
));
?>

		</div>
<div class="panel-footer"><a href='?c=Areas&m=add'>Add</a></div>
		</div>
</div>
</div>
