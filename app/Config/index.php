<?php
class Config extends php_Controller{
	function __construct(){
		parent::__construct();
		$this->_require_access("Config");
	}
	
	function index($args){
		$result=array();
		
		$result['owners']=$this->db->search("owners");
		$result['categories']=$this->db->search("categories");
		$result['areas']=$this->db->search("areas");
		
		return $result;
	}
	function test_whatsapp($args){
		php_WhatsApp::send("ANDON_TEST",$args['group'],"*ANDON TEST* ".$args['message']." This is a test of the WhatsApp group. Please send a WhatsApp message to (656) 123 1312 in order to confirm you have received it correctly.");
		return "?c=Config";
	}
	function send_massive($args){
		$result=array();
		if(isset($args['action']) && isset($args['message'])){
			$owners = $this->db->search("owners");
			foreach($owners as $o){
				//$result[]=$o['whatsapp'].":".$args['message'];
				php_WhatsApp::send("ANDON_MASSIVE_".str_ireplace(" ","_",trim($o['whatsapp'])),$o['whatsapp'],"*ANDON ADMIN MESSAGE* ".$args['message']."");
			}
		}
		return $result;
	}
	
}