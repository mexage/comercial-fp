<?php
class Self_Monitor extends php_Controller{
	function __construct($args){
		parent::__construct($args);
		$this->title="Self Monitor";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		$result=array();

		// Check camera 1
		$test_result=($this->camera_time_ago(array("id"=>"1"))[0]<60)?"success":"danger";
		$test=array(
			"Description"=>"Harmoni Camera 1 age",

			"Result"=>"<div class='btn btn-"
			.($test_result)
			."'>".$test_result."</div>"
		);
		$result[]=$test;
		
		
		// Check camera 2
		$test_result=($this->camera_time_ago(array("id"=>"2"))[0]<60)?"success":"danger";
		$test=array(
			"Description"=>"Harmoni Camera 2 age",

			"Result"=>"<div class='btn btn-"
			.($test_result)
			."'>".$test_result."</div>"
		);
		$result[]=$test;

		// Check heartbeat
		$test_result=($this->heartbeat_time_ago(array("id"=>"1"))[0]<120)?"success":"danger";
		$test_message=$test_result;
		if(strcasecmp($test_result,"success")==0){
			$test_ini=parse_ini_file("s/self_monitor/heartbeat/1.txt",true,INI_SCANNER_RAW);
			file_put_contents("test_ini.txt",var_export($test_ini,true));
			if(strcasecmp($test_ini["message"]["message"],"success")!=0){
				$test_result="danger";
				$test_message=$test_ini["message"]["created"].": ".$test_ini["message"]["message"];
			}
		}
		
		$test=array(
			"Description"=>"Harmoni Interface Job Heartbeat",

			"Result"=>"<div class='btn btn-"
			.($test_result)
			."'>".$test_message."</div>"
		);
		$result[]=$test;

		return $result;
	}

	function test($args){
		$result=array();
		if(!is_numeric($args["id"]))return "/";
		// Check heartbeat
		$test_result=($this->heartbeat_time_ago(array("id"=>$args['id']))[0]<120)?"success":"danger";
		$test_message=$test_result;
		if(strcasecmp($test_result,"success")==0){
			$test_ini=parse_ini_file("s/self_monitor/heartbeat/".$args['id'].".txt",true,INI_SCANNER_RAW);
			//file_put_contents("test_ini.txt",var_export($test_ini,true));
			if(strcasecmp($test_ini["message"]["message"],"success")!=0){
				$test_result="danger";
				$test_message=$test_ini["message"]["created"].": ".$test_ini["message"]["message"];
			}
		}
		
		$test=array(
			"Description"=>"Harmoni Interface Job Heartbeat",

			"Result"=>"<div class='btn btn-"
			.($test_result)
			."'>".$test_message."</div>"
		);
		$result[]=$test;

		return $result;
	}


    function test_button($args){
		$ini="[header]"."\n";
		$ini.="id=".date("YmdHis")."\n";
		$ini.="[accesos]"."\n";
		$ini.="punto_accesos_id=test\n";
		$ini.="personas_id=0\n";
		$ini.="created=".date("Y-m-d H:i:s")."\n";
		
		file_put_contents("doors/test.txt",$ini);		
		return array();
	}
	function camera_time_ago($args){
		if(!is_numeric($args["id"]))die("error");
		$result=array();
		$modif_time=filemtime("s/camaras/".$args["id"]."/image.jpg");
		$result[]=time()-$modif_time;
		return $result;
	}
	function heartbeat_time_ago($args){
		if(!is_numeric($args["id"]))die("error");
		$result=array();
		$modif_time=filemtime("s/self_monitor/heartbeat/".$args["id"].".txt");
		$result[]=time()-$modif_time;
		return $result;
	}
	function heartbeat($args){
		if(!is_numeric($args["id"]))return null;
		$data="[header]\n";
		$data.="id=".$args['id']."\n";
		$data.="[message]\n";				
		$data.="created=".date("Y-m-d H:i:s")."\n";
		$data.="message=".$args['message']."\n";
		
		file_put_contents("s/self_monitor/heartbeat/".$args['id'].".txt",$data);
		return array();
	}
	function cli($args){
		if(isset($args["cmd"])){
			unlink("cmd/testing.result");
			file_put_contents("cmd/testing",$args["cmd"]);			
		}
	}
	function cli_result($args){
		$result=array();
		if(file_exists("cmd/testing.result")){
			$result["cli"]=file_get_contents("cmd/testing.result");
			unlink("cmd/testing.result");
		}
		return $result;
	}
	function cli_upload($args){
		file_put_contents("cmd/testing.result",$args["cli"]);
		unlink("cmd/testing");
		$this->template="blank";
	}

}