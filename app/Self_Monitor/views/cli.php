<div class='box-section'>
<?php
echo php_Field::text("Cmd","");
echo php_Field::textarea("Result","");
?>
<button class='btn btn-primary' onclick="send()">Send</button>
</div>
<script>
var checking_result;

function send(){
    
    $.ajax({
        "url":"index.php",
        "data":{
            "c":"Self_Monitor",
            "m":"cli",
            "cmd":$("#cmd").val()
        },
        "method":"POST",
        "success":function(data){
            try{

                $("#cmd").val("");
                $("#result").val("");
                //var o = JSON.parse(data);
                //if(o.cli){
                //    $("#result").val(o.cli);
                //    clearInterval(checking_result);
                //}
                checking_result=setInterval(checkResult,1000);

            }catch(e){

            }
        }
    });
}

function checkResult(){
    $.ajax({
        "url":"index.php",
        "data":{
            "c":"Self_Monitor",
            "m":"cli_result",
            "_data":"json"
        },
        "success":function(data){
            try{
                var o = JSON.parse(data);
                if(o.cli){
                    $("#result").val(o.cli);
                    clearInterval(checking_result);
                }
            }catch(e){

            }
        }
    });    
}

</script>