<?php
class Whatsapp_Messages extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Whatsapp_Messages");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Whatsapp Messages";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		$result = array();
		
		$result=parent::index($args,$filter,$order = 'created desc',20);
		
		// Your code here.
		
		return $result;
	}
	function add($args){
		$result = array();
		
		$result=parent::add($args);
		
		// Your code here.
		
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("whatsapp_messages",$args['id']);
		//return "?c=Whatsapp_Messages";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
		return $result;
    }
    function read($args){
        $id_where = "(id>'".addslashes($args['id'])."')";
        $queried_where = "(queried='')";
        $result=array();
        $result["messages"]=$this->db->search("whatsapp_messages",$id_where,"id ASC",1)[0];
        $this->db->execute("UPDATE whatsapp_messages SET queried=now() WHERE ".implode(" AND ", array($id_where,$queried_where)));        
        return $result;
    }
	
}
	