<style>
.timeline > li > .timeline-panel {
    width:100%;
}
.timeline:before {
    width:0;
}
</style>
<div class='box-section'>
<?php
com_Timeline::draw($result["whatsapp_messages"],"<?=name?> (<?=id?>)","<?=created?>","<?=message?>");
?>
</div>
<?php
if(!isset($args['ajax'])){
    ?>
    <script>
        function refresh(){
            $.ajax({
                "url":"?c=Whatsapp_Messages&ajax=noheader",
                "success":function(data){
                    $("#main_template_content").html(data);
                    setTimeout(refresh,5000);
                }
            });
        }
        refresh();
    </script>
    <?php
}
?>