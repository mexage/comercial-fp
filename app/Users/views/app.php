<h1><?php echo ucwords(str_ireplace("_"," ", $args['c']));?></h1>
<div id="app">
  <v-app id="inspire">
    <v-card-text>
        <template v-slot:top>
            <v-toolbar
            flat
            >
                <v-toolbar-title>My CRUD</v-toolbar-title>
                <v-divider
                    class="mx-4"
                    inset
                    vertical
                ></v-divider>
                <v-spacer></v-spacer>
                
                <v-btn
                    color="primary"
                    dark
                    class="mb-2"
                    v-bind="attrs"
                    v-on="on"
                >
                    New Item
                </v-btn>
            </v-toolbar>
        </template>
        <v-data-table
        :headers="headers"
        :items="users"
        :items-per-page="5"
        class="elevation-1"
        ></v-data-table>
  </v-app>
</div>

<?php 
$table_name=strtolower($args['c']);
?>
<script>
var users = new Vue({
	el: '#app',
	vuetify: new Vuetify(),
	
	data:{
		"<?php echo $table_name;?>":[],
		"headers":[
                <?php
                $columns=$db->describe($table_name);
                foreach($columns as $c){
                    echo "{text:'".ucwords(str_ireplace("_"," ", $c['COLUMN_NAME']))."',value:'".$c['COLUMN_NAME']."'},";
                }
                ?> 
			],		
	},
	created:function(){
		this.$http.get("?c=Users&_data=json").then(function(response){
			this.users=response.body['users'];
		});
	}
});


</script>