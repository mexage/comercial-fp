<?php
class Users extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		//$this->_require_access("Config");
		
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Users"; 
	}
	
	/*function edit($args){
		$result=array();
		
		$user= $this->db->search("users","username='".addslashes($args['username'])."'");
		
		$result['users']=$user[0];
		
		return $result;
	}*/
}