<?php
class Pagos extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Pagos");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Pagos";
	}
	function index($args){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		
		return $result;
	}
	function add($args){
		$result = array();
		
		$result=parent::add($args);
		
		// Your code here.
		
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("pagos",$args['id']);
		//return "?c=Pagos";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
		return $result;
	}
	function diario($args){
	$result = array();
	if(!$args['fecha'])$args['fecha']=date("Y-m-d");
	$result=parent::view($args);
	
	$criteria = "fecha = '" .$args["fecha"]."'";
	$result['pagos_dia'] =  $this->db->search ( "pagos" , $criteria , "" , 5000  )   ;
	
	// Your code here.
	
	return $result;
	}
	
}
	