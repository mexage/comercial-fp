<style>
th {
  position: sticky;
  top: 0;
  z-index: 10;
  background-color: white;
  text-align:center;
  border-top:2pt solid gray !important;
  border-bottom:2pt solid gray !important;
}
</style>

	<h1>Reporte Pagos</h1>

		<form  action="?c=pagos&m=diario" method="GET"> 
			<br />
			<!--<label for="start">Fecha de Reporte:</label>
			<input type="date" id="fecha" name="fecha" >-->
			<!-- <input type="submit" name="submit" value="Generar"> -->

			<?php
				if(!$args['fecha'])$args['fecha']=date("Y-m-d");
				echo php_Field::hidden("c",$args['c']);
				echo php_Field::hidden("m",$args['m']);
				echo php_Field::date("Fecha",$args['fecha']);
				echo php_Field::hidden("action","Seleccionar");
			
			?>
			<br />
			<script>
			$("#fecha").change(function(){$("#fecha").closest("form").submit();});
			</script>
		</form>
		
<br />

<?php


	$pagos_dia = array();


	foreach($result['pagos_dia'] as $data){
		$pagos_dia[] = array (
			//"id" => $data['id'],
			"cliente" => $data['clientes_id'],
			"cantidad" => $data['cantidad'],
			"fecha" => $data['fecha'],
			"notas" => $data['notas'],
			"creado_por" => $data['created_by__users_id'],
			"creado" => $data['created'],
			"termino_pagos_id" => $data['termino_pagos_id']
			
		);
	}


?>

<div class='panel panel-info'>
<div class='panel-heading'>
<h4 class='panel-title'><a data-toggle='collapse' href='#cliente'>Pagos recibidos</a></h4>
</div>
<div id='cliente' class='panel-collapse collapse in'>
<div class='panel-body' style=''>
<?php
echo php_Chart::copy_button("#pagos","Copiar");
?>
<div id='pagos'>
<?php
echo php_Grid::draw($pagos_dia);	
?>
</div>
</div>
</div>
</div>

		
		








