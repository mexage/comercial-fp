<?php
class Ventas extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Ventas");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Ventas";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		
		return $result;

	}
	function add($args){
		$result = array();
		if(!$args['clientes_id'] && $args['action']){
			$this->set_message("Necesita especificar un cliente!","danger");
			unset($args['action']);
			return "?".http_build_query($args);
		}
		$result=parent::add($args);
		
		// Your code here.
		if(is_string($result)){
			$parts=explode("id=",$result);
			$result='?c=Ventas&m=add_products&id='.$parts[1];
		}
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("ventas",$args['id']);
		//return "?c=Ventas";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
		return $result;
	}
	function add_products($args){
		$result=array();
		$estatus_ventas_hash=$this->db->hash("estatus_ventas","name");
		$termino_pagos_hash=$this->db->hash("termino_pagos","name");
		if($args['action']){
			$venta=$this->db->get("ventas",$args['id']);
			$articulos = json_decode(stripslashes($args['articulos']),true);
			if(strcasecmp($args['termino_pagos_id'],$termino_pagos_hash['Contado']['id'])==0){
				$args['fecha_promesa']='';
			}
			if(!$args['termino_pagos_id']){
				$estatus_nuevo=$estatus_ventas_hash['Cancelado']['id'];
			}else{
				$estatus_nuevo=$estatus_ventas_hash['Surtido']['id'];
			}
			$this->db->upd("ventas",$args['id'],array(
				"termino_pagos_id"=>$args['termino_pagos_id'],
				"requiere_factura"=>$args['requiere_factura'],
				"fecha_promesa"=>$args['fecha_promesa'],
				"notas"=>$args['notas'],
				"estatus_ventas_id"=>$estatus_nuevo,
				"surtida"=> date('Y-m-d H:i:s'),
			));
			if(count($articulos)){
				// will check for removed articles
				$db_articulos=$this->db->hash("ventas_articulos","id","ventas_id='".$args['id']."'");
				
				foreach($articulos as $a){
					// remove from existing articles in the new list
					unset($db_articulos[$a['id']]);
					
					if(strcasecmp($a['id'],"*")==0){
						$this->db->insert("ventas_articulos",array(
							"ventas_id"=>$a['ventas_id'],
							"productos_id"=>$a['productos_id'],
							"precio_unitario"=>$a['precio_unitario'],
							"cantidad"=>$a['cantidad'],
							"notas"=>$a['notas']
						));
					}else{
						$this->db->upd("ventas_articulos",$a['id'],array(
							"ventas_id"=>$a['ventas_id'],
							"productos_id"=>$a['productos_id'],
							"precio_unitario"=>$a['precio_unitario'],
							"cantidad"=>$a['cantidad'],
							"notas"=>$a['notas']
						));
					}
				}
				
				if(count($db_articulos)){
					// these were the ones which were not deleted during previous cycle. Remove from db.
					foreach($db_articulos as $a){
						$this->db->del("ventas_articulos",$a['id']);
					}
				}
			}
			
			if(floatval($args['abono_recibido'])){
				$this->db->insert("pagos",array(
					"clientes_id"=>$venta['clientes_id'],
					"cantidad"=>$args['abono_recibido'],
					"fecha"=>date("Y-m-d"),
					"notas"=>$args['abono_nota'],
					"created_by__users_id"=>$this->user->attr("id"),
					"termino_pagos_id"=>$termino_pagos_hash['Credito']['id']
				));
			}
			if(strcasecmp($args['termino_pagos_id'],$termino_pagos_hash['Contado']['id'])==0){
				$this->db->insert("pagos",array(
					"clientes_id"=>$venta['clientes_id'],
					"cantidad"=>str_ireplace('$','',$args['total']),
					"fecha"=>date("Y-m-d"),
					"notas"=>"Venta ID: ".$args['id']. " | Subtotal: ".$args['subtotal']." | IVA: ".$args['iva']." | Total: ".$args['total'],
					"created_by__users_id"=>$this->user->attr("id"),
					"termino_pagos_id"=>$termino_pagos_hash['Contado']['id']
				));
			}
			
			$result['message']="Se guardo correctamente!";
		}else{
			$cliente=new mod_Clientes();
			$result['ventas']=$this->db->get("ventas",$args['id']);
			if(
				strcasecmp($result['ventas']['estatus_ventas_id'],$estatus_ventas_hash['Pendiente']['id'])==0
				||strcasecmp($result['ventas']['estatus_ventas_id'],$estatus_ventas_hash['En camino']['id'])==0
			){
				$this->db->upd("ventas",$args['id'],array(
					"estatus_ventas_id"=>$estatus_ventas_hash['Surtiendo']['id']
				));
			}
			$result['productos']=$this->db->search("productos");
			$result['productos_hash']=$this->db->hash("productos");
			
			$result['ventas_articulos']=$this->db->search("ventas_articulos","ventas_id='".$args['id']."'");
			$result['clientes']=$this->db->get("clientes",$result['ventas']['clientes_id']);
			$result['adeudo']=$cliente->adeudo($result['ventas']['clientes_id']);
			$result['termino_pagos']=$this->db->hash("termino_pagos");
			$result['estatus_ventas']=$this->db->hash("estatus_ventas");
		}
		return $result;
	}
	function diario($args){
		
		$result = array();
		if(!isset($args['repartidor']))$args['repartidor']="%";
		if(!$args['fecha'])$args['fecha']=date("Y-m-d");
		$q = "select * from ventas inner join ventas_articulos 
						where ventas.id = ventas_articulos.ventas_id 
						and fecha = '". $args["fecha"] ."'
						and ventas.repartidor__users_id like '".$args["repartidor"]."'
						";
		$result['ventas_desglosada'] =  $this->db->query(  $q  )  ;
		

		$criteria = "fecha = '" .$args["fecha"]."' AND repartidor__users_id like '".$args["repartidor"]."'";
		$result['ventas_cliente'] =  $this->db->search ( "ventas" , $criteria , "" , 5000  )   ;
		
		$result['clientes'] =  $this->db->hash( "clientes" )   ;
		
		
		
		///////////// Economico /////////////////////
		$q = "select distinct(clientes_id) from pagos where fecha = '" .$args[fecha]."' AND created_by__users_id like '".$args["repartidor"]."' UNION DISTINCT select distinct(clientes_id) from ventas where fecha='" .$args[fecha]."'
		AND repartidor__users_id like '".$args["repartidor"]."'";
		$disc_clientes =  $this->db->query ( $q  )   ;

		$economico = array();
		$fec = $args["fecha"] ;
		
		$ventas=$this->db->query("
			SELECT v.clientes_id clientes_id,
				sum(va.precio_unitario*va.cantidad) importe
			FROM ventas v
			INNER JOIN ventas_articulos va ON v.id=va.ventas_id
			WHERE v.fecha='". $args["fecha"] ."'
			AND v.termino_pagos_id=2 
			GROUP BY v.clientes_id
		");
		$ventas_hash=array();
		$result['ventas']=$ventas_hash;
		
		foreach($ventas as $v){
			$ventas_hash[$v['clientes_id']]=$v;
		}
		$total_contado=0;
		$total_credito=0;
		$total_abono=0;
		
		foreach( $disc_clientes as $data){
		
			$contado = 0;
			$credito = 0;
			$abono   = 0;
			
			
			$q = "select  clientes_id , termino_pagos_id , sum(cantidad) as cantidad from pagos 
									where fecha = '"       . $fec . 
									"' and clientes_id = '"   . $data['clientes_id'] . 
									"' and created_by__users_id like '".$args["repartidor"].
									"' group by  termino_pagos_id " ; 
			
			$pagos =  $this->db->query ( $q  ) ;
			
			foreach ($pagos as $pago ){
				
				if ( $pago['termino_pagos_id'] == 1  ){
					
					$contado = $pago['cantidad'] ;
					$total_contado+=$contado;
				}elseif ( $pago['termino_pagos_id'] == 2  ){
					
					$abono = $pago['cantidad'] ;
					$total_abono+=$abono;

				} 
				
			}
			
			$credito=$ventas_hash[$data['clientes_id']]['importe']*(1+($result['clientes'][$data['clientes_id']]['iva']/100));
			
			$total_credito+=$credito;
			$mod_cliente=new mod_Clientes();
			$adeudo=$mod_cliente->adeudo($data['clientes_id']);
			$economico[] = array (


				//"id" => $data['id'],
				"clientes_id" => $data['clientes_id'],
				"Ventas al Contado" => $contado, 
				"Ventas a Credito" => $credito, 
				"Abonos Recibidos"   => $abono, 
				"Proximo Vencimiento"=>$adeudo['vencimiento_minimo'],
				"Saldo por vencer"=>$adeudo['saldo_por_vencer'],
				"Adeudo Total"=>$adeudo['saldo'],
			//	"created" => $data['created'],
			//	"created_by__users_id" => $data['created_by__users_id'],
			//	"fecha" => $data['fecha'],
			//	"estatus_ventas_id" => $data['estatus_ventas_id'],
			//	"notas" => $data['notas'],
			//	"fecha_de_promesa" => $data['fecha_promesa'],
			//	"termino_pagos_id" => $data['termino_pagos_id'],
			//	"repartidor__users_id" => $data['repartidor__users_id'],
				/*"Action" => "<a href='?c=Ventas&m=view&id=".$data['id']."' class='btn btn-danger' role='button'>
				<span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>
				View
				</a>"*/
				
			);
		}
		
		$economico[] = array (


				//"id" => $data['id'],
				"clientes_id" => "TOTAL",
				"Ventas al Contado" => $total_contado, 
				"Ventas a Credito" => $total_credito, 
				"Abonos Recibidos"   => $total_abono, 
				"Proximo Vencimiento"=>$adeudo['vencimiento_minimo'],
				"Saldo por vencer"=>$adeudo['saldo_por_vencer'],
				"Adeudo Total"=>$adeudo['saldo'],
				
			//	"created" => $data['created'],
			//	"created_by__users_id" => $data['created_by__users_id'],
			//	"fecha" => $data['fecha'],
			//	"estatus_ventas_id" => $data['estatus_ventas_id'],
			//	"notas" => $data['notas'],
			//	"fecha_de_promesa" => $data['fecha_promesa'],
			//	"termino_pagos_id" => $data['termino_pagos_id'],
			//	"repartidor__users_id" => $data['repartidor__users_id'],
				/*"Action" => "<a href='?c=Ventas&m=view&id=".$data['id']."' class='btn btn-danger' role='button'>
				<span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>
				View
				</a>"*/
				
			);
		
		
		$result['economico'] = $economico ;
		
		$result['repartidores']=$this->db->search("users");
		array_unshift($result['repartidores'],array("id"=>"%","name"=>"Todos"));
		foreach($result['repartidores'] as &$r){
			$d=$this->db->query("
			SELECT 
				count(*) c
			FROM ventas v
			INNER JOIN clientes c on v.clientes_id=c.id
			WHERE v.repartidor__users_id='".$r['id']."' 
			AND v.estatus_ventas_id in(0,1,2)");
			$r['name'].= ": ".$d[0]['c']." pendiente(s)";
		}
		
		return $result;
		
		
		
	}
	function json($args){
		$result = $this->db->query("
			select 
				v.id,
				c.name cliente,
				c.iva,
				v.fecha,
				concat(YEAR(v.fecha),'-',lpad(MONTH(v.fecha),2,'0')) mes,
				concat(YEAR(v.fecha),'-',lpad(week(v.fecha),2,'0')) semana,
				elt(weekday(v.fecha)+1,'1) Lunes','2) Martes','3) Miercoles','4) Jueves','5) Viernes','6) Sabado','7) Domingo') dia_semana,
				
				ev.name estatus,
				v.fecha_promesa,

				tp.name termino_pago,
				if(v.requiere_factura,'Requiere factura','No requiere factura') requiere_factura,
				
				p.name producto,
				va.cantidad,
				va.precio_unitario,
				(va.cantidad*va.precio_unitario) importe_subtotal,
				(va.cantidad*va.precio_unitario)*((100+c.iva)/100) importe_total
			from ventas v
			inner join ventas_articulos va on v.id=va.ventas_id
			inner join productos p on va.productos_id=p.id
			inner join clientes c on c.id=v.clientes_id
			inner join estatus_ventas ev on v.estatus_ventas_id=ev.id
			inner join termino_pagos tp on v.termino_pagos_id = tp.id;
		");
		return $result;
	}
}
	