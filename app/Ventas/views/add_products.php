<div class='row'>
<div class='col-lg-4'>

<?php $id="cliente"; // TODO: Change title ?>
<div class='panel panel-info'>
	<div class='panel-heading'>
		<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' ><?php echo $result['clientes']['name'] ?></a></h4>
	</div>
	<?php echo php_Field::start_form($args['c'],$args['m'],$args,"POST"); ?>
	<div id="<?php echo $id; ?>" class='panel-collapse collapse in'>
		<div class='panel-body' style='overflow:auto;'>
		<?php
			echo php_Field::hidden('c',$args['c']);
			echo php_Field::hidden('m',$args['m']);

		?>
			<div class='alert alert-<?php echo $result['adeudo']['css_class']; ?>'>
				<h3>Adeudo total: $<?php echo $result['adeudo']['saldo']; ?></h3>
				<?php if( $result['adeudo']['vencimiento_minimo'] ){ ?>
				<h3>Siguiente vencimiento: <br /><?php echo $result['adeudo']['vencimiento_minimo']." ($". $result['adeudo']['saldo_por_vencer']; ?>)</h3>
				
				<?php } ?>
			</div>
		</div>
		
	</div>
	<div class='panel-footer'>

	</div>
	<?php echo php_Field::end_form(); ?>
</div>

<?php $id="pago"; // TODO: Change title ?>
<?php if($result['adeudo']['saldo']){ ?>
<div class='panel panel-success'>
	<div class='panel-heading'>
		<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' >¿Se recibe abono?</a></h4>
	</div>
	<div id="<?php echo $id; ?>" class='panel-collapse collapse'>
		<div class='panel-body' style='overflow:auto;'>
		<?php
			echo php_Field::hidden('c',$args['c']);
			echo php_Field::hidden('m',$args['m']);
			echo php_Field::number("Abono Recibido","");
			echo php_Field::textarea("Notas de abono","");
			// TODO: Add content
		?>
		</div>
	</div>
	<div class='pane l-footer'>

	</div>
	<?php echo php_Field::end_form(); ?>
</div>
<?php } ?>




</div>

<div class='col-lg-8'>
<?php $id="venta"; // TODO: Change title ?>
<div class='panel panel-info'>
	<div class='panel-heading'>
		<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' >Venta</a></h4>
	</div>
	<?php echo php_Field::start_form($args['c'],$args['m'],$args,"POST"); ?>
	<div id="<?php echo $id; ?>" class='panel-collapse collapse <?php if(count($result['ventas_articulos']))echo "in";?>'>
		<div class='panel-body' style='overflow:auto;'>
		<?php
			echo php_Field::hidden('c',$args['c']);
			echo php_Field::hidden('m',$args['m']);

			// TODO: Add content
			array_unshift($result['termino_pagos'],array("id"=>"","name"=>"Seleccionar uno"));
			echo php_Field::dropdown("Termino de pago",$result['termino_pagos'],"");
			echo "<div id='campos_credito'>";
			if(!$result['ventas']['fecha_promesa']||strcasecmp($result['ventas']['fecha_promesa'],'0000-00-00')==0)$result['ventas']['fecha_promesa']=date("Y-m-d",strtotime("+7 days"));
			echo php_Field::date("Fecha de promesa",$result['ventas']['fecha_promesa']);
			echo "</div>";
			echo php_Field::textarea("Notas",$result['ventas']['notas']);
			
		?>
		<div class='well'>
		<?php echo php_Field::start_form($args['c'],$args['m'],$args,"POST"); ?>
		
		<?php
			echo php_Field::hidden('c',$args['c']);
			echo php_Field::hidden('m',$args['m']);

			// TODO: Add content
			echo php_Field::text("Busqueda de articulo","");
			
		?>
			<div id='tabla'>
			
			</div>
		</div>
		
		<?php echo php_Field::end_form(); ?>
		</div>
		<div class='panel-footer'>
		<div class='row'>
			<div class='col-sm-4'>
			</div>
			<div class='col-sm-2'>
			
			</div>
			<div class='col-sm-2'>
			
			<?php
			echo php_Field::checkbox("Requiere Factura",array(array("id"=>0),array("id"=>"1")),$result['ventas']['requiere_factura']);
			?>
			</div>
			<div class='col-sm-4'>
			<table style='width:100%;'>
				<tr><td><h4>Articulos:</h4></td><td class='text-right'><h4><span id='num_articulos'></span></h4></td></tr>
				<tr><td><h4>Subtotal:</h4></td><td class='text-right'><h4><span id='subtotal'></span></h4></td></tr>
				<tr id='iva_row'><td><h4>IVA:</h4></td><td class='text-right'><h4><span id='iva'></span></h4></td></tr>
				<tr><td><h4>Total:</h4></td><td class='text-right'><h4><span id='total'></span></h4></td></tr>
			
			</table>
			
			</div>
		</div>
	</div>
	</div>
	<?php echo php_Field::end_form(); ?>
</div>



<button id='btn_guardar' class='btn btn-primary' onclick='guardar();return false;'>Guardar</button><br />
<img id='loading_guardar' src='images/fp/loading.gif' style='display:none;' />
</div>
</div>

<script>
$("#requiere_factura").change(function(){
	draw_grid("tabla",articulos,"");
});
function draw_grid(id,data,css_class){
	if(typeof css_class==="undefined"){
		css_class="";
	}
	var html="<table class='table table-striped'>";
	
	/*for(var r in data){
		html+="<tr>";
		for(var c in data[r]){
			html+="<th>"+c.split("_").join(" ")+"</th>";
		}
		html+="</tr>";
		break;
	}
	for(var r in data){
		html+="<tr class='"+css_class+"'>";
		for(var c in data[r]){
			html+="<td>"+data[r][c]+"</td>";
		}
		html+="</tr>";
	}*/
	html+="<tr><th>&nbsp;</th><th>Producto</th><th>Cantidad</th><th>Precio unitario</th><th>Subtotal</th></tr>";
	var subtotal=0;
	var num_articulos=0;
	var iva=0;
	var total=0;
	
	for(var r in data){
		html+="<tr class='"+css_class+"'>";
		html+="<td><a onclick='del_articulo(this);return false;' data-rowid='"+r+"' class='btn btn-danger' href='#'>&times;</a></td>";
		html+="<td>"+productos_hash[data[r]["productos_id"]]['name']+"</td>";
		html+="<td><input onchange='upd_cantidad(this);' style='width:50px;' class='cantidad' type='number' value='"+data[r]["cantidad"]+"' data-rowid='"+r+"' /></td>";
		html+="<td>$<input onchange='upd_precio(this);' style='width:50px;' class='precio' type='number' value='"+data[r]["precio_unitario"]+"' data-rowid='"+r+"'/></td>";
		html+="<td>$"+data[r]["cantidad"]*data[r]["precio_unitario"]+"</td>";
		
		html+="</tr>";
		num_articulos+=data[r]["cantidad"]*1;
		subtotal+=data[r]["cantidad"]*data[r]["precio_unitario"];
	}
	$("#subtotal").text("$"+subtotal);
	$("#num_articulos").text(num_articulos);
	if($("#requiere_factura").val()-0){
		$("#iva_row").show();
		iva=subtotal*<?php echo floatval($result['clientes']['iva'])/100; ?>;
	}else{
		$("#iva_row").hide();
	}
	$("#iva").text("$"+(Math.round(iva*100)/100));
	total=subtotal+iva;
	$("#total").text("$"+(Math.round(total*100)/100));
	html+="</table>";
	$("#"+id).html(html);

	$("#"+id+" input").click(function(ev){ev.preventDefault();$(ev.currentTarget).focus().select();});
}

var articulos = <?php echo json_encode($result['ventas_articulos']); ?>;
var productos = <?php echo json_encode($result['productos']); ?>;
var productos_hash = <?php echo json_encode($result['productos_hash']); ?>;

draw_grid("tabla",articulos,"");

function del_articulo(el){
	delete articulos[el.dataset['rowid']];
	draw_grid("tabla",articulos,"");
}

function upd_precio(el){
	articulos[el.dataset['rowid']].precio_unitario=el.value;
	draw_grid("tabla",articulos,"");
}
function upd_cantidad(el){
	articulos[el.dataset['rowid']].cantidad=el.value;
	draw_grid("tabla",articulos,"");
}

function add_articulo(productos_id){
	var precio=0;
	if($("#termino_de_pago").val()=="1"){
		precio=productos_hash[productos_id]['precio_contado'];
	}else{
		precio=productos_hash[productos_id]['precio_credito'];
	}
	articulos.push({
		"id":"*",
		"ventas_id":"<?php echo $args['id']; ?>",
		"productos_id":productos_id,
		"cantidad":"1",
		"precio_unitario":precio,
		"notas":""
	});
	draw_grid("tabla",articulos,"");
}
function search_products(request,response){
	var result=[];
	var terms_orig=request.term.toLowerCase().split(" ");
	var terms=[];
	for(var i in terms_orig){
		if(terms_orig[i]!=''){
			terms.push(terms_orig[i]);
		}
	}
	
	for(var p in productos){
		var matched=0;
		for(var i in terms){
			 if(productos[p]['name'].toLowerCase().indexOf(terms[i])!=-1){
				 matched++;
			 }
		}
		var percent=0;
		if(terms.length>0){
			percent = matched/terms.length;
		}
		
		if(
			request.term==productos[p]['codigo_busqueda'].substr(0,request.term.length)
			|| productos[p]['name'].toLowerCase().indexOf(request.term.toLowerCase())!=-1
			|| percent==1
		){
			result.push({
				"label":productos[p]["codigo_busqueda"]+ " "+ productos[p]["name"],
				"value":productos[p]["id"],
				"desc":"<em>Contado: $"+productos[p]["precio_contado"]+" Credito: $"+productos[p]["precio_credito"]+"</em>"
			});
		}
	}
	response(result);
}

$(document).ready(function(){
	
 
    $( "#busqueda_de_articulo" ).autocomplete({
      minLength: 0,
      source: search_products,
      focus: function( event, ui ) {
        $( "#busqueda_de_articulo" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        add_articulo(ui.item.value);
		$( "#busqueda_de_articulo" ).val("");
        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.label + "<br>" + item.desc + "</div>" )
        .appendTo( ul );
    };
});

function termino_pagos_update(){
	if($("#termino_de_pago").val()=="1"){
		$("#campos_credito").slideUp();
	}else{
		$("#campos_credito").slideDown();
	}
}
$("#termino_de_pago").change(termino_pagos_update);
termino_pagos_update();

function guardar(){
	
	var has_pago=false;
	
	if(typeof $("#pago").attr("class")!=="undefined"){
		if($("#pago").attr("class").indexOf("in")!==-1){
			has_pago=true;
		}
	}
	
	
	
	if(!has_pago && $("#venta").attr("class").indexOf("in")==-1 ){
		alert("Nada que guardar!");
		return;
	}
	
	if(has_pago){
		// Con pago
		if($("#abono_recibido").val()-0==0){
			alert("No olvide capturar el abono!");
			return;
		}
	}else{
		if(typeof $("#pago").attr("class")!=="undefined"){
			// Sin pago
			if($("#abono_recibido").val()-0!=0){
				alert("Favor de quitar el abono!");
				return;
			}
		}
	}
	if($("#venta").attr("class").indexOf("in")!=-1){
		// Con venta
		if($("#termino_de_pago").val()==""){
			alert("No se olvide poner el termino de pago!");
			return;
		}
		if($("#total").text().trim()=='$0'){
			alert("La venta esta en $0!");
			return;	
		}
	}else{
		// Sin Venta
		
		if($("#total").text().trim()!='$0'){
			alert("Quite los articulos de la venta por favor!");
			return;	
		}
		$("#termino_de_pago").val("");
		
	}
	$("#btn_guardar").prop("disabled",true);
	$("#loading_guardar").show();
	$.ajax({
		"url":"?c=Ventas&m=add_products&_data=json",
		"method":"POST",
		"data":{
			"c":"ventas",
			"m":"add_products",
			"requiere_factura":$("#requiere_factura").val(),
			"id":"<?php echo $args['id'];?>",
			"articulos":JSON.stringify(articulos),
			"fecha_promesa":$("#fecha_de_promesa").val(),
			"termino_pagos_id":$("#termino_de_pago").val(),
			"notas":$("#notas").val(),
			"abono_recibido":$("#abono_recibido").val(),
			"abono_nota":$("#notas_de_abono").val(),
			"subtotal":$("#subtotal").text(),
			"iva":$("#iva").text(),
			"total":$("#total").text(),
			"action":"Guardar"
		},
		"success":function(data){
			try{
				var o=JSON.parse(data ); 
				//test
				alert(o.message);
				window.location.href="?";
			}catch(e){
				prompt("Error:",e);
				$("#btn_guardar").prop("disabled",false);
				$("#loading_guardar").hide();
			}
		},
		"error":function(){
			prompt("Error de conexion","");
			$("#btn_guardar").prop("disabled",false);
			$("#loading_guardar").hide();
		}
	});
	
	
}
<?php if(!$result['clientes']['mapa_url']){?>

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(storePosition,showError);
  }
}
function showError(error) {
  switch(error.code) {
    case error.PERMISSION_DENIED:
      alert("Necesita permitir el acceso a la ubicacion en el telefono para registrarla con esta funcion!");
      break;
    case error.POSITION_UNAVAILABLE:
      alert("La informacion de ubicacion no esta disponible. Intentar mas tarde.");
      break;
    case error.TIMEOUT:
      alert("Tomo demasiado tiempo para registrar la ubicacion. Intentar mas tarde.");
      break;
    case error.UNKNOWN_ERROR:
      alert("Ocurrio un error al intentar obtener la ubicacion.");
      break;
  }
}
function storePosition(position) {
  $.ajax({
		"url":"?c=Clientes&m=store_position&_data=json",
		"method":"POST",
		"data":{
			"c":"Clientes",
			"m":"store_position",
			"id":"<?php echo $result['clientes']['id'];?>",
			"latitude":position.coords.latitude,
			"longitude":position.coords.longitude
		},
		"success":function(data){
			try{
				var o=JSON.parse(data ); 
				//test
			}catch(e){
				prompt("Error:",e);
			}
		},
		"error":function(){
			prompt("Error de conexion","");
		}
	});
  
}

if(confirm("Se encuentra actualmente en la ubicacion del cliente?")){
	getLocation();
}
<?php } ?>
</script>