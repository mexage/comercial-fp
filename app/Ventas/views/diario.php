<style>
#economico th {
  position: sticky !important;
  top: 0;
  z-index: 10;
  background-color: white;
  text-align:center;
  border-top:2pt solid gray !important;
  border-bottom:2pt solid gray !important;
}
#economico table > tbody > tr:last-child { 
		background:#ff0000; 
		background-color: white;
	  text-align:center;
	  border-top:2pt solid gray !important;
	  border-bottom:2pt solid gray !important;
	  font-weight:bold;
}
</style>

	<h1>Reporte Venta Diaria</h1>

		<form  action="?c=ventas&m=diario" method="GET"> 
			<br />
			
			<?php
				if(!$args['fecha'])$args['fecha']=date("Y-m-d");
				echo php_Field::hidden("c",$args['c']);
				echo php_Field::hidden("m",$args['m']);
				echo php_Field::date("Fecha",$args['fecha']);
				
				echo php_Field::combobox("Repartidor",$result['repartidores'],(isset($args['repartidor']))?$args['repartidor']:"");		

				echo php_Field::hidden("action","Seleccionar");
			
			?>
			<br />
			<script>
			$("#fecha").change(function(){$("#fecha").closest("form").submit();});
			$("#repartidor").change(function(){$("#repartidor").closest("form").submit();});
			</script>
		</form>
		

<?php


	$ventas_cliente = array();
	$ventas_desglosada = array();

	foreach($result['ventas_cliente'] as $data){
		$ventas_cliente[] = array (
			//"id" => $data['id'],
			"clientes_id" => $data['clientes_id'],
			"created" => $data['created'],
			"created_by__users_id" => $data['created_by__users_id'],
			"fecha" => $data['fecha'],
			"estatus_ventas_id" => $data['estatus_ventas_id'],
			"notas" => $data['notas'],
			"fecha_de_promesa" => $data['fecha_promesa'],
			"termino_pagos_id" => $data['termino_pagos_id'],
			"repartidor__users_id" => $data['repartidor__users_id'],
			/*"Action" => "<a href='?c=Ventas&m=view&id=".$data['id']."' class='btn btn-danger' role='button'>
			<span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>
			View
			</a>"*/
			
		);
	}


	foreach($result['ventas_desglosada'] as $data){
		
		//print_r ( $result['ventas_desglosada'] );
		//exit;
		
		$subtotal    = $data['cantidad'] * $data['precio_unitario'] ;
		$tmp_iva     = ( $result['clientes'][$data['clientes_id']]['iva'] ) * $subtotal / 100 ;
		$tmp_importe = $subtotal + $tmp_iva ;  
		 
		$ventas_desglosada[] = array (
			//"id" => $data['id'],
			"clientes_id" => $data['clientes_id'],
			"created" => $data['created'],
			"created_By" => $data['created_by__users_id'],
			"fecha" => $data['fecha'],
			"estatus_ventas_id" => $data['estatus_ventas_id'],
			//"notas " => $data['notas'],
			"fecha_promesa" => $data['fecha_promesa'],
			"termino_pagos_id" => $data['termino_pagos_id'],
			"repartidor__users_id" => $data['repartidor__users_id'], 
			//"ventas_id"
			"productos_id" => $data['productos_id'],
			"cantidad" => $data['cantidad'],
			"precio_unitario" => $data['precio_unitario'],
			"subtotal" => $subtotal,
			"IVA" =>  $tmp_iva ,
			"importe" => $tmp_importe
			
		//	"Action" => "<a href='?c=".$data['object']."&m=view&id=".$data['object_key']."' class='btn btn-danger' role='button'>
		//   <span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>
		//	View
		// </a>"
		);
	}




?>



<br />
<div class='panel panel-info'>
<div class='panel-heading'>
<h4 class='panel-title'><a data-toggle='collapse' href='#economico'>Resumen Economico</a></h4>
</div>
<div id='economico' class='panel-collapse collapse in'>
<div class='panel-body' style=''>
<?php
echo php_Chart::copy_button("#resumen_economico","Copiar");	
?>
<div id='resumen_economico'>
<?php
echo php_Grid::draw( $result['economico'] );	
?>
</div>
</div>
</div>
</div>




<br />
<div class='panel panel-info'>
<div class='panel-heading'>
<h4 class='panel-title'><a data-toggle='collapse' href='#cliente'>Venta por Cliente</a></h4>
</div>
<div id='cliente' class='panel-collapse collapse'>
<div class='panel-body' style='overflow:auto;'>
<?php
echo php_Chart::copy_button("#venta_por_cliente","Copiar");	
?>
<div id='venta_por_cliente'>
<?php
echo php_Grid::draw($ventas_cliente,array("View:danger:eye-open"=>"?c=Ventas&m=view&id=<?=id?>"));	
?>
</div>
</div>
</div>
</div>

		
		

<div class='panel panel-info'>
<div class='panel-heading'>
<h4 class='panel-title'><a data-toggle='collapse' href='#desglosada'>Ventas Deglosada</a></h4>
</div>
<div id='desglosada' class='panel-collapse collapse'>
<div class='panel-body' style='overflow:auto;'>
<?php
echo php_Chart::copy_button("#ventas_desglosada","Copiar");	
?>
<div id='ventas_desglosada'>
<?php
echo php_Grid::draw( $ventas_desglosada );	
?>
</div>
</div>
</div>
</div>








