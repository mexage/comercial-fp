<?php
	
	// Table name to get from $result[$table_name].
	// If $result[$table_name] is not set, then it will take $args as data row
	// $table_name = strtolower($args['c']);
	
	// Human name of the table.
	// $human_name = ucwords(trim(str_replace("_"," ",$table_name)));
	
	// Class name
	// $class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));
	
	// Middle buttons
	//$links = array(
	//	"List"=>"?c=".$class_name."",		
	//	"View"=>"?c=".$class_name."&m=view&id=< ?=id>",		
	//);
	
	// Actions
	//$actions=array(
	//	"Save"=>"primary"
	//);
	
	// Field types
	//$field_type=array(
	//	"id"=>"readonly"
	//);
	
	// Dropdown tables
	// $f_tables=array(
	//		"active"=>"states"
	//);
	// $result['states']=array(
	//	array("name"=>"Yes","id"="1"),
	//	array("name"=>"No","id"="2"),
	// );		

	
	// Hidden
	//$hidden = array(
	//		"created"
	//);
	
	// Slave tables (in the form of ["linkfield table"]
	//if(!isset($slave_tables)){
	//	$slave_tables=array();
	//}	
	
	unset($result['slave_tables']['ventas estatus_ventas_id']);
	$result['slave_tables']['ventas_articulos ventas_id']['dynamic']='jExcel';
	$result['slave_tables']['ventas_articulos ventas_id']['jExcel_configuration']=array(
		"columns"=>array(
			array(
				"name"=>"id",
				"type"=>"hidden",
			),
			array(
				"name"=>"ventas_id",
				"type"=>"hidden",
			),
			array(
				"name"=>"productos_id",
				"type"=>"dropdown",
				"title"=>"Producto",
				"url"=>"?c=Productos&m=_list&_data=json",
				"autocomplete"=>true,
			),
			array(
				"name"=>"cantidad",
				"type"=>"number",
				"title"=>"Cantidad",
			),
			
			array(
				"name"=>"precio_unitario",
				"type"=>"number",
				"title"=>"Precio Unitario",
				"mask"=>"$#,##",
				"decimal"=>"."
			),
			array(
				"name"=>"notas",
				"type"=>"text",
				"title"=>"Notas",
			),
		),
		"defaults"=>array(
			"ventas_id"=>$args['id']
		)
	);
	
	// Default view "app/_default/form.php"
	// This view will take all previous parameters and render the assigned table.
	include $vista_default;	
