<?php
	
	// Table name to get from $result[$table_name].
	// If $result[$table_name] is not set, then it will take $args as data row
	// $table_name = strtolower($args['c']);
	
	// Human name of the table.
	// $human_name = ucwords(trim(str_replace("_"," ",$table_name)));
	
	// Class name
	// $class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));
	
	// Middle buttons
	$links = array(
		"Pantalla inicial"=>"?",		
	//	"View"=>"?c=".$class_name."&m=view&id=< ?=id>",		
	);
	
	// Actions
	//$actions=array(
	//	"Save"=>"primary"
	//);
	
	// Field types
	//$field_type=array(
	//	"id"=>"readonly"
	//);
	$field_type['clientes_id']='combobox';
	$field_type['notas']='textarea';
	$field_type['repartidor__users_id']='combobox';
	$field_type['fecha']='hidden';
	
	if(!$args['repartidor__users_id']){
		$args['repartidor__users_id']=$user->attr("id");
	}
	
	// Dropdown tables
	// $f_tables=array(
	//		"active"=>"states"
	//);
	// $result['states']=array(
	//	array("name"=>"Yes","id"="1"),
	//	array("name"=>"No","id"="2"),
	// );		

	
	// Hidden
	//$hidden = array(
	//		"created"
	//);
	
	$pass_args=array(
		//"repartidor__users_id"=>$user->attr("id"),
		"fecha"=>date("Y-m-d"),
		
	);
	
	$hidden[]="id";
	$hidden[]="created";
	$hidden[]="created_by__users_id";
	$hidden[]="fecha_promesa";
	
	$hidden[]="termino_pagos_id";
	//$hidden[]="estatus_ventas_id";
	$hidden[]="requiere_factura";
	
	
	
	// Slave tables (in the form of ["linkfield table"]
	//if(!isset($slave_tables)){
	//	$slave_tables=array();
	//}	
	
	
	
	// Default view "app/_default/form.php"
	// This view will take all previous parameters and render the assigned table.
	include $vista_default;	
?>
<script>
$(document).ready(function(){
	$("#clientes_id").val("<?php echo $args['clientes_id'];?>").change();
	$("#clientes_id").closest("td").find("input").val($("#clientes_id option:selected").text()).change();
});
</script>