<div class='row'>
	<div class='col-md-12'>
		<h1>Ventas</h1>
		<?php echo php_Chart::div('ventas_semana');?>
	</div>
</div>
<div class='row'>
	<div class='col-md-4'>
		<h3>Productos mas vendidos (<?php echo date("Y-m");?>)</h3>
		<?php echo php_Chart::div('productos');?>
	</div>
	<div class='col-md-4'>
		<h3>Clientes con mas ventas credito (<?php echo date("Y-m");?>)</h3>
		<?php echo php_Chart::div('clientes_credito');?>
	</div>
	<div class='col-md-4'>
		<h3>Clientes con mas ventas contado (<?php echo date("Y-m");?>)</h3>
		<?php echo php_Chart::div('clientes_contado');?>
	</div>
</div>
<?php

echo php_Chart::ajax("?c=Ventas&m=json&_data=json",
	php_Chart::javascript('ventas_semana','{"cols":["semana"],"rows":["dia_semana"],"vals":["importe_total"],"inclusions":{},"exclusions":{},"rendererName":"Stacked Bar Chart","aggregatorName":"Integer Sum","rendererOptions":{"localeStrings":{"renderError":"An error occurred rendering the PivotTable results.","computeError":"An error occurred computing the PivotTable results.","uiRenderError":"An error occurred rendering the PivotTable UI.","selectAll":"Select All","selectNone":"Select None","tooMany":"(too many to list)","filterResults":"Filter values","apply":"Apply","cancel":"Cancel","totals":"Totals","vs":"vs","by":"by"}}}',false)
	.
	php_Chart::javascript('productos','{"cols":[],"rows":["producto"],"vals":["cantidad"],"inclusions":{"mes":"'.date("Y-m").'"},"exclusions":{"producto":["CARGA INICIAL"]},"rendererName":"Table","aggregatorName":"Integer Sum","rendererOptions":{"sort":{ "direction" : "desc", "col_totals" : [ "0" ]},"localeStrings":{"renderError":"An error occurred rendering the PivotTable results.","computeError":"An error occurred computing the PivotTable results.","uiRenderError":"An error occurred rendering the PivotTable UI.","selectAll":"Select All","selectNone":"Select None","tooMany":"(too many to list)","filterResults":"Filter values","apply":"Apply","cancel":"Cancel","totals":"Totals","vs":"vs","by":"by"}}}',false)
	.
	php_Chart::javascript('clientes_credito','{"cols":[],"rows":["cliente"],"vals":["importe_total"],"inclusions":{"mes":"'.date("Y-m").'","termino_pago":"Credito"},"exclusions":{},"rendererName":"Table","aggregatorName":"Sum","rendererOptions":{"sort":{ "direction" : "desc", "col_totals" : [ "0" ]},"localeStrings":{"renderError":"An error occurred rendering the PivotTable results.","computeError":"An error occurred computing the PivotTable results.","uiRenderError":"An error occurred rendering the PivotTable UI.","selectAll":"Select All","selectNone":"Select None","tooMany":"(too many to list)","filterResults":"Filter values","apply":"Apply","cancel":"Cancel","totals":"Totals","vs":"vs","by":"by"}}}',false)
	.
	php_Chart::javascript('clientes_contado','{"cols":[],"rows":["cliente"],"vals":["importe_total"],"inclusions":{"mes":"'.date("Y-m").'","termino_pago":"Contado"},"exclusions":{},"rendererName":"Table","aggregatorName":"Sum","rendererOptions":{"sort":{ "direction" : "desc", "col_totals" : [ "0" ]},"localeStrings":{"renderError":"An error occurred rendering the PivotTable results.","computeError":"An error occurred computing the PivotTable results.","uiRenderError":"An error occurred rendering the PivotTable UI.","selectAll":"Select All","selectNone":"Select None","tooMany":"(too many to list)","filterResults":"Filter values","apply":"Apply","cancel":"Cancel","totals":"Totals","vs":"vs","by":"by"}}}',false)
);