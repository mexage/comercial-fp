<div class='row'>
<div class='col-md-6'>
	<?php $id="panel"; // TODO: Change title ?>
	<div class='panel panel-success'>
		<div class='panel-heading'>
			<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' >Venta</a></h4>
		</div>
		<?php //echo php_Field::start_form("",$args['m'],$args,"POST"); ?>
		<div id="<?php echo $id; ?>" class='panel-collapse collapse'>
			<div class='panel-body' style='overflow:auto;'>
			<?php
				//echo php_Field::hidden('c',$args['c']);
				//echo php_Field::hidden('m',$args['m']);

				// TODO: Add content
				
			?>
			</div>
		</div>
		<div class='panel-footer'>

		</div>
		<?php //echo php_Field::end_form(); ?>
	</div>
</div>
<div class='col-md-6'>
	<?php $id="customer"; // TODO: Change title ?>
	<div class='panel panel-primary'>
		<div class='panel-heading'>
			<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' ><?php echo $result['clientes']['name']; ?></a></h4>
		</div>
		<div id="<?php echo $id; ?>" class='panel-collapse collapse in'>
			<div class='panel-body' style='overflow:auto;'>
			<?php

				
					
			?>
			</div>
		</div>
		<div class='panel-footer'>

		</div>
	</div>
</div>
</div>
<script>
function draw_grid(id,data,css_class){
	if(typeof css_class==="undefined"){
		css_class="";
	}
	var html="<table class='table table-striped'>";
	
	for(var r in data){
		html+="<tr>";
		for(var c in data[r]){
			html+="<th>"+c.split("_").join(" ")+"</th>";
		}
		html+="</tr>";
		break;
	}
	for(var r in data){
		html+="<tr class='"+css_class+"'>";
		for(var c in data[r]){
			html+="<td>"+data[r][c]+"</td>";
		}
		html+="</tr>";
	}
	
	html+="</table>";
	$("#"+id).html(html);
}
</script>