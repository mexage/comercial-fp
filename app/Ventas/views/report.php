<?php

if(!$args['query']){
	$args['query']='{"cols":["semana","dia_semana"],"rows":["cliente"],"vals":["importe_total"],"inclusions":{},"exclusions":{},"rendererName":"Heatmap","aggregatorName":"Sum","rendererOptions":{"localeStrings":{"renderError":"An error occurred rendering the PivotTable results.","computeError":"An error occurred computing the PivotTable results.","uiRenderError":"An error occurred rendering the PivotTable UI.","selectAll":"Select All","selectNone":"Select None","tooMany":"(too many to list)","filterResults":"Filter values","apply":"Apply","cancel":"Cancel","totals":"Totals","vs":"vs","by":"by"}}}';
}

include $vista_default;