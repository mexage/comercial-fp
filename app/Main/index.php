<?php
class Main extends php_Controller{
	function __construct($args){
		parent::__construct($args);
		$this->title="Comercial FP - Sistema de administraci&oacute;n";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		
		$result=array();
		
		/*if(isset($args['call']) &&isset($args['name'])){
			$this->db->insert("calls",array("name"=>addslashes($args['name']),"phone"=>addslashes($args['call']),"created_by__users_id"=>$this->user->attr("id"),"ip"=>$_SERVER['REMOTE_ADDR']." ".$_SERVER['HTTP_X_FORWARDED_FOR']));
			return "?";
		}*/			
		
		$result['clientes']=$this->db->search("clientes");
		if($this->user->attr("id")){
			$repartidor=$this->user->attr("id");
			if(isset($args["repartidor"]))$repartidor=$args["repartidor"];
			$result['ventas_pendientes']=$this->db->query("
				SELECT 
					v.*,
					c.mapa_url
				FROM ventas v
				INNER JOIN clientes c on v.clientes_id=c.id
				WHERE v.repartidor__users_id='".$repartidor."' 
				AND v.estatus_ventas_id in(0,1,2)");
		
			if(!count($result['ventas_pendientes'])){
				$this->set_message("No hay ventas pendientes para el repartidor ".$repartidor,"warning");
				return "?c=Rutas&repartidor=".$repartidor;
			}
			
		}
		
		return $result;
	}
	function repost($args){
		if(!isset($_SESSION['repost'])){
			//return "?";
		}
	}
	function login($args){
        
        if($args['login']){
			if(strcasecmp(substr($args['login'],0,14),"Quick login as")==0){				
				$args['user']=php_AD::get_sso_user();
				$args['pwd']="";
			}
			if($this->user->login($args['user'],$args['pwd'])){
				$args['c']=$args['requested_c'];
				$args['m']=$args['requested_m'];
				unset($args['requested_c']);
				unset($args['requested_m']);
				unset($args['user']);
				unset($args['pwd']);
				unset($args['login']);
				if($this->user->attr("change_password")){
					$this->set_message("Debe cambiar su contraseña.","info");
					return "?m=account_details";
				}else{
					$_SESSION['repost']=$args;
					return "?c=main&m=repost";
				}
			}else{
				return array("message"=>"Su usuario y/o contraseña son incorrectos.");
			}
		
        }
        return array();
    }
	function logout($args){
        $this->user->logout();
        return "?c=".$args['requested_c']."&m=".$args['requested_m'];
	}
	function change_password($args){
		$hash=sha1($args['user'].date("Ymd").getConfig("SECURITY/SALT"));
		if(($this->user->is_logged()||strcasecmp($args['hash'],$hash)==0) && $args['action']){			
			if(strcmp($args['nuevo_password'],$args['confirmar_password'])==0){
				$upd=array();
				$upd["pass_hash"]=sha1(getConfig("SECURITY/SALT").$args['nuevo_password']);
				$upd["change_password"]=0;
				if($this->user->is_logged()){
					$usuario=$this->user->attr("id");
				}else{
					$usuario=$args['user'];
				}
				if(count($this->db->upd("users",$usuario,$upd))){
					$this->set_message("Se cambió la contraseña!","success");
				}else{
					$this->set_message("No se cambió la contraseña!","danger");
				}								
				return "?c=".$args['requested_c']."&m=".$args['requested_m'];

			}
		}else if(strcasecmp($args['hash'],$hash)!=0){
			return "?";
		}
	}
	function self_register($args){
		$self_registration=array();
		$fields=array("correo","nombres","apellidos","telefono","fraccionamiento","calle","numero","action");
		
		foreach($fields as $f){
			if(!trim($args[$f])){
				trace("Exit, no fields.");
				$this->set_message("Todos los campos son obligatorios.","danger");
				return "?";
			}
			$self_registration[$f]=$args[$f];
		}
		unset($self_registration["action"]);
		$self_registration['ip']=$_SERVER['REMOTE_ADDR'];
		$this->db->insert("self_registrations",$self_registration);
		

		$user=$this->db->get("users",addslashes($args['correo']));
		
		if($user['id']){
			trace("Exit, email already exists.");
			$this->set_message("Su correo ya había sido registrado previamente. Utilice la opción para recuperar contraseñas.","warning");
			return "?";
		}
		trace("Success!");
		$this->set_message("Se recibió su solicitud de manera exitosa. Esté al pendiente de su correo (Revise su SPAM).","success");
		@mail("soporte@accesamo.com","Solicitud de registro.","Se recibió la siguiente solicitud: \n".var_export($self_registration));
		return "?";

	}
	function forgot_password($args){
		$hash=sha1($args['correo'].date("Ymd").getConfig("SECURITY/SALT"));
		if($args["action"]){			
			$person=$this->db->get("users",$args["correo"]);
			if(!$person){
				$this->set_message("Verifique su información.","warning");
				return "?m=forgot_password&correo=".$args["correo"];
			}
			$envio=@mail($args["correo"],"Registro de control de accesos - Accesamo","

Hola ".$person["name"].":
            
Se solicitó reiniciar su contraseña.
            
Vaya a https://mexage.com.mx/cherry/?m=change_password&user=". $args['correo']."&hash=".$hash." y cambie su contraseña.
            
Cualquier duda estamos a sus ordenes en soporte@accesamo.com
            
https://www.accesamo.com
            ",
                "From: Control de Acceso - Accesamo <soporte@accesamo.com> \r\n".
                "Bcc: soporte@accesamo.com \r\n"
                );
			if($envio){
				$this->set_message("Se envió un correo con instrucciones.","success");
				return "?";
			}else{
				$this->set_message("Ocurrió un error. Vuelva a intentarlo más tarde.","danger");
				return "?m=forgot_password&correo=".$args["correo"];
			}
		}
	}
	public function test($args){

	}
}