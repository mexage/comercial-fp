<?php 
if(!isset($args['requested_c'])){
	$args['requested_c']=$args['c'];
}
if(!isset($args['requested_m'])){
	if(strcasecmp($args['m'],"login")!=0){
		$args['requested_m']=$args['m'];
	}
}

?>
<script>
$(document).ready(function(){
	$("footer a:contains(Login)").hide();
});
</script>
<div class='box-section'>
<div class='row'>
	<!--<div class='col-lg-4'></div>-->
	<div class='col-lg-12'>
		<form action="?c=main&m=login&requested_c=<?php echo $args['requested_c']; ?>&requested_m=<?php echo $args['requested_m']; ?>" method="POST">
		
			<input type='hidden' name='c' value='main' />
			<input type='hidden' name='m' value='login' />
			
			<input type='hidden' name='requested_c' value='<?php echo $args['requested_c']; ?>' />
			<input type='hidden' name='requested_m' value='<?php echo $args['requested_m']; ?>' />
			<?php 
			foreach($args as $k=>$v){
				if(
					strcasecmp($k,"c")!=0
					&&strcasecmp($k,"m")!=0
					&&strcasecmp($k,"requested_c")!=0
					&&strcasecmp($k,"requested_m")!=0
				
				){
					echo "<textarea name='".$k."' style='display:none;'>".$v."</textarea>";
				}
			}
			?>
			<h1>Ingreso al Sistema</h1>
			<?php 
				if(isset($result['message'])){
					?>
					<div class="alert alert-danger">
						<?php echo $result['message']; ?>
					</div>
					<?php
				}
			?>
			<p>Ingrese su usuario y contraseña:</p>
			<div class="form-group">
				<label for="user">Usuario:</label>
				<input type="text" class="form-control" name='user' id="user" value='<?php echo $args['user'];?>'>
			</div>
			<div class="form-group">
				<label for="pwd">Contraseña:</label>
				<input type="password" name='pwd' class="form-control" id="pwd">
			</div>
			<input type="submit" class="btn btn-primary" name="login" value="Login" />
			<p></p>
			<p><a href='?m=forgot_password'>¿Olvidó su contraseña?</a></p>
			<?php
				if($sso_user=php_AD::get_sso_user()){
					$user_no_domain=explode("\\",php_AD::get_sso_user());
					
					echo '<input type="submit" class="btn btn-success" name="login" value="Quick login as '.$user_no_domain[count($user_no_domain)-1].'" />';
				}
			?>
			<script>$(document).ready(function(){
				$("footer").hide();
			});</script>
		</form>
	</div>
	<!--<div class='col-lg-4'></div>-->
</div>
</div>
