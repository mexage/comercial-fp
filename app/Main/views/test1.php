<style>
	#icons .well{
		/*width:250px;
		height:250px;*/
		width:100%;
		height:0;
		padding-bottom:100%;
		
	}
	#icons .well img{
		/*max-height:150px;
		max-width:130px;
		height:auto;*/
		max-width:100%;
		height:auto;
		
	}
	
	/* select with custom icons */
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item-wrapper {
      padding: 0.5em 0 0.5em 3em;
    }
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item .ui-icon {
      height: 24px;
      width: 24px;
      top: 0.1em;
    }
    .ui-icon.printer {
		background: url("images/andon/printer.png") 0 0 no-repeat;
	}
</style>

<div class="row content-fluid" id='icons'>
	<div id='device' class="col-md-12"> 
		<div class='row'>
			<?php 
			$n=1;
			foreach($result['devices'] as $device){
				echo "<div class='col-xs-2'>";
				com_Device::button($device,array(
					"data-toggle"=>"modal",
					"data-target"=>"#report",
					"onclick"=>'GenerateReport("device-'.$device['id'].'"); return false;'
				));
				echo "</div>";
				if($n%6==0){
					echo "</div><div class='row'>";
				}
				$n++;
			}
			?>
			
		</div>
	</div>	
</div>
<div class="row content-fluid" id='icons'>
	<div id='issues' class="col-md-12" style='display:none;'>
		<div class='btn-group-vertical'>
			<button class="btn btn-primary">Hojas atoradas</button>
			<button class="btn btn-primary">No hay toner</button>
			<button class="btn btn-primary">La hoja sale borrosa o manchada</button>
			<button class="btn btn-primary">La información sale incorrecta</button>
		</div>
	</div>
</div>

<div class="row content-fluid">
	<div class="col-md-12"> 
		<div class="row">
			<div class="col-lg-6">
				<div class='panel panel-danger'>
					<div class='panel-heading'>					
						<div class='btn-group'>
							<button class='btn btn-default'>#50080</button>												
							<button class='btn btn-danger'>Reportado</button>
							<button class='btn btn-default'>Tiempo total: <span id='a_total'>-</span></button>
						</div>
					</p>
					</div>
					<div class='panel-content'>
						<div class='media'>
							<div class='media-left'>
								<div class='well'>
									<img src='images/andon/zebra.png' style='max-width:200px;height:auto;' />
								</div>
							</div>
							<div class='media-body'>
								<h4 class='media-heading'>Zebra - La etiqueta sale desfasada</h4>
								<div class='media'>
									<table class='table table-condensed table-hover'>
										<tr>
											<th>Equipo</th>
											<th>Mensaje</th>
											<th>Tiempos</th>
											<th>Acciones</th>
										</tr>
										
										<tr>
											<td><h3>IT</h3><span class="badge">Remoto</span></td>
											<td><pre style='width:210px;max-height:100px;overflow-y:auto;' class='text-left chatbox'></pre>
											
											<form action="?c=Alerts&amp;m=responder&amp;id=45308&amp;areas_id=8&amp;location=TESTING" method="POST"><input type="text" name="message" style="width:120px;"><input class="btn btn-info" type="submit" value="Enviar"></form>
											
											</td>
											<td>Tiempos</td>
											<td>							
												<div class='btn-group'>
													<button class='btn btn-warning'>Revisando</button>
												</div>
											</td>
										</tr>
									</table>
								</div>								
							</div>
						</div>
					</div>
					<div class='panel-footer'>
						<div class='btn-group'>
							<button class='btn btn-default' data-toggle="modal" data-target="#close" onclick='GenerateSolution("impresora"); return false;'>Falsa Alarma</button>
							<button class='btn btn-primary'>Agregar a otro equipo</button							
						</div>						
					</div>
					
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
	S.add("a_total","2020-02-06 00:00:00");
</script>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#report">Open Modal</button>

<!-- Modal -->
<div id="report" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Selecciona un problema</h4>
      </div>
      <div class="modal-body">
		<div class="media" id='problemas-device-1'>
			<div class="media-left">
				<img src="images/andon/printer.png" class="media-object" style="width:200px">
			</div>
			<div class="media-body">
				<h4 class="media-heading">Impresora</h4>
				<div class='btn-group-vertical' id='printer'>
					<button class="btn btn-danger">Hojas atoradas</button>
					<button class="btn btn-danger">No hay toner / hojas</button>
					<button class="btn btn-danger">La hoja sale borrosa o manchada</button>
					<button class="btn btn-danger">La información sale incorrecta</button>
					<button class="btn btn-danger">Otro</button>
				</div>
			</div>
			
		</div>
		
		<div class="media" id='problemas-device-9'>
			<div class="media-left">
				<img src="images/andon/zebra.png" class="media-object" style="width:200px">
			</div>
			<div class="media-body">
				<h4 class="media-heading">Zebra</h4>
				<div class='btn-group-vertical' id='zebra'>
					<button class="btn btn-danger">No hay etiquetas / ribbon</button>
					<button class="btn btn-danger">La etiqueta sale desfasada</button>
					<button class="btn btn-danger">La etiqueta sale borrosa o manchada</button>
					<button class="btn btn-danger">La información sale incorrecta</button>
					<button class="btn btn-danger">Otro</button>
				</div>
			</div>
			
		</div>
		
        
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary">Enviar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="close" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cierre de Andon</h4>
      </div>
      <div class="modal-body">
		<div class="media" id='cierre-device-1'>
			<div class="media-left">
				<img src="images/andon/printer.png" class="media-object" style="width:200px">
			</div>
			<div class="media-body">
				<h4 class="media-heading">Impresora</h4>
				<div class='btn-group-vertical' id='printer'>
					<button class="btn btn-danger">Hojas atoradas</button>
					<button class="btn btn-danger">No hay toner / hojas</button>
					<button class="btn btn-danger">La hoja sale borrosa o manchada</button>
					<button class="btn btn-danger">La información sale incorrecta</button>
					<button class="btn btn-danger">Otro</button>
				</div>
			</div>
			
		</div>
		
		<div class="media" id='cierre-device-9'>
			<div class="media-left">
				<img src="images/andon/zebra.png" class="media-object" style="width:200px">
			</div>
			<div class="media-body">
				<h4 class="media-heading">Zebra</h4>
				<div class='btn-group-vertical' id='zebra'>
					<button class="btn btn-danger">No hay etiquetas / ribbon</button>
					<button class="btn btn-danger">La etiqueta sale desfasada</button>
					<button class="btn btn-danger">La etiqueta sale borrosa o manchada</button>
					<button class="btn btn-danger">La información sale incorrecta</button>
					<button class="btn btn-danger">Otro</button>
				</div>
			</div>
			
		</div>
		
        
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary">Enviar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>

<?php 

com_Device::dropdown("device",$result['devices']);
?>


<script>
function GenerateReport(id){
	$("#report .media").hide();
	$("#report #problemas-"+id).show();
}
function GenerateSolution(id){
	$("#close .media").hide();
	$("#close #cierre-"+id).show();
}
</script>
<?php

?>