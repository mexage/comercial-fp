<div id="app">
{{ message }}
</div>
<div id="app2">
<span class='btn btn-primary' v-bind:title="message">Try hover</span>
</div>

<script>
var app = new Vue({
	el: '#app',
	data:{
		message:"Hello"
	}

});

var app2 = new Vue({
	el: '#app2',
	data:{
		message:'You loaded this page on ' + new Date().toLocaleString()
	}

});
</script>