<h1>Nueva ubicaci&oacute;n</h1>

<div class="panel panel-warning">
	<div class="panel-heading text-left">
		<h4>
			En aprobación
		</h4>
	</div>
	<div class='panel-body'>
		<p>La ubicación <?php echo $args['location'];?> está en aprobación.</p>
		<p>Para cualquier duda favor de contactar a Guillermo Morales (ext 8645)</p>
		<a class='btn btn-success' href="?<?php echo "areas_id=".$args['areas_id']."&location=".$args['location'];?>">Revisar estatus</a>
	</div>
</div>