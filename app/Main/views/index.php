<?php 
if(!$user->is_logged()){
	?>
	<div class="row">
		<div class="col-md-6">
			<?php
			require "app/Main/views/login.php";
			?>
		</div>
		<div class="col-md-6">        
			<img style='width:100%' src='images/fp/logo2.png' />
		</div>
	</div>
<?php
}else{
	
	?>
<?php $id="customer"; // TODO: Change title ?>
<div class='panel panel-success'>
	<div class='panel-heading'>
		<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' >Acciones Cliente <span class='badge'>Nuevo!</span></a></h4>
	</div>
	<div id="<?php echo $id; ?>" class='panel-collapse collapse'>
		<div class='panel-body' style='overflow:auto;'>
		<form action='?c=Clientes&m=shortcut' method='POST'>
		<?php
			echo php_Field::hidden("c","Clientes");
			echo php_Field::hidden("m","shortcut");
			echo php_Field::combobox("Clientes ID",$result['clientes'],"");
			echo php_Field::submit("action","Venta Imprevista");
			echo php_Field::submit("action","Ver Cliente");
		?>
		</form>
		<script>
		$(document).ready(function(){
			$("#clientes_id").val("").change();
			$("#clientes_id").closest("form").find("input.custom-combobox-input").val("").change();
		});
		</script>
		</div>
	</div>
	<!--<div class='panel-footer text-right'>
	</div>-->

</div>
	<?php
	
	if(count($result['ventas_pendientes'])){
?>
<?php $id="route"; // TODO: Change title ?>
<div class='panel panel-<?php echo (isset($args['repartidor']))?"warning":"primary"?>'>
	<div class='panel-heading'>
		<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' ><?php echo (isset($args['repartidor']))?"Ruta de ".$args['repartidor']:"Mi Ruta";?></a></h4>
	</div>
	<div id="<?php echo $id; ?>" class='panel-collapse collapse in'>
		<div class='panel-body' style='overflow:auto;'>
		<?php
			echo php_Grid::draw($result['ventas_pendientes'],
				array(
					"Ir:primary:road"=>"onclick:open_map(\"<?=mapa_url?>\",\"<?=clientes_id?>\",\"<?=id?>\");",
					"Vender:success:list-alt"=>"?c=ventas&m=add_products&id=<?=id?>",
					"Cancelar:danger:remove"=>"?c=rutas&m=cancel&id=<?=id?>",
				),
				array("created","created_by__users_id","fecha","fecha_promesa","termino_pagos_id","repartidor__users_id","mapa_url","requiere_factura") 
			);
		?>
		</div>
	</div>
	<div class='panel-footer text-right'>
	<a class='btn btn-warning' href='?c=Rutas'><span class="glyphicon glyphicon-road"></span> Administrar rutas</a> 
	<a class='btn btn-primary' href='?c=Ventas&m=add'>Venta imprevista</a>
	</div>

</div>
<script>
function open_map(url,clientes_id,ventas_id){
	$.ajax({
		"url":"?c=Ventas&m=edit&_data=json",
		"method":"POST",
		"data":{"c":"Ventas","m":"edit","id":ventas_id,"estatus_ventas_id":"1","action":"Save"},
		"success":function(data){
			try{
				//var o=JSON.parse(data);
				if(url==""){
					window.open("?c=clientes&m=edit&id="+clientes_id);
				}else{
					window.open(url,"_blank");
				}
				window.location.reload(true);
			}catch(e){
				alert(e);
			}
		},
		"error":function(){
			alert("No se pudo actualizar el estatus");
			if(url==""){
				window.open("?c=clientes&m=edit&id="+clientes_id);
			}else{
				window.open(url,"_blank");
			}
			window.location.reload(true);
		}
	});
	
	
}
</script>	
<?php
	}
}
?>