<div class='box-section'>
    <h1>¿Olvidó su contraseña?</h1>
    <?php 
        if(isset($result['message'])){
            ?>
            <div class="alert alert-danger">
                <?php echo $result['message']; ?>
            </div>
            <?php
        }
    ?>
    <p>Ingrese su correo electrónico y recibirá instrucciones para recuperar su cuenta.</p>
    <form action="?m=forgot_password" method="POST">
        <?php
            echo php_Field::hidden("c","main");
            echo php_Field::hidden("m","forgot_password");
            echo php_Field::text("Correo",$args["correo"]);
            echo php_Field::submit("action","Recuperar contraseña");
        ?>
    </form>
</div>