<div class="row content-fluid">
	<div class="col-md-12"> 
		<h1><?php echo $result['area']['name']." - ". $args['location'];  ?></h1>
	</div>
</div>   
<div class="row content-fluid">
	<div class="col-md-12"> 

	<form action='?c=alerts&m=create' method="POST">
		<input type='hidden' name='areas_id' value='<?php echo $args['areas_id']; ?>' />
		<input type='hidden' name='location' value='<?php echo $args['location']; ?>' />
		<?php
			foreach($result['categories'] as $r){
				echo "<input class='btn btn-lg btn-danger' type='submit' value='".$r['name']."' name='action'/>";
			}
		?>
	</form>
	</div>	
</div>
<div class="row content-fluid">
	<div class="col-md-12"> 
		<h1>Alertas</h1>
	</div>
</div>
<div class="row content-fluid">

		<?php
			if(count($result['alerts'])){
				$columns=2;
				for($column=0;$column<$columns;$column++){
					echo "<div class='col-lg-".floor(12/$columns)."'>";
					$item=0;
					foreach($result['alerts'] as $r){
						
						if((($item++ +$column)%$columns)){
							
							continue;
						}
						
						echo "<script>S.add('"."a_".$r['id']."','".$r['start']."');</script>";
						?>
						<div class="panel panel-<?php echo $r['status']['css_class']; ?>">
							<div class="panel-heading text-left">
							<h4>
								#<?php echo $r['id']; ?> <?php echo $r['category_name']; ?> - <?php echo $r['status']['name']; ?>
							</h4>
							</div>
								
							<div class='panel-body'>
							
								<table class='table'>
									<tr>
										<th>Team</th>
										<th>Mensaje</th>
										<th>Respuesta</th>
										<th>Total</th>
										<th>Actions</th>
									</tr>
									<?php
									foreach($r['owners'] as $o){
										echo "<tr class='".$o['status']['css_class']."'>";
										echo "<td><h3>".$o['owner']['name']."</h3></td>";
										echo "<td>".$o['message']."</td>";
										
										echo "<td><span id='o_".$o['id']."'>".$o['start']."</span></td>";
										
										echo "<td><span id='w_".$o['id']."'>".$o['start']."</span></td>";
										
										echo "<td>";
										?>
										<form action='?c=alerts&m=update' method='POST'>
											<div class="btn-group">
									
												<input type='hidden' value='<?php echo $r['id']; ?>' name='alerts_id' />
												<input type='hidden' value='<?php echo $args['areas_id']; ?>' name='areas_id' />
												<input type='hidden' value='<?php echo $args['location']; ?>' name='location' />									
												<input type='hidden' value='<?php echo $o['id']; ?>' name='alerts_owners_id' />
												<input type='hidden' value='<?php echo $o['owners_id']; ?>' name='owners_id' />
												<input type='hidden' id='reason_<?php echo $o['id']; ?>' value='' name='reason' />
												
												<?php if($o['status']['name']!="Descartado"){ ?>
													<?php if($o['status']['name']=="En Espera"){?>
														<input class='btn btn-warning' type='submit' value='Revisando' name='action' />
													<?php } ?>
													
													<?php if($o['status']['name']=="En Revision"){?>
														<input class='btn btn-success' onclick='$("#reason_<?php echo $o['id']; ?>").val(prompt("Razon:"));' type='submit' value='Resuelto' name='action' />
													<?php } ?>
													
													<?php if(($o['status']['name']=="En Espera" && $r['can_discard_pending']
															)||($o['status']['name']=="En Revision" && $r['can_discard_active']) ){?>
														<input class='btn btn-danger' type='submit' value='Descartar' name='action' />
													<?php } ?>
												<?php }else{
													echo "Descartado";
												}												?>
											</div>
										</form>
												
									<?php
										echo "<script>S.add('"."o_".$o['id']."','".$o['start']."','".$o['first']."');</script>";
										echo "<script>S.add('"."w_".$o['id']."','".$o['start']."','".$o['finish']."');</script>";
										
										
											echo "</td>";
										
										
										
										echo "</tr>";
										}
							
									?>
								</table>
							</div>
							<div class='panel-footer'>
								
								
								<form action='?c=alerts&m=update' method='POST'>
									<input type='hidden' name='areas_id' value='<?php echo $args['areas_id']; ?>' />
									<input type='hidden' name='location' value='<?php echo $args['location']; ?>' />
									<input type='hidden' name='reason' value='' id='false_reason_<?php echo $o['id']; ?>'/>
									
			
									<input type='hidden' value='<?php echo $r['id']; ?>' name='alerts_id' />
											
									<div class="btn-group">
										<button class='btn btn-primary' onclick='$("#call_teams").fadeIn();$("#call_teams_alerts_id").val("<?php echo $r['id']; ?>");return false;'>Llamar</button>
										<input class='btn btn-default' onclick='$("#false_reason_<?php echo $o['id']; ?>").val( prompt("Razon:"));' type='submit' value='Falsa Alarma' name='action' />
									</div>
									<h4 class='visible-lg-inline-block visible-md-inline-block visible-sm-inline-block visible-xs-inline-block'>Tiempo Total:
											<span id='<?php echo "a_".$r['id']; ?>' class='data'><?php echo $r['created']; ?></span>
									</h4>
								</form>
								
							</div>
						</div>
							
									
						<?php
					}
					echo "</div>";
				}
			}
		?>

</div>

<div id='call_teams' class='modal'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header'> 
			<button class='close' onclick='$("#call_teams").fadeOut();'>X</button><h4 class='modal-title'>Call Team</h4></div>
			<div class='modal-body'>
				<form action="?c=alerts&m=update" method="POST">
					<input type='hidden' name='action' value='Llamar' />
					<input type='hidden' name='areas_id' value='<?php echo $args['areas_id']; ?>' />
					<input type='hidden' name='location' value='<?php echo $args['location']; ?>' />
					<input type='hidden' name='message' value='' id="call_message" />
					<input type='hidden' id='call_teams_alerts_id' name='alerts_id' value='' />
					<?php
						foreach($result['owners'] as $o){
							echo "<input class='btn-lg btn-info' onclick='$(\"#call_message\").val(prompt(\"Mensaje:\"));' type='submit' name='owner' value='".$o['name']."' />";
						}
					?>
				</form>
			</div>
		</div>
	</div>
</div>
<?php //echo "<pre>".var_export($result,true)."</pre>"; ?>


