<h1>Nueva ubicaci&oacute;n</h1>

<div class="panel panel-danger">
	<div class="panel-heading text-left">
		<h4>
			Advertencia
		</h4>
	</div>
	<div class='panel-body'>
		<p>La creación de nuevas ubicaciones de Andon se debe realizar de manera controlada.</p>
		<p>Si esta seguro de que quiere crear la ubicacion, presione enviar.</p>
		<p>De ahí se enviará un mensaje de aprobación a IT para verificar la ubicación.</p>
		<p>Para cualquier duda favor de contactar a Guillermo Morales (ext 8645)</p>
		<h3>IP: <?php echo $_SERVER['HTTP_X_FORWARDED_FOR']." ".$_SERVER['REMOTE_ADDR'];?> </h3>
		<a class='btn btn-danger' href="?<?php echo "areas_id=".$args['areas_id']."&location=".$args['location']."&confirm=true";?>">Confirmar y enviar solicitud</a>
	</div>
</div>