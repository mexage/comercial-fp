<div class='box-section'>
    <div class='row'>
        <div class='col-md-12'>
            <h1>Bienvenidos al sistema de acceso inteligente</h1>
            <p>Para instalar esta aplicación en su teléfono, basta con agregar esta liga a su página de inicio.</p>
        </div>
    </div>
    <div class='row'>    
        <div class='col-md-6'>
            <div class="well">
                <h2>Android</h2>
                <p>En el menú del navegador Chrome <img src='images/manual/chrome.png' style='height:32px;' /> utilice esta opción para agregar el atajo en su teléfono.</p>
                <img src='images/manual/android_2.jpg' style="width:60%;"/><br />
             
            </div>
        </div>
        
        <div class='col-md-6'>
            <div class="well">
                <h2>iPhone</h2>
                <p>Utilice la opción compartir en su navegador Safari <img src='images/manual/safari.png' style='height:32px;' /> y después esta opción para agregar el atajo en su teléfono.</p>
                <img src='images/manual/iphone_2.jpg' style="width:60%;"/><br />                
            </div>
        </div>
    </div>
</div>