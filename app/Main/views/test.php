<!--
	#icons .well{
		/*width:250px;
		height:250px;*/
		width:100%;
		height:0;
		padding-bottom:100%;
		
	}
	#icons .well img{
		/*max-height:150px;
		max-width:130px;
		height:auto;*/
		max-width:100%;
		height:auto;
		
	}
	
	/* select with custom icons */
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item-wrapper {
      padding: 0.5em 0 0.5em 3em;
    }
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item .ui-icon {
      height: 24px;
      width: 24px;
      top: 0.1em;
    }
    .ui-icon.printer {
		background: url("images/andon/printer.png") 0 0 no-repeat;
	}
-->
<?php
if(isset($args['areas_id'])&&isset($args['location'])){
?>
<script>var autoRefresh=setTimeout(function(){window.location.reload(true);},60000);</script>
				
<div class="row content-fluid">
	<div class="col-md-12"> 
		<h1><?php echo $result['area']['name']." - ". $args['location'];  ?></h1>
		<script>document.title="<?php echo $args['location']." |  Andon | ". $result['area']['name'];?>";</script>
	</div>
</div>   
<div id='alertas' class="row content-fluid">
	<?php
		if(count($result['alerts'])){
			?>
			<?php
			$columns=2;
			for($column=0;$column<$columns;$column++){
				echo "<div class='col-lg-".floor(12/$columns)."'>";
				$item=0;
				foreach($result['alerts'] as $alert){
					
					if((($item++ +$column)%$columns)){
						
						continue;
					}
					com_AndonCard::card($alert);
					
				}
				echo "</div>";
			}
		}
	?>
</div>


<div class="row content-fluid" id='icons'>
	<div id='device' class="col-md-12"> 
		<?php 
		com_AndonButtons($result['device_types'],$result['categories'] );
		?>
			
		<div class='row'>
		<?php
			com_AndonButtons::legacy($args['areas_id'],$args['location'],$result['categories']);
		?>
		</div>
	</div>	
</div>


<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#report">Open Modal</button>
<?php 
	com_AndonButtons::report_dialog($result['device_types'],$result['categories']);
?>
<!-- Modal -->
<div id="close" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cierre de Andon</h4>
      </div>
      <div class="modal-body">
		<div class="media" id='cierre-device-1'>
			<div class="media-left">
				<img src="images/andon/printer.png" class="media-object" style="width:200px">
			</div>
			<div class="media-body">
				<h4 class="media-heading">Impresora</h4>
				<div class='btn-group-vertical' id='printer'>
					<button class="btn btn-danger">Hojas atoradas</button>
					<button class="btn btn-danger">No hay toner / hojas</button>
					<button class="btn btn-danger">La hoja sale borrosa o manchada</button>
					<button class="btn btn-danger">La información sale incorrecta</button>
					<button class="btn btn-danger">Otro</button>
				</div>
			</div>
			
		</div>
		
		<div class="media" id='cierre-device-9'>
			<div class="media-left">
				<img src="images/andon/zebra.png" class="media-object" style="width:200px">
			</div>
			<div class="media-body">
				<h4 class="media-heading">Zebra</h4>
				<div class='btn-group-vertical' id='zebra'>
					<button class="btn btn-danger">No hay etiquetas / ribbon</button>
					<button class="btn btn-danger">La etiqueta sale desfasada</button>
					<button class="btn btn-danger">La etiqueta sale borrosa o manchada</button>
					<button class="btn btn-danger">La información sale incorrecta</button>
					<button class="btn btn-danger">Otro</button>
				</div>
			</div>
			
		</div>
		
        
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary">Enviar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>

<?php 

com_Device::dropdown("device",$result['device_types']);
?>

<?php 
com_AndonCard::call_dialog($args['areas_id'],$args['location'],$result['owners'])
?>


<?php

?>
<?php
}else{
	include 'app/main/views/manual.php';
}
?>