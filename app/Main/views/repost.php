<?php 
if(!isset($_SESSION['repost'])){
?>
<div class='box-section'>
<h3>Bienvenido!</h3>
<p>Por favor seleccione una opción del menú.</p> 
<script>
window.history.back();
</script>
</div>
<?php
}else{ 
?>
<div class='box-section'>
<div class='row'>
<div class='col-md-4'>
</div>
<div class='col-md-4'>
<h3>Acceso correcto</h3>
<p>Redirigiendo al contenido. Por favor espere.</p>
<form id='repost' action="?c=<?php echo $_SESSION['repost']['c'];?>&m=<?php echo $_SESSION['repost']['m'];?>" method="POST" enctype='multipart/form-data'>
<?php 

foreach($_SESSION['repost'] as $k=>$v){
		echo "<textarea name='".$k."' style='display:none;'>".$v."</textarea>";
}
unset($_SESSION['repost']);
?>
<div class="progress">
  <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar"
  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
    Cargando...
  </div>
</div>
<input style='display:none;' type='submit' class='btn btn-primary' value='Go' />
</form>
<script>
setTimeout(function(){$("#repost").submit();},1000);
$(document).ready(function(){
	$("footer").hide();
});
</script>
</div>
<div class='col-md-4'>
</div>
</div>
</div>

<?php }?>