<style>
    .well{
        background:#c1c1c1;
    }
</style>
<!--<div class='box-section'>-->
    <div class='panel panel-primary'>
    <div class='panel-heading'><h3 class='panel-title'>Pre-registro</h3></div>
        <div class='panel-body'>
            <?php
                echo php_Field::start_form("Fraccionamientos","preregistration");
                echo php_Field::hidden("c","Fraccionamientos");
                echo php_Field::hidden("m","preregistration");
                echo php_Field::hidden("fraccionamientos_id",$args["fraccionamientos_id"]);
                
                echo "<h2>Persona</h2>";
                echo php_Field::text("Email",$result['pre_register']['correo']);
                echo "<div class='well' style='display:none;' id='new_persona'>";
                    echo "<h3>Nueva Persona</h3>";
                    echo php_Field::text("Nombres",$result['pre_register']['nombres']);
                    echo php_Field::text("Apellidos",$result['pre_register']['apellidos']);
                    echo php_Field::number("Telefono",$result['pre_register']['telefono']);                    
                echo "</div>";
                echo "<div class='well' style='display:none;' id='existing_persona'>";
                    echo "<h3 id='existing_nombres'></h3>";                    
                    echo "<div id='existing_usuario'></div>";
                echo "</div>";    

                echo "<h2>Residencia</h2>";
                echo php_Field::dropdown("Lugares ID",$result["lugares_residencias"],"");
                echo "<div class='well' id='new_residencia'>";
                    echo "<h3>Nueva Residencia</h3>";
                    
                    echo php_Field::text("Calle",$result['pre_register']['calle']);
                    echo php_Field::text("Numero",$result['pre_register']['numero']);
                echo "</div>";
                // TODO: Add list of places with access $result["lugares_accesos"] and store comma separated
                echo "<h2>Lugares</h2>";
                $lugares=array();
                foreach($result["lugares_accesos"] as $acceso){
                    echo php_Field::checkbox("Accesos",$acceso["name"],$acceso["id"]);
                    $lugares[]=array("name"=>$acceso["name"],"id"=>$acceso["id"]);
                }
                //var_dump($lugares);
                //echo php_Field::dropdown("accesos",$lugares,"",-3);
                
                echo php_Field::submit("action","Guardar");
                echo php_Field::end_form();
            ?>
        </div>
    </div>
<!--</div>-->
<script>
$("#lugares_id").change(function(){
    if($("#lugares_id").val()==""){
        $("#new_residencia").slideDown();
    }else{
        $("#calle").val("<?php echo $result['pre_register']['calle']; ?>");
        $("#numero").val("<?php echo $result['pre_register']['numero']; ?>");
        $("#new_residencia").slideUp();
    }  
});
$("#email").change(function(){
    $("#new_persona").slideUp();
    $("#existing_persona").slideUp();
    $("#nombres").val("");
    $("#apellidos").val("");
    $("#telefono").val("");
    
    $("#existing_nombres").val("");
    $("#existing_usuario").val("");

    $.ajax({
        "url":"?c=users&m=view&id="+escape($("#email").val())+"&_data=json",
        "success":function(data){
            try{
                var o = JSON.parse(data);
                $("#existing_nombres").text(o.users.name);                
                $("#existing_usuario").text(o.users.id);                
                $("#existing_persona").slideDown();
            }catch(e){
                $("#nombres").val("<?php echo $result['pre_register']['nombres']; ?>");
                $("#apellidos").val("<?php echo $result['pre_register']['apellidos']; ?>");
                $("#telefono").val("<?php echo $result['pre_register']['telefono']; ?>");
                $("#new_persona").slideDown();
            }
        }
    });
});
<?php
if($args['self_registrations_id']){
?>
$("#email").change();
<?php
}
?>
</script>