<?php
class Fraccionamientos extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Fraccionamientos");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Fraccionamientos";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		
		return $result;
	}
	function add($args){
		$result = array();
		
		$result=parent::add($args);
		
		// Your code here.
		
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("fraccionamientos",$args['id']);
		//return "?c=Fraccionamientos";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
		return $result;
    }
    function preregistration($args){
        $result = array();
		if(is_numeric($args['self_registrations_id'])){
            $result['pre_register']=$this->db->get("self_registrations",$args['self_registrations_id']);
        }
		if(!is_numeric($args["fraccionamientos_id"])){
            return "?c=Fraccionamientos";
        }

        if(isset($args["action"])){
            // TODO: Check if user already exists and load persona
            // TODO: Check if persona already belongs to residencia and access

            $user=$this->db->get("users",addslashes(trim($args["email"])));
            if(count($user)){
                $persona=$this->db->get("personas",$user["personas_id"]);
                $password = "";
            }else{
                $persona=array(
                    "name"=>addslashes($args["nombres"]." ".$args["apellidos"]),
                    "nombre"=>addslashes($args["nombres"]),
                    "apellidos"=>addslashes($args["apellidos"]),
                    "telefono"=>addslashes($args["telefono"]),
                );
                $persona = $this->db->insert("personas",$persona);
                
                $password="Accesamo". str_pad(rand(0,999),3,"0",STR_PAD_LEFT);


                $user=array(
                    "id"=>addslashes(trim($args["email"])),
                    "name"=>$persona["name"],
                    "pass_hash"=>php_AccessControl::pass_hash($password), /* TODO: Add hash and default password */
                    "personas_id"=>$persona["id"],
                    "change_password"=>1
                );
                $user = $this->db->insert("users",$user);
            }
            if($args['lugares_id']){
                $residencia=$this->db->get("lugares",$args['lugares_id']);
            }else{
                $residencia= array(
                    "name"=>addslashes($args["calle"] .' - '.$args["numero"]),
                    "tipo_lugares_id"=>2, /* Residencia */
                    "fraccionamientos_id"=>$args["fraccionamientos_id"],
                );
                $residencia=$this->db->insert("lugares",$residencia);
            }
            $persona__residencia = array(
                "lugares_id"=>$residencia["id"],
                "personas_id"=>$persona["id"],
                "tipo_lugares_personas_id"=>1, /* Residente */
            );
            $persona__residencia=$this->db->insert("lugares__personas",$persona__residencia);

            $lugares=explode(",",$args["accesos"]);

            foreach($args["accesos"] as $acceso){
                //trace(var_export($acceso,true));
                $persona__acceso = array(
                    "lugares_id"=>$acceso,
                    "personas_id"=>$persona["id"],
                    "tipo_lugares_personas_id"=>4, /* Controlador */
                );
                $persona__acceso=$this->db->insert("lugares__personas",$persona__acceso);
            }
            /* TODO: Send welcome e-mail. */
            if($password){
                $envio=mail($args["email"],"Registro de control de accesos - Accesamo","

Hola ".$persona["name"].":
            
Ya puede controlar el acceso a su fraccionamiento desde su celular!
            
Vaya a https://mexage.com.mx/cherry/?user=". $args['email']." y use estos datos:

Usuario: ". $args['email']."
Password: ".$password."
            
Cualquier duda estamos a sus ordenes en soporte@accesamo.com
            
https://www.accesamo.com
            ",
                "From: Control de Acceso - Accesamo <soporte@accesamo.com> \r\n".
                "Bcc: soporte@accesamo.com \r\n"
                );
            }else{
                $envio=mail($args["email"],"Registro de control de accesos - Accesamo","

Hola ".$persona["name"].":
            
Ya puede controlar el acceso a una nueva ubicacion!
            
Vaya a https://mexage.com.mx/cherry/?user=". $args['email']." y use estos datos:

Usuario: ". $args['email']."
Password: Su contraseña existente.

Cualquier duda estamos a sus ordenes en soporte@accesamo.com
            
https://www.accesamo.com
            ",
                "From: Control de Acceso - Accesamo <soporte@accesamo.com> \r\n".
                "Bcc: soporte@accesamo.com \r\n"
                );
            }
            if($envio){
                $this->set_message("Pre-registration done!","success");            
            }else{
                $this->set_message("Couldn't send e-mail!","danger");            
            }
            return "?c=Fraccionamientos&m=edit&id=".$args["fraccionamientos_id"];
        }else{
            $result["fraccionamiento"]=$this->db->get("fraccionamientos",$args["fraccionamientos_id"]);
            $result["lugares_accesos"]=$this->db->search(
                "lugares",
                "tipo_lugares_id='". 1 /* Accesos */ ."' 
                 AND fraccionamientos_id='".$args["fraccionamientos_id"]."'"
            );
            $result["lugares_residencias"]=$this->db->search(
                "lugares",
                "tipo_lugares_id='". 2 /* Residencias */ ."' 
                 AND fraccionamientos_id='".$args["fraccionamientos_id"]."'"
            );
            array_unshift($result["lugares_residencias"],array("name"=>"Nueva","id"=>""));
        }
        
		return $result;
    }	
}
	