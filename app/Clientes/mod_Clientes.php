<?php
class mod_Clientes extends php_Model{
	function adeudo($clientes_id){
		$result=array();
		$result['pagos_contado']=$this->db->search("pagos","clientes_id='".addslashes($clientes_id)."' AND termino_pagos_id=1");
		$result['pagos_credito']=$this->db->search("pagos","clientes_id='".addslashes($clientes_id)."' AND termino_pagos_id<>1");
		$estatus_hash=$this->db->hash("estatus_ventas","name");
		$result['ventas_contado']=$this->db->query("
			SELECT 
				va.* 
			FROM 
				ventas v 
			INNER JOIN ventas_articulos va ON va.ventas_id=v.id
			WHERE 
				v.clientes_id='".addslashes($clientes_id)."'
				AND v.termino_pagos_id=1
				AND v.estatus_ventas_id='".$estatus_hash['Surtido']['id']."'
		");
		$result['ventas_credito']=$this->db->query("
			SELECT 
				va.*,
				v.fecha_promesa,
				v.requiere_factura
			FROM 
				ventas v 
			INNER JOIN ventas_articulos va ON va.ventas_id=v.id
			WHERE 
				v.clientes_id='".addslashes($clientes_id)."'
				AND v.termino_pagos_id<>1
				AND v.estatus_ventas_id='".$estatus_hash['Surtido']['id']."'
			ORDER BY v.fecha_promesa ASC
		");
		
		$saldo=0;
		$saldo_por_vencer=0;
		$vencimiento_minimo='';
		
		$result['clientes']=$this->db->get("clientes",$clientes_id);
		
		// Saldo total y vencimiento minimo
		if(count($result['pagos_credito'])){
			foreach($result['pagos_credito'] as $p){
				$saldo-=floatval($p['cantidad']);
			}
		}
		
		if(count($result['ventas_credito'])){
			foreach($result['ventas_credito'] as $v){
				$saldo+=(floatval($v['cantidad'])*floatval($v['precio_unitario']))*((100+(floatval($result['clientes']['iva'])*$v['requiere_factura']))/100);
				if($saldo>0 && !$vencimiento_minimo){
					$vencimiento_minimo=$v['fecha_promesa'];
				}
			}
			
			
		}
		// Saldo por vencer
		if(count($result['pagos_credito'])){
			foreach($result['pagos_credito'] as $p){
				$saldo_por_vencer-=floatval($p['cantidad']);
			}
		}
		
		if(count($result['ventas_credito'])){
			foreach($result['ventas_credito'] as $v){
				if(strcasecmp($v['fecha_promesa'],$vencimiento_minimo)<=0){
					$saldo_por_vencer+=floatval($v['cantidad'])*floatval($v['precio_unitario'])*((100+(floatval($result['clientes']['iva'])*$v['requiere_factura']))/100);
				}
			}
		}
		

		
		
		$result['saldo']=$saldo;
		$result['vencimiento_minimo']=$vencimiento_minimo;
		$result['saldo_por_vencer']=$saldo_por_vencer;
		
		if($saldo<=0){
			$result['css_class']='success';
		}else{
			$warning_day = date("Y-m-d",strtotime("+7 days"));
			if(strcasecmp($result['vencimiento_minimo'],date("Y-m-d"))<0){
				$result['css_class']='danger';
			}else if(strcasecmp($result['vencimiento_minimo'],$warning_day)<=0){
				$result['css_class']='warning';
			}else{
				$result['css_class']='success';
			}
		}
		$result['warning_day']=$warning_day;
		return $result;
	}
	
	
}