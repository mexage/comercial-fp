<?php
class Clientes extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Clientes");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Clientes";
	}
	function index($args){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		
		return $result;
	}
	function add($args){
		$result = array();
		
		$result=parent::add($args);
		
		// Your code here.
		
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		if(!is_string($result)){
			$mod_cliente=new mod_Clientes();
			$result['adeudo']=$mod_cliente->adeudo($args['id']);
			
		}
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("clientes",$args['id']);
		//return "?c=Clientes";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		if(!is_string($result)){
			$mod_cliente=new mod_Clientes();
			$result['adeudo']=$mod_cliente->adeudo($args['id']);
			
			$result['resumen'] = $this->resumen($args)['resumen'];
	
		}

		return $result;
	}
	function estados($args){
		$result = array();
		
		$clientes =  $this->db->search ( "clientes" , "" , "" , 5000  )   ;

		$estados = array();
		
		$por_vencer = 0 ;
		$ade_total  = 0 ;
		
		foreach( $clientes as $cliente ){
		
			$mod_cliente=new mod_Clientes();
			$adeudo = $mod_cliente->adeudo( $cliente['id'] );


			$por_vencer += $adeudo['saldo_por_vencer'];
			$ade_total  += $adeudo['saldo'];

			$estados[] = array (

				"clientes_id" => $cliente['id'],
				"Proximo Vencimiento"=>$adeudo['vencimiento_minimo'],
				"Saldo por vencer"=>$adeudo['saldo_por_vencer'],
				"Adeudo Total"=>$adeudo['saldo'],
				"Ver Cliente" => "<a href='?c=clientes&m=view&id=".$cliente['id'] ."' class='btn btn-danger' role='button'>
				<span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>
				View
				</a>"
				
			);
		}
		
		$estados[] =  array ( 	"clientes_id" => "--" ,
								"Proximo Vencimiento"=> "--" ,
								"Saldo por vencer"=>$por_vencer , 
								"Adeudo Total"=>$ade_total ) ;
		
		$result['estados'] = $estados ;

			
		return $result;
		
	}
	function resumen($args){
		$result = array();
		$resumen = array();
		$estatus_ventas_hash=$this->db->hash("estatus_ventas","name");
	//$result['status'] =$estatus_ventas_hash ; 	
	
		$criteria = "id = '" . $args['id'] . "'" ;
		//$criteria = "id = '1'" ;
		$cliente  = $this->db->search ( "clientes" , $criteria , "" , 1  )[0]   ;
		
		
		// ***************** VENTAS **************  //
		$criteria = " clientes_id  = '" . $args['id'] . "' and estatus_ventas_id = '" . 
		//$criteria = " clientes_id  = '1' and estatus_ventas_id = '" . 
					$estatus_ventas_hash['Surtido']['id'] . "'" .
					" order by id desc"; 
		$ventas   = $this->db->search ( "ventas" , $criteria , "" , 5000  )   ;
//print_r($criteria);
	//	$result['ventas'] = $ventas ; 
		
		
//pagos a credito como abonos
//ventas todas

		foreach( $ventas as $venta ){
			
			
			$importe = 0;
			
			$criteria = " ventas_id ='" . $venta['id'] . "'" ; 
			$ventas_articulos = $this->db->search ( "ventas_articulos" , $criteria , "" , 5000  )   ;
			
			foreach( $ventas_articulos as $ventas_art ){
				
				$importe += $ventas_art['cantidad'] * $ventas_art['precio_unitario'] ;  
				
			}
			
			
		
			$total = $importe * ((100+($cliente['iva']*$venta['requiere_factura']) )/100);
			
			
			//$clientes = $this->db->search ( "clientes" , "" , "" , 5000  );
			
			//$criteria = " fecha ='" . $venta['surtida'] . "'" ; 
			//$pagos = $this->db->search ( "pagos" , $criteria , "" , 5000  )   ;
			
			
			$movimiento = "Contado"; 
			
			if ( $venta['termino_pagos_id'] == 2 ) $movimiento = "Credito"; 
			
			$resumen[] = array (
			
				
				"Referencia" => "<a href='?c=Ventas&m=view&id=".$venta['id'] ."' class='btn btn-danger' role='button'>
				<span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>
				Venta(" . $venta['id'] . ")
				</a>",
				
				//"ventas_id" => $venta['id'] , 
				//"pagos_id" => "" , 
				"fecha"           => $venta['fecha'] ,
				"tipo_movimiento" => $movimiento ,
				"cantidad"        => $total
			
			);

		
		}
		
			/* 	$resumen[] = array (
				"Fecha"=> "www" ,
				"termino_pagos_id"=>"www" ,
				"cantidad"=>"www" , */
			
			//);
			
			
			
			

		
		
		// ***************** Pagos **************  //
		$criteria = " clientes_id =" . $args['id'] . " and termino_pagos_id = 2 order by id desc "; 
		$pagos   = $this->db->search ( "pagos" , $criteria , "" , 5000  )   ;
		
		
		foreach( $pagos as $pago ){
			
			
			$resumen[] = array (
			
				"Referencia" => "<a href='?c=Pagos&m=view&id=".$pago['id'] ."' class='btn btn-danger' role='button'>
				<span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>
				Pago(" . $pago['id'] . ")
				</a>",
			
				//"ventas_id" => "" , 
				//"pagos_id"  => $pago['id'] , 
				"fecha"           => $pago['fecha'] ,
				"tipo_movimiento" => "Abono" ,
				"cantidad"        => $pago['cantidad']
			
			);
			
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		usort( $resumen , function( $a , $b ){ return strcasecmp($b['fecha'],$a['fecha']); });
		$result['resumen'] = $resumen;
		
		
		return $result ;
		
	}
	function shortcut($args){
		switch(strtolower($args['action'])){
			case 'venta imprevista':{
				return "?c=Ventas&m=add&clientes_id=".$args['clientes_id'];
			}break;
			case 'ver cliente':{
				return "?c=Clientes&m=view&id=".$args['clientes_id'];
			}break;
		}
		return "?";
	}
	function store_position($args){
		$this->db->upd("clientes",$args['id'],array(
			"latitude"=>$args['latitude'],
			"longitude"=>$args['longitude'],
			"mapa_url"=>"https://www.google.com/maps/dir//".$args['latitude'].",".$args['longitude']."/@".$args['latitude'].",".$args['longitude'].",15z")
		);
		
		
		return array();
	}
	
	
}
	