<div class='row'>
<div class='col-md-4'>
<div class='alert alert-<?php echo $result['adeudo']['css_class']; ?>'>
	<h3>Adeudo total: $<?php echo $result['adeudo']['saldo']; ?></h3>
	<?php if( $result['adeudo']['vencimiento_minimo'] ){ ?>
	<h3>Siguiente vencimiento: <br /><?php echo $result['adeudo']['vencimiento_minimo']." ($". $result['adeudo']['saldo_por_vencer']; ?>)</h3>
	
	<?php } ?>
</div>
</div>
<div class='col-md-8'>
<?php
	
	// Table name to get from $result[$table_name].
	// If $result[$table_name] is not set, then it will take $args as data row
	// $table_name = strtolower($args['c']);
	
	// Human name of the table.
	// $human_name = ucwords(trim(str_replace("_"," ",$table_name)));
	
	// Class name
	// $class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));
	
	// Middle buttons
	$links = array(
		"Ver todos los clientes"=>"?c=Clientes&m=estados",		
		"Venta imprevista"=>"?c=Ventas&m=add&clientes_id=".$args['id'],		
		"Ver"=>"?c=Clientes&m=view&id=".$args['id'],
	);
	
	// Actions
	//$actions=array(
	//	"Save"=>"primary"
	//);
	
	// Field types
	//$field_type=array(
	//	"id"=>"readonly"
	//);
	
	// Dropdown tables
	// $f_tables=array(
	//		"active"=>"states"
	//);
	// $result['states']=array(
	//	array("name"=>"Yes","id"="1"),
	//	array("name"=>"No","id"="2"),
	// );		

	
	// Hidden
	//$hidden = array(
	//		"created"
	//);
	
	// Slave tables (in the form of ["linkfield table"]
	//if(!isset($slave_tables)){
	//	$slave_tables=array();
	//}	
	
	
	
	// Default view "app/_default/form.php"
	// This view will take all previous parameters and render the assigned table.

	include $vista_default;	
?>
</div>
</div>