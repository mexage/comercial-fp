<div class='row'>
<div class='col-md-4'>
<div class='alert alert-<?php echo $result['adeudo']['css_class']; ?>'>
	<h3>Adeudo total: $<?php echo $result['adeudo']['saldo']; ?></h3>
	<?php if( $result['adeudo']['vencimiento_minimo'] ){ ?>
	<h3>Siguiente vencimiento: <br /><?php echo $result['adeudo']['vencimiento_minimo']." ($". $result['adeudo']['saldo_por_vencer']; ?>)</h3>
	
	<?php } ?>
</div>


<?php
	
	// Table name to get from $result[$table_name].
	// If $result[$table_name] is not set, then it will take $args as data row
	// $table_name = strtolower($args['c']);
	
	// Human name of the table.
	// $human_name = ucwords(trim(str_replace("_"," ",$table_name)));
	
	// Class name
	// $class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));
	
	// Middle buttons
	$links = array(
		"Ver todos los clientes"=>"?c=Clientes&m=estados",	
		"Venta imprevista"=>"?c=Ventas&m=add&clientes_id=".$args['id'],	
	);
	
	// Actions
	//$actions=array(
	//	"Save"=>"primary"
	//);
	
	// Field types
	//$field_type=array(
	//	"id"=>"readonly"
	//);
	
	// Dropdown tables
	// $f_tables=array(
	//		"active"=>"states"
	//);
	// $result['states']=array(
	//	array("name"=>"Yes","id"="1"),
	//	array("name"=>"No","id"="2"),
	// );		

	
	// Hidden
	//$hidden = array(
	//		"created"
	//);
	$hidden[]='mapa_url';
	$hidden[]='latitude';
	$hidden[]='longitude';
	
	// Slave tables (in the form of ["linkfield table"]
	//if(!isset($slave_tables)){
	//	$slave_tables=array();
	//}	
	
	
	
	// Default view "app/_default/form.php"
	// This view will take all previous parameters and render the assigned table.
	include $vista_default;	
?>
</div>
<div class='col-md-8'>
<?php $id="movements"; // TODO: Change title ?>
<div class='panel panel-success'>
	<div class='panel-heading'>
		<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' >Movimientos Cliente <span class='badge'>Nuevo!</span></a></h4>
	</div>
	<div id="<?php echo $id; ?>" class='panel-collapse collapse in'>
		<div class='panel-body' style='overflow:auto;'>
		<?php
			echo php_Grid::draw($result['resumen']);
		?>

		</div>
	</div>
	<!--<div class='panel-footer text-right'>
	</div>-->

</div>
</div>
</div>