<?php

	// VARIABLES ////////////////////////////////////////////////////////////
	
	// If data is in different table from $result[ $args['c'] ]
	// $table_name = "other_table";
	
	// **Controller class name (for interactions)
	// $class_name = "Some_Controler";

	// ** Customize title
	// $human_name = "My panel";


	// PANEL //////////////////////////////////////////////////////////////////
	
	// ** Panel color
	// $panel_class='danger';

	// ** id for the panel
	// $id="panel-".$table_name;

	// ** Panel should be collapsed by default
	// $collapse=true;

	
	// BUTTONS /////////////////////////////////////////////////////////////////

	// ** Links (shown at right column)
	$links = array(
			"Ver:danger:eye-open"=>"?c=Clientes&m=view&id=<?=id?>",		
			"Mapa:primary:road"=>"<?=mapa_url?>",
	);

	// ** Actions (shown at bottom)
	// $actions = array(
	//		"Add"=>"?c=".$class_name."&m=add",		
	// );


	// INTERFACE ///////////////////////////////////////////////////////////
	
	// ** Hide columns
	$hidden=array('mapa_url','latitude','longitude');

	// ** If you want to show as Excel datasheet - https://bossanova.uk/jexcel/v3/docs/quick-reference
	// $dynamic='jExcel';
	//
	// ** Interaction configuration
	// $configuration = array("c"=>$class_name,"m"=>"edit");
	//
	// ** Columns (jExcel format see https://bossanova.uk/jexcel/v3/examples/column-types)
	// $configuration['columns']= array(
	// 		array(
	//			"type"=>"readOnly"
	//			"name"=>"id",
	//			"title"=>"Your ID",
	//		),
	// 		array(
	//			"type"=>"text"
	//			"name"=>"",
	//			"title"=>""
	//		),
	// );
	// ** Events (jExcel format)
	// $configuration['events']=array(
	// 		"onload"=>"function(){alert('Hi');}"
	// );

	// ** If you want to show as tableEdit
	// $dynamic=true;
	//
	// ** Columns which are editable
	// $editable = array("some_column","other_column");

	// Default view "app/_default/views/index.php"
	// This view will take all previous parameters and render the assigned table.
	include $vista_default;	
	