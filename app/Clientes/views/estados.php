<style>
#desglosada th {
  position: sticky !important;
  top: 0;
  z-index: 10;
  background-color: white;
  text-align:center;
  border-top:2pt solid gray !important;
  border-bottom:2pt solid gray !important;
}
#desglosada table > tbody > tr:last-child { 
		background:#ff0000; 
		background-color: white;
	  text-align:center;
	  border-top:2pt solid gray !important;
	  border-bottom:2pt solid gray !important;
	  font-weight:bold;
}
</style>



<div class='panel panel-info'>
<div class='panel-heading'>
<h4 class='panel-title'><a data-toggle='collapse' href='#desglosada'>Estado de Cliente</a></h4>
</div>
<div id='desglosada' class='panel-collapse collapse in'>
<div class='panel-body'>
<?php
echo php_Chart::copy_button("#estados","Copiar");	
?>
<div id='estados'>
<?php
echo php_Grid::draw( $result['estados'] );	
?>
</div>
</div>
</div>
</div>
