<?php
class Self_Registrations extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args) ;
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Self_Registrations");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Self Registrations";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		
		return $result;
	}
	function add($args){
		$result = array();
		
		$result=parent::add($args);
		
		// Your code here.
		
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("self_registrations",$args['id']);
		//return "?c=Self_Registrations";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
            $result['fraccionamientos']=$this->db->search("fraccionamientos");
        

		return $result;
	}
	
}
	