<?php
	
	// Table name to get from $result[$table_name].
	// If $result[$table_name] is not set, then it will take $args as data row
	// $table_name = strtolower($args['c']);
	
	// Human name of the table.
	// $human_name = ucwords(trim(str_replace("_"," ",$table_name)));
	
	// Class name
	// $class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));
	
	// Middle buttons
	//$links = array(
	//	"List"=>"?c=".$class_name."",		
	//	"View"=>"?c=".$class_name."&m=view&id=< ?=id >",		
	//);
	
	// Actions
	//$actions=array(
	//	"Save"=>"primary"
	//);
	
	// Field types
	//$field_type=array(
	//	"id"=>"readonly"
	//);
	
	// Dropdown tables
	// $f_tables=array(
	//		"active"=>"states"
	//);
	// $result['states']=array(
	//	array("name"=>"Yes","id"="1"),
	//	array("name"=>"No","id"="2"),
	// );		

	
	// Hidden
	//$hidden = array(
	//		"created"
	//);
	
	// Slave tables (in the form of ["linkfield table"]
	//if(!isset($slave_tables)){
	//	$slave_tables=array();
	//}	
	
	$result['slave_tables']['rutas_clientes rutas_id']['dynamic']="jExcel";
	$result['slave_tables']['rutas_clientes rutas_id']['jExcel_configuration']=array(
		"columns"=>array(
			array(
				"name"=>"id",
				"title"=>"ID",
				"type"=>"hidden",
				"readOnly"=>"true",
			),
			array(
				"name"=>"rutas_id",
				"title"=>"Rutas ID",
				"type"=>"hidden",
				"readOnly"=>"true",
			),
			array(
				"name"=>"clientes_id",
				"title"=>"Clientes ID",
				"type"=>"dropdown",
				"url"=>"?c=Clientes&m=_list&_data=json",
				"autocomplete"=>true,
			),
			array(
				"name"=>"orden",
				"title"=>"Orden",
				"type"=>"number",
				
			),
		),
		"defaults"=>array(
			"rutas_id"=>$args['id']
		)
	);
	if(!count($result['rutas_clientes rutas_id'])){
		$result['rutas_clientes rutas_id'][]=$db->empty_row("rutas_clientes",array("rutas_id"=>$args['id']));
	}
	
	usort($result['rutas_clientes rutas_id'],function($a,$b){return $a['orden']-$b['orden'];});
	// Default view "app/_default/form.php"
	// This view will take all previous parameters and render the assigned table.
	include $vista_default;	
