<?php $id="customer"; // TODO: Change title ?>
<div class='panel panel-success'>
	<div class='panel-heading'>
		<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' >Acciones Cliente <span class='badge'>Nuevo!</span></a></h4>
	</div>
	<div id="<?php echo $id; ?>" class='panel-collapse collapse'>
		<div class='panel-body' style='overflow:auto;'>
		<form action='?c=Clientes&m=shortcut' method='POST'>
		<?php
			echo php_Field::hidden("c","Clientes");
			echo php_Field::hidden("m","shortcut");
			echo php_Field::combobox("Clientes ID",$result['clientes'],"");
			echo php_Field::submit("action","Venta Imprevista");
			echo php_Field::submit("action","Ver Cliente");
		?>
		</form>
		<script>
		$(document).ready(function(){
			$("#clientes_id").val("").change();
			$("#clientes_id").closest("form").find("input.custom-combobox-input").val("").change();
		});
		</script>
		</div>
	</div>
	<!--<div class='panel-footer text-right'>
	</div>-->

</div>
<div class='well'>

	<?php
echo php_Field::combobox("Repartidor",$result['repartidores'],(isset($args['repartidor']))?$args['repartidor']:"");		
?>


	<button class='btn btn-warning' onclick="view_pending_sales();">Ver ventas pendientes</button>

</div>
<?php $id="rutas"; // TODO: Change title ?>

<?php

	// VARIABLES ////////////////////////////////////////////////////////////
	
	// If data is in different table from $result[ $args['c'] ]
	// $table_name = "other_table";
	
	// **Controller class name (for interactions)
	// $class_name = "Some_Controler";

	// ** Customize title
	// $human_name = "My panel";


	// PANEL //////////////////////////////////////////////////////////////////
	
	// ** Panel color
	// $panel_class='danger';

	// ** id for the panel
	// $id="panel-".$table_name;

	// ** Panel should be collapsed by default
	// $collapse=true;

	
	// BUTTONS /////////////////////////////////////////////////////////////////

	// ** Links (shown at right column)
	$links = array(
			"Edit:danger:pencil"=>"?c=Rutas&m=edit&id=<?=id?>",
			"Go!:success:check"=>"onclick:schedule(<?=id?>,$(\"#repartidor\").val());",
			
	);

	// ** Actions (shown at bottom)
	$actions = array(
		"Venta Imprevista"=>"?c=Ventas&m=add",
		"Agregar Ruta"=>"?c=Rutas&m=add",		
	);


	// INTERFACE ///////////////////////////////////////////////////////////
	
	// ** Hide columns
	// $hidden = array("some_column","other_column");

	// ** If you want to show as Excel datasheet - https://bossanova.uk/jexcel/v3/docs/quick-reference
	// $dynamic='jExcel';
	//
	// ** Interaction configuration
	// $configuration = array("c"=>$class_name,"m"=>"edit");
	//
	// ** Columns (jExcel format see https://bossanova.uk/jexcel/v3/examples/column-types)
	// $configuration['columns']= array(
	// 		array(
	//			"type"=>"readOnly"
	//			"name"=>"id",
	//			"title"=>"Your ID",
	//		),
	// 		array(
	//			"type"=>"text"
	//			"name"=>"",
	//			"title"=>""
	//		),
	// );
	// ** Events (jExcel format)
	// $configuration['events']=array(
	// 		"onload"=>"function(){alert('Hi');}"
	// );

	// ** If you want to show as tableEdit
	// $dynamic=true;
	//
	// ** Columns which are editable
	// $editable = array("some_column","other_column");

	// Default view "app/_default/views/index.php"
	// This view will take all previous parameters and render the assigned table.
	include $vista_default;	
?>
<script>
function schedule(id,repartidor){
	window.location.href='?c=Rutas&m=schedule&id='+id+"&repartidor__users_id="+repartidor;
}
function view_pending_sales(){
	window.location.href='?repartidor='+$("#repartidor").val();
}
</script>