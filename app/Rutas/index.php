<?php
class Rutas extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Rutas");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Rutas";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		$result['clientes']=$this->db->search("clientes");

		$result['repartidores']=$this->db->search("users");
		foreach($result['repartidores'] as &$r){
			$d=$this->db->query("
			SELECT 
				count(*) c
			FROM ventas v
			INNER JOIN clientes c on v.clientes_id=c.id
			WHERE v.repartidor__users_id='".$r['id']."' 
			AND v.estatus_ventas_id in(0,1,2)");
			$r['name'].= ": ".$d[0]['c']." pendiente(s)";
		}

		return $result;
	}
	function add($args){
		$result = array();
		
		$result=parent::add($args);
		
		// Your code here.
		
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("rutas",$args['id']);
		//return "?c=Rutas";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
		return $result;
	}
	function schedule($args){
		$clientes=$this->db->search("rutas_clientes","rutas_id='".$args['id']."' AND clientes_id NOT IN (SELECT DISTINCT clientes_id FROM ventas WHERE fecha='".date("Y-m-d")."' AND estatus_ventas_id=0)","orden");
		if(count($clientes)){
			foreach($clientes as $c){
				$this->db->insert("ventas",array(
					"clientes_id"=>$c['clientes_id'],
					"repartidor__users_id"=>$args['repartidor__users_id'],
					"fecha"=>date("Y-m-d"),
					"created_by__users_id"=>$this->user->attr("id")
				));
			}
			$this->set_message("Se agregaron ". count($clientes) . " visita(s) al repartidor ".$args['repartidor__users_id'],"success");
		}else{
			$this->set_message("No hay clientes pendientes en esa ruta!","danger");
		}
		return "?c=Rutas";
	}
	function cancel($args){
		$venta = $this->db->get("ventas",$args['id']);
		$estatus_hash=$this->db->hash("estatus_ventas","name");
		if(strcasecmp($venta['estatus_ventas_id'],$estatus_hash['Pendiente']['id'])==0
			||strcasecmp($venta['estatus_ventas_id'],$estatus_hash['En camino']['id'])==0
		){
			$this->db->upd("ventas",$args['id'],array("estatus_ventas_id"=>$estatus_hash['Cancelado']['id']));
			$this->set_message("La venta fue cancelada","info");
		}else{
			if(strcasecmp($venta['estatus_ventas_id'],$estatus_hash['Surtiendo']['id'])==0){
				if(count($this->db->search("ventas_articulos","ventas_id='".$args['id']."'"))){
					$this->set_message("La venta solo puede ser cancelada en estatus surtiendo si no tiene articulos.","danger");
				}else{
					$this->db->upd("ventas",$args['id'],array("estatus_ventas_id"=>$estatus_hash['Cancelado']['id']));
					$this->set_message("La venta fue cancelada","info");
				}
				
			}else{
				$this->set_message("La venta solo puede ser cancelada en estatus pendiente, en camino o surtiendo.","danger");
			}
		}
		return "?";
	}
}
	