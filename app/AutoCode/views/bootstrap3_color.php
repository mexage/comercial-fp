<?php 
if(!isset($args['class'])){
	$args['class']='color_class';
}
if(!isset($args['letters'])){
	$args['letters']='#004000';
}
if(!isset($args['dark'])){
	$args['dark']='#008c00';
}
if(!isset($args['light'])){
	$args['light']='#96ff73';
}
if(!isset($args['base'])){
	$args['base']='#36d900';
}
?>

<?php $id="color_panel";  ?>
<div class='panel panel-primary'>
	<div class='panel-heading'>
		<h4 class='panel-title'><a data-toggle='collapse' href='#<?php echo $id; ?>' >New Bootstrap Color</a></h4>
	</div>
	<?php echo php_Field::start_form($args['c'],$args['m'],$args,"GET"); ?>
	<div id="<?php echo $id; ?>" class='panel-collapse collapse in'>
		<div class='panel-body' style='overflow:auto;'>
		<input type="hidden" name="c" value="AutoCode" />
		<input type="hidden" name="m" value="bootstrap3_color" />
		<label for='class'>Class:</label><input type="text" value="<?php echo $args['class'];?>" name="class" id="class" />
		<label for='letters'>Text:</label><input type="color" value="<?php echo $args['letters'];?>" name="letters" id="letters" />
		<label for='dark'>Dark:</label><input type="color" value="<?php echo $args['dark'];?>" name="dark" id="dark" />
		<label for='light'>Light:</label><input type="color" value="<?php echo $args['light'];?>" name="light" id="light" />
		<label for='base'>Base:</label><input type="color" value="<?php echo $args['base'];?>" name="base" id="base" />
		<input type="submit" />
		</div>
	</div>
	<div class='panel-footer'>

	</div>
	<?php echo php_Field::end_form(); ?>
</div>

<?php

// https://stackoverflow.com/questions/38541526/adding-a-color-class-to-bootstrap
$css=".class,
.class a {
  color: letters !important;
  background-color: base !important;
  border-color: light !important;
}
.class:active,
.class.active,
.class:focus,
.class.focus,
.class:hover,
.class a:hover {
  background-color: light !important;
  border-color: dark !important;
}";

$css=str_ireplace("dark",$args['dark'],$css);
$css=str_ireplace("light",$args['light'],$css);
$css=str_ireplace("base",$args['base'],$css);
$css=str_ireplace("letters",$args['letters'],$css);
$css=str_ireplace("class",$args['class'],$css);

?>
<style>
<?php
echo $css;
?>
</style>

<h1>Preview<h1>
<div class="container">
  <nav class="navbar navbar-default">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <div class="collapse navbar-collapse" id="navbar-1">
      <ul class="nav navbar-nav">
        <li class="<?php echo $args['class'];?>"><a href="#">class item</a></li>
        <li><a class="<?php echo $args['class'];?>" href="#">class link</a></li>
      </ul>
    </div>
  </nav>

  <div class="<?php echo $args['class'];?>">class block</div>

  <p>class <span class="<?php echo $args['class'];?>">text</span></p>

  <div class="alert <?php echo $args['class'];?>">class alert</div>

  <div class="well <?php echo $args['class'];?>">class well</div>

  <button class="btn <?php echo $args['class'];?>">class button</button>
</div>
<h1>Code</h1>
<div class='well'>
<textarea rows="15" style='width:100%'>
<?php
echo $css;
?>
</textarea>
</div>