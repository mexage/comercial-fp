<h1>Welcome to autocoder!</h1>
<p>Don't let developers trick you into thinking development is hard...</p>
<a href="?c=AutoCode&m=bootstrap3_color" class="btn btn-primary">Bootstrap3 Color Generator</a>

<form action="" method="POST">
	<?php echo php_Field::dropdown("Table",$result['tables'],$result['table']); ?>
	<input class='btn btn-primary' type='submit' name='action' submit='Do your magic!' />  
</form>
<?php 
if(isset($result['table'])){
?>
	<?php

	$human_name = ucwords(trim(str_replace("_"," ",$result['table'])));
	$table_name = $result['table'];
	$class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));
	?>
	<h1>Your Code For <?php echo $human_name; ?></h1>
	<h2>Controller</h2>sdfsdf
	<input class='btn btn-primary' type='submit' name='controller' submit='create controller' /> 
	<p>This file goes into app/<?php echo $class_name; ?>/index.php</p>
	
	<textarea rows='20' class='form-control'>
&lt;?php
class <?php echo $class_name;?> extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("<?php echo $class_name; ?>");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - <?php echo $human_name; ?>";
	}
	function index($args){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		
		return $result;
	}
	function add($args){
		$result = array();
		
		$result=parent::add($args);
		
		// Your code here.
		
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("<?php echo $table_name;?>",$args['id']);
		//return "?c=<?php echo $class_name; ?>";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
		return $result;
	}
	
}
<?php 
	



	?>
	</textarea>

	<h2>Views</h2>
	<h3>Index</h3>
	<textarea class='form-control' rows='20' >
&lt;?php

	// VARIABLES ////////////////////////////////////////////////////////////
	
	// If data is in different table from $result[ $args['c'] ]
	// $table_name = "other_table";
	
	// **Controller class name (for interactions)
	// $class_name = "Some_Controler";

	// ** Customize title
	// $human_name = "My panel";


	// PANEL //////////////////////////////////////////////////////////////////
	
	// ** Panel color
	// $panel_class='danger';

	// ** id for the panel
	// $id="panel-".$table_name;

	// ** Panel should be collapsed by default
	// $collapse=true;

	
	// BUTTONS /////////////////////////////////////////////////////////////////

	// ** Links (shown at right column)
	// $links = array(
	//		"View"=>"?c=".$class_name."&m=view&id=<?php echo "<?=id?>" ?>",		
	//	);

	// ** Actions (shown at bottom)
	// $actions = array(
	//		"Add"=>"?c=".$class_name."&m=add",		
	// );


	// INTERFACE ///////////////////////////////////////////////////////////
	
	// ** Hide columns
	// $hidden = array("some_column","other_column");

	// ** If you want to show as Excel datasheet - https://bossanova.uk/jexcel/v3/docs/quick-reference
	// $dynamic='jExcel';
	//
	// ** Interaction configuration
	// $configuration = array("c"=>$class_name,"m"=>"edit");
	//
	// ** Columns (jExcel format see https://bossanova.uk/jexcel/v3/examples/column-types)
	// $configuration['columns']= array(
	// 		array(
	//			"type"=>"readOnly"
	//			"name"=>"id",
	//			"title"=>"Your ID",
	//		),
	// 		array(
	//			"type"=>"text"
	//			"name"=>"",
	//			"title"=>""
	//		),
	// );
	// ** Events (jExcel format)
	// $configuration['events']=array(
	// 		"onload"=>"function(){alert('Hi');}"
	// );

	// ** If you want to show as tableEdit
	// $dynamic=true;
	//
	// ** Columns which are editable
	// $editable = array("some_column","other_column");

	// Default view "app/_default/views/index.php"
	// This view will take all previous parameters and render the assigned table.
	include $vista_default;	
	</textarea>
	
	<h3>Form</h3>
	<p>This works for form views (e.g. add, edit, view)</p>
	<textarea class='form-control' rows='20' >
&lt;?php
	
	// Table name to get from $result[$table_name].
	// If $result[$table_name] is not set, then it will take $args as data row
	// $table_name = strtolower($args['c']);
	
	// Human name of the table.
	// $human_name = ucwords(trim(str_replace("_"," ",$table_name)));
	
	// Class name
	// $class_name = "".str_replace(" ","_",ucwords(str_replace("_"," ",$table_name)));
	
	// Middle buttons
	//$links = array(
	//	"List"=>"?c=".$class_name."",		
	//	"View"=>"?c=".$class_name."&m=view&id=< ?=id?>",		
	//);
	
	// Actions
	//$actions=array(
	//	"Save"=>"primary"
	//);
	
	// Field types
	//$field_type=array(
	//	"id"=>"readonly"
	//);
	
	// Dropdown tables
	// $f_tables=array(
	//		"active"=>"states"
	//);
	// $result['states']=array(
	//	array("name"=>"Yes","id"="1"),
	//	array("name"=>"No","id"="2"),
	// );		

	
	// Hidden
	//$hidden = array(
	//		"created"
	//);
	
	// Slave tables (in the form of ["linkfield table"]
	//if(!isset($slave_tables)){
	//	$slave_tables=array();
	//}	
	
	
	
	// Default view "app/_default/form.php"
	// This view will take all previous parameters and render the assigned table.
	include $vista_default;	
</textarea>
<?php
}
?>