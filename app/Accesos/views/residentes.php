<style>
.panel{
	margin-top:10px;
}
#buttons a{
	white-space: break-spaces;
	display:inline-block;
	/*height:25vh;*/
	overflow:hidden;

	margin-top:10px;
}
#buttons a img{
	width:100%;
	margin-left:auto;
	margin-right:auto;
	display:block;
}
</style>
<div class='box-section'>
<h1><?php echo $result["personas"]["name"]; ?></h1>
<div id="buttons">
<?php
//foreach($result['lugares'] as $lugar){
	//echo "<a class='btn btn-lg btn-primary' href='?c=Accesos&m=residentes&id=".$args["id"]."&open=".$lugar["id"]."'>".$lugar["name"]."</a> ";
	//}
	?>
</div>

<script>
var Bootstrap = new function(){
	this.section=function(el){
		return $($("<div>").addClass("well").addClass("row")).appendTo(el);
	};
	this.title=function(el,text,l){
		return $(el).append($("<h"+l+">").text(text));
	};
	this.button=function(el,text,id,cl,click,column_width){
		var showing=false;
		var timer;
		var link = 	$("<a>").html(
				"<span class='glyphicon glyphicon-ok'></span> Abrir"
			).addClass("btn btn-success btn-lg").addClass("text-center").css("width","100%")/*.prepend(
				$("<img>").attr("src",'?c=Punto_Accesos&id="+id+"&name=image.jpg&_data=attachment')
			)*/;
		var nel=$(el).append(
			$("<div>").append(
				$("<div>").addClass("panel").addClass("panel-"+cl).append(
					$("<div>").addClass("panel-heading").click(function(ev){
								ev.preventDefault();
								showing=!showing;
								$("#botones"+id).slideToggle(showing);
								if(showing){
									timer=setTimeout(function(){$("#botones"+id).slideUp();showing=false;},2000);
								}else{
									clearTimeout(timer);
								}
							}).append(
						$("<h4>").addClass("panel-title").append(
							$("<a>").attr("href","#").text(text)
						)
					)
				).append(
					$("<div id='botones"+id+"'>").addClass("panel-body").hide().append(link)
				)
			).addClass("col-md-"+column_width)
		);
		
		if(typeof click === "string"){	
			//link.attr("href",click);
			link.click(function(){
				window.location.href=click;
				$(link).removeClass("btn-primary").addClass("btn-info");
			});
		}else{
			link.click(click);
		}
		return nel;
	};
	this.cameras=function(el,lugar){
		var showing=false;
				
		var timer=0;
		return $(el).append(
			$("<div>").addClass("row").append(
				$("<div>").addClass("panel panel-info").append(
					$("<div>").addClass("panel-heading").append(
						$("<h4>").addClass("panel-title").html("<a><span class='glyphicon glyphicon-facetime-video'></span> Ver Camaras</a>").click(function(ev){
							showing=!showing;
							$("#camaras"+lugar.id).slideToggle(showing);
							if(showing){
								var update_function=function(){
									var html="";
									for(var c in lugar.camaras){
										html+="<img src='?c=Camaras&id="+lugar.camaras[c].id+"&name=image.jpg&_data=attachment&time="+(new Date()).toISOString()+"' style='width:100%' />";
									}
									var $pre_img=$("#preload_camaras"+lugar.id).html(html)
									var loaded_images_count=0;
									$pre_img.find("img").on('load',function(){
										loaded_images_count++;

										if (loaded_images_count == $pre_img.find("img").length) {
											$("#camaras"+lugar.id).html($("#preload_camaras"+lugar.id).html());
										}
									});
									setTimeout(function(){
										//$("#camaras"+lugar.id).html($("#preload_camaras"+lugar.id).html());
									},2000);								
								}
								timer = setInterval(update_function,2000);
								update_function();
							}else{
								clearInterval(timer);
							}
						})
					)
				).append(
					$("<div id='camaras"+lugar.id+"'>").hide().append(
						$("<div>").addClass("panel-body").html('<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">Cargando...</div></div>')
					)
				).append(
					$("<div id='preload_camaras"+lugar.id+"'>").hide()
				)
			)
		);
		
	}
}();
$.ajax({
	url:"?c=Accesos&m=residentes&_data=json",
	method:"POST",
	data:{"c":"Accesos","m":"residentes","id":"<?php echo $user->attr('personas_id');?>","_data":"json"},
	success:function(data){
		try{
			var o=JSON.parse(data);
			for(var f in o.fraccionamientos){
				var fraccionamiento=o.fraccionamientos[f];
				Bootstrap.title("#buttons",fraccionamiento.name,2);
				for(var l in fraccionamiento.lugares){
					var section = Bootstrap.section("#buttons");
					var lugar = fraccionamiento.lugares[l];
					Bootstrap.title(section,lugar.name,3);
					for(var p in lugar.punto_accesos){
						var n_accesos =  lugar.punto_accesos.length;
						var column_width=12;
						if(n_accesos==2){
							column_width=6;
						}else if(n_accesos>=3){
							column_width=4;
						}
						var punto_acceso = lugar.punto_accesos[p];
						Bootstrap.button(section,punto_acceso.name,punto_acceso.id,"primary",
						"?c=Accesos&m=residentes&open="+punto_acceso.id
						// TODO: Fix function to highlight
						//function(ev,ui){
							//if(ev.delegateTarget.classList.value.indexOf("btn-info")>=0){					
								

								//$(ev.delegateTarget).removeClass("btn-primary").addClass("btn-info");
								//location.href="?c=Accesos&m=residentes&open="+punto_acceso.id;
							//}else{
								//$(ev.delegateTarget).removeClass("btn-primary").addClass("btn-info");
							//	setTimeout(function(){$(ev.delegateTarget).addClass("btn-primary").removeClass("btn-info");},3000);
							//}
						//}
						,column_width);
					}
					if(lugar.camaras.length){
						Bootstrap.cameras(section,lugar);
					}
				}
			}
		}catch(e){
			alert(e);			
		}
	},
	error:function(){

	}
});

setTimeout(function(){
	$(".alert").slideUp();
},3000);

</script>

<a href='?m=manual' id="bookmarkme" class="btn btn-basic"><span class='glyphicon glyphicon-question-sign'></span> Cómo instalar en el celular como app</a>
<a href='?m=account_details' class="btn btn-basic"><span class='glyphicon glyphicon-lock'></span> Cambiar mis datos</a>
</div>
<?php 
echo com_Dialog::shell("manual","Manual");
echo com_Dialog::ajax_load_links("#bookmarkme","manual");
?>