<div class='box-section'>
<h1><?php echo $result["fraccionamientos"]["name"]; ?></h1>
<h2><?php echo $result["lugares"]["name"]; ?></h2>

<div id="request" class='well'>
	<h3><?php echo $result["punto_accesos"]["name"]; ?></h3>
	<p>Por favor escriba el número telefónico del residente o el código de acceso.</p>
	<form id="main" action='?c=Accesos&m=punto_de_acceso&id=<?php echo $args['id'];?>' method="POST">
		<?php
		echo php_Field::hidden("c","Accesos");
		echo php_Field::hidden("m","punto_de_acceso");
		echo php_Field::hidden("id",$args['id']);
		echo php_Field::number("codigo","",0);
		echo php_Field::submit("action","Enviar");
		?>
	</form>
	<a href="?c=Accesos&m=residentes" class="btn btn-basic">Acceso Residente</a>
</div>
<div id="loading" class="progress" style="display:none;">
  <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
    Esperando respuesta...
  </div>
</div>
</div>
<script>
	var evento={};
//$(document).ready(function(){
	$("#codigo").focus();
	$("#main").submit(function(ev){
		ev.preventDefault();
		$("#loading").show();
		$("#request").hide();

		$.ajax({
			"url":"?c=Accesos&m=punto_de_acceso&id=<?php echo $args["id"];?>&_data=json",
			"method":"POST",
			"data":{
				"c":"Accesos",
				"m":"punto_de_acceso",
				"id":"<?php echo $args["id"];?>",
				"_data":"json",
				"codigo":$("#codigo").val(),
				"action":"send"
			},
			"success":function(data){
				try{
					var o = JSON.parse(data);
					if(o.persona==null){
						alert("Persona no encontrada");
						$("#loading").hide();
						$("#request").show();
						$("#codigo").focus().select();
						return;
					}
					evento = o.evento;
					check_approval();
					//alert("Success "+data);
				}catch(e){
					$("#loading").hide();
					$("#request").show();
					location.reload();
					//alert(e);
				}
			},
			"error":function(data){
				$("#loading").hide();
				$("#request").show();

				alert(data);
				location.reload();
			},

		});

	});
	
//});
	function check_approval(){
		
		$.ajax({
			"url":"?c=Eventos&m=view&id="+evento.id+"&_data=json",
			"method":"POST",
			"data":{
				"c":"Eventos",
				"m":"view",
				"id":evento.id,
				"_data":"json"
			},
			"success":function(data){
				try{
					var o = JSON.parse(data);
					
					
					if(o.eventos.validez_id-0){
						//alert(o.eventos.validez_id);
						// TODO: Show success window offering to save and/or register your user.
						if(o.eventos.validez_id==1){ // Valid
							window.location.href="?c=Accesos&m=punto_de_acceso&id=<?php echo $args["id"]; ?>&result=success";
						}else{
							window.location.href="?c=Accesos&m=punto_de_acceso&id=<?php echo $args["id"]; ?>&result=reject";
						}
						$("#loading").hide();
						$("#request").show();
					}else{
						setTimeout(check_approval,1000);
					}
				}catch(e){					
					alert(e);
					location.reload();
					//$("#loading").hide();
					//$("#request").show();
				}
			},
			"error":function(data){
				//$("#loading").hide();
				//$("#request").show();

				alert(data);
				location.reload();
			},

		});
	}
	setTimeout(function(){$(".alert").slideUp();},3000);
</script>