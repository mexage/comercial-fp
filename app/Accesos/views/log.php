<style>
.modal-body{
	overflow:auto;
}
</style>
<div class='box-section' id='accesos'>
<h2>Accesos</h2>
<?php
//echo com_Timeline::draw($result['accesos'],"<=personas_id>");
//echo php_Grid::draw($result['accesos']);
foreach($result['accesos'] as $r){
	?>
	<div class='well'>
		<span class='information'>
			<h4><?php echo $r['fraccionamiento'] ?><a href='#' onclick='hide_record(this,<?php echo $r['id'];?>);' style='float:right;color:gray;'>&times;</a></h4>
			<span class='btn btn-primary'>
			<?php echo $r['puerta'] ?> - 
			<?php echo $r['lugar'] ?>  
			</span> 
			<span class='btn btn-info'><?php echo $r['persona'] ?></span> 
			<span class='btn btn-default'><?php echo $r['tiempo'] ?>  
			<span class='badge'><?php echo $r['id'] ?></span></span>
		</span>
		<?php
		if(count($r['img'])){
			foreach($r['img'] as $i){
			?>
				<img class='img-thumbnail' src='<?php echo "?c=accesos&m=camara&id=".$i['id']."&name=".$i['name']."&_data=attachment";?>'/>
				<p><?php echo $i['name'];?></p>
			<?php
			}
		}
		?>
	</div>
	<?php
}

?>
</div>

<?php
echo com_Dialog::shell("image_dlg","Imagen");
?>
<script>
$("#accesos img").click(function(){
	$("#image_dlg .modal-title").html($(this).closest(".well").find(".information").html());
	$("#image_dlg .modal-body").html("<img src='"+$(this).attr("src")+"' />");
	$("#image_dlg").modal("show");
	
});

function hide_record(el,id){
	event.preventDefault();
	$.ajax({
		"url":"?c=Accesos&m=hide&_data=json",
		"method":"POST",
		"data":{"c":"Accesos","m":"hide","id":id},
		"success":function(data){
			try{
				var o=JSON.parse(data);
				if(typeof o.error==="undefined"){
					$(el).closest(".well").slideUp();
				}else{
					alert(o.error);
				}
			}catch(e){
				alert(e);
			}
		},
		"error":function(){
			
		}
	});
	
	
}
</script>