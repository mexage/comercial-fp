<?php
class Accesos extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Accesos");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Accesos";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		
		//return $result;
		
		// TODO: Check for access and decide which lading page to show.
		return "?c=Accesos&m=residentes";
	}
	function add($args){
		$result = array();
		if(!$this->user->attr("personas_id")){
			return "?c=main&m=login&requested_c=Accesos&requested_m=index";
		}
		//$result=parent::add($args);
		if(!is_numeric($args["open"])){
			$this->set_message("Error","danger");
			return "?c=Accesos&m=".$args["m"];
		}
		// Your code here.
		$row=array(
			"punto_accesos_id"=>$args["open"],
			"personas_id"=>$this->user->attr("personas_id")
		);
		
		$inserted=$this->db->insert("accesos",$row);

		$punto_acceso=$this->db->get("punto_accesos",$args["open"]);
		$this->store_camera($punto_acceso["camaras_id"],"accesos",$inserted['id'],"open");

		$ini="[header]"."\n";
		$ini.="id=".$inserted["id"]."\n";
		$ini.="[accesos]"."\n";
		$ini.="punto_accesos_id=".$args["open"]."\n";
		$ini.="personas_id=".$this->user->attr("personas_id")."\n";
		$ini.="created=".date("Y-m-d H:i:s")."\n";
		
		file_put_contents("doors/".$args["open"].".txt",$ini);

		$json=array();
		$json["header"]=array(
			"id"=>$inserted["id"]
		);
		$json["accesos"]=array(
			"punto_accesos_id"=>$args["open"],
			"personas_id"=>$this->user->attr("personas_id"),
			"created"=>date("Y-m-d H:i:s")
		);
		
		file_put_contents("doors/".$args["open"].".json",json_encode($json,JSON_PARTIAL_OUTPUT_ON_ERROR));

		$this->set_message("Abriendo","success");
		return "?c=Accesos&m=".$args["m"];
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
		// Your code here.
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("accesos",$args['id']);
		//return "?c=Accesos";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
		return $result;
	}
	function residentes($args){
		$result = array();		
		
		if($args["open"]){
			return $this->add($args);			
		}
		
		if(!$this->user->attr("personas_id")){
			return "?c=main&m=login&requested_c=Accesos&requested_m=index";
		}
		$result['personas']=$this->db->search("personas","id='".addslashes($this->user->attr("personas_id"))."'","id",1)[0];
		
		$persona = new mod_Personas();

		$result['fraccionamientos']=$persona->lugares($result['personas']['id']);
		/*$result['lugares']=$this->db->query("
			SELECT l.* 
			FROM lugares l inner join lugares__personas lp ON l.id=lp.lugares_id
			WHERE lp.personas_id='".$result['personas']['id']."';
			");
		*/
		
		
		return $result;
	}
	function hide($args){
		$result=array();
		
		if(!$this->user->attr("id")){
			return array("error"=>"ERROR: No esta logueado");
		}
		$acceso=$this->db->get("accesos",$args['id']);
		if(strcasecmp($this->user->attr("personas_id"),$acceso['personas_id'])!=0){
			return array("error"=>"ERROR: El acceso no es del usuario. (u:".$this->user->attr("personas_id").", a:".$acceso['personas_id'].")");
		}
		
		return $this->db->upd("accesos",$args['id'],array("hidden"=>"1","hidden_time"=>date("Y-m-d H:i:s")));
		
	}
	function log($args){
		$result=array();
		
		if(!$this->user->attr("id")){
			return "?";	
		}
		$persona=$this->db->get("personas",$this->user->attr("personas_id"));
		if(count($this->user->get("Admin"))){
			$filter="1";
		}else{
			$filter="pe.id='".$this->user->attr("personas_id")."' AND a.hidden=0";
		}
		$result["accesos"]=$this->db->query("
			SELECT 
				a.id,
				pe.name persona,
				pa.name puerta,
                l.name lugar,
                f.name fraccionamiento,
				a.created tiempo
			FROM accesos a
			INNER JOIN punto_accesos pa ON a.punto_accesos_id = pa.id
            INNER JOIN personas pe ON a.personas_id=pe.id
  			INNER JOIN lugares l ON pa.lugares_id=l.id
            INNER JOIN fraccionamientos f on l.fraccionamientos_id = f.id
			WHERE (".$filter.")
			ORDER BY id DESC
			LIMIT 100");			
		
		foreach($result["accesos"] as &$r){
			$images=dir_attachment("accesos",$r['id']);
			
			$r["images"]="";
			if($images){
				foreach($images as $i){
					/*http://localhost/cherry/?c=Camaras&id=1&name=image.jpg&_data=attachment&time=2020-11-27T07:04:41.363Z*/
					$url="?c=accesos&m=camara&id=".$i['id']."&name=".$i['name']."&_data=attachment";
					$r["images"].="<a href='".$url."'><img class='img-thumbnail' style='height:100px;' src='".$url."' title='".$i['name']."' /><br />".$i['name']."</a> ";
					if(!isset($r['img'])){
						$r['img']=array();
					}
					$r['img'][]=$i;
				}
			}
			
		}
		return $result;
	}
	function punto_de_acceso($args){
		$result=array();
		$args['id']=addslashes($args['id']);
		$args['codigo']=addslashes($args['codigo']);
		$result["punto_accesos"]=$this->db->get("punto_accesos",addslashes($args["id"]));
		$result["lugares"]=$this->db->get("lugares",$result["punto_accesos"]["lugares_id"]);
		$result["fraccionamientos"]=$this->db->get("fraccionamientos",$result["lugares"]["fraccionamientos_id"]);
		
		if($args["action"]){
			/* TODO: Call notification */	
			$result["persona"]=$this->db->search("personas","telefono='".$args['codigo']."' AND telefono<>''")[0];
			if(!$result["persona"]){
				/* 
				TODO: Dynamic list of tipo de lugares personas
				TODO: Filter Fraccionamiento				
				*/
				$result['residencia']=$this->db->query("
				SELECT l.* 
				FROM `lugares__personas` lp 
				inner join lugares l on lp.lugares_id=l.id
				WHERE l.codigo='".$args["codigo"]."' AND lp.tipo_lugares_personas_id=1
				AND l.tipo_lugares_id=2
				AND l.fraccionamientos_id='".$result["lugares"]["fraccionamientos_id"]."'"
				)[0]; // TODO: Remove hardcoding
				
				$result["persona"]=$this->db->get("personas",$this->db->search("lugares__personas","lugares_id='".$result["residencia"]["id"]."'")[0]["personas_id"]);// TODO: Remove hardcoding
			}else{
				/* 
					TODO: Dynamic list of tipo de lugares personas
					TODO: Filter Fraccionamiento				
				*/
				$result['residencia']=$this->db->query("
					SELECT l.* 
					FROM `lugares__personas` lp 
					inner join lugares l on lp.lugares_id=l.id
					WHERE lp.personas_id='".$result["persona"]['id']."' AND lp.tipo_lugares_personas_id=1
					AND l.tipo_lugares_id=2"
				)[0]; // TODO: Remove hardcoding
	
				/*$this->db->get(
					"lugares",
					$this->db->search(
						"lugares__personas",
						"personas_id='".$result["persona"]['id']."' AND tipo_lugares_personas_id=1"
					)[0]["lugares_id"]); // Hardcoded Residente
				*/
				
			}
			if($result["residencia"]["whatsapp"]){
				$new_evento=array(
					"name"=>"Visitante en punto de acceso ".$result["residencia"]['name'],
					"personas_id"=>$result["persona"]["id"],
					"punto_accesos_id"=>$result["punto_accesos"]["id"]
				);
				$new_evento=$this->db->insert("eventos",$new_evento);
				$result['evento']=$new_evento;
				
				// TODO: add hash
				// TODO: change to real link
				php_WhatsApp::send(1,$result["residencia"]["whatsapp"],"Favor de aprobar acceso https://mexage.com.mx/cherry/?c=Accesos&m=aprobar&id=".$new_evento['id']);

				$this->store_camera($result["punto_accesos"]["camaras_id"],"eventos",$new_evento['id'],"requested");
			}else{
				$this->set_message("El teléfono no está asociado a una residencia y/o un grupo de notificación.","danger");
			}
		}
		if(strcasecmp($args["result"],"success")==0){
			$this->set_message("Abriendo puerta...","success");
		}else if(strcasecmp($args["result"],"reject")==0){
			$this->set_message("Acceso denegado","danger");
		}
		return $result;
	}
	function aprobar($args){
		$result=array();
		//TODO: add hash check
		
		$result['evento']=$this->db->get("eventos",addslashes($args['id']));
		//TODO: validar tiempo del evento y validez
		$result['punto_accesos']=$this->db->get("punto_accesos",$result['evento']['punto_accesos_id']);
		//TODO: agregar validaciones
		$result['camaras']=$this->db->get("camaras",$result['punto_accesos']['camaras_id']);
		//TODO: validar persona
		$result['personas_id']=$this->db->get("personas",$result['evento']['personas_id']);
		
		if(isset($args['action'])){
			if(strcasecmp($args['action'],"Aprobar")==0){
				$this->db->upd("eventos",$result['evento']['id'],array("validez_id"=>"1"));
				//$this->set_message("Se aceptó la solicitud.","success");
				$args["open"]=$result['evento']['punto_accesos_id'];
				
				$this->add($args);
				
			}else{
				$this->db->upd("eventos",$result['evento']['id'],array("validez_id"=>"2"));
				$this->set_message("Se rechazó la solicitud.","danger");
				$this->store_camera($result["camaras"]["id"],"eventos",$result["evento"]["id"],"reject");
			}
			
			
			// TODO: Add real link
			return "?";
		}

		return $result;
	}
	function store_camera($camera,$object,$id,$name){
		if(!$camera)return;
		if(!$object)return;
		if(!$id)return;
		if(!$name)return;

		$dest_folder="s/".$object."/".$id;
		$error=false;
		if(!file_exists($dest_folder)){
			trace("Creating folder ".$dest_folder);
			$error|=mkdir($dest_folder,0755,true);
		}
		$error=file_put_contents($dest_folder."/index.php","");		
		if($error){
			trace("There was a problem setting up the folder. ".$dest_folder."/index.php");			
		}
		$error=copy("s/camaras/".$camera."/image.jpg",$dest_folder."/".$name.".jpg");		
		if($error){
			trace("There was some error storing the camera image. "."s/camaras/".$camera."/image.jpg"." -> ".$dest_folder."/".$name.".jpg");			
		}
		
	}
}
	