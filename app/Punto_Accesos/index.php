<?php
class Punto_Accesos extends php_Controller{
	function __construct($args){
		// Don't remove this, to check.
		parent::__construct($args);
		
		// Enable this if you want to require users to login and have a permission setup (user and user_permissions tables)
		// Also you can use it to limit specific methods (put at the beginning of the method)
		// $this->_require_access("Punto_Accesos");
		// To require user to be logged in 
		// global $user;
		// $this->has_access=$user->is_logged();
		// Change for custom code
		$this->title = ucwords(str_replace("_"," ",$args['m']))." - Punto Accesos";
	}
	function index($args, $filter = '1', $order = '', $limit = 5000){
		$result = array();
		
		$result=parent::index($args);
		
		// Your code here.
		
		return $result;
	}
	function add($args){
		$result = array();
		
		$result=parent::add($args);
		
		// Your code here.
		
		return $result;
	}
	function edit($args){
		$result = array();
		
		$result=parent::edit($args);
		
        // Your code here.
        $result["files"]=dir_attachment($args['c'],$args['id']);
		
		return $result;
	}
	function remove($args){
		// Note this function is not implemented by default.
		//$this->db->del("punto_accesos",$args['id']);
		//return "?c=Punto_Accesos";
	}
	function view($args){
		$result = array();
		
		$result=parent::view($args);
		
		// Your code here.
		
		return $result;
    }
    function attach($args){
        ensure_attachment_folder("punto_accesos",$args['id']);
        store_attachment("attachment","punto_accesos",$args['id'],"image.jpg",array("jpg"));
        return "?c=".$args['c']."&m=edit&id=".$args['id'];
    }
    function unattach($args){
        unlink(get_attachment_folder($args['c'],$args['id'])."/image.jpg");
        return "?c=".$args['c']."&m=edit&id=".$args['id'];
    }
	
}
	