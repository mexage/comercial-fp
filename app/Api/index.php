<?php 
class Api extends php_Controller{
	function index($args){
		
	}
	public function create($args){
		// $args:
		//		areas_id
		//		location
		//		reason
		//		action
		if(
			!isset($args['areas_id'])
			||!isset($args['location'])
			||!isset($args['reason'])
			||!isset($args['action'])
			||!is_numeric($args['areas_id'])
		){
			return array('error'=>'Required fields not present or invalid.');	
		}
		switch($args['key']){
			case 'B0C5F6B7266E3A6A2A162CBED1E45C3155A2EBE1563F60B3072D5C326406D3BB':{//ERMS201810121452
				$alerts = new Alerts();
				return $alerts->create($args);
			}
		}
		
			return array('error'=>'Invalid key');
	}
	
}