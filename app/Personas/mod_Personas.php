<?php 
class mod_Personas extends php_Model{
    
    public function lugares($personas_id){
        $fraccionamientos=$this->db->query("
            SELECT DISTINCT f.* 
            FROM fraccionamientos f 
            INNER JOIN lugares l ON l.fraccionamientos_id = f.id
            INNER JOIN lugares__personas lp ON l.id=lp.lugares_id
            WHERE lp.personas_id='".addslashes($personas_id)."';
        ");     
        foreach($fraccionamientos as &$fraccionamiento){
            $lugares = $this->db->query("
                SELECT l.*, tpl.name tipo_lugares_personas
                FROM lugares l
                INNER JOIN lugares__personas lp ON l.id=lp.lugares_id
                LEFT OUTER JOIN tipo_lugares_personas tpl ON lp.tipo_lugares_personas_id=tpl.id
                WHERE lp.personas_id='".addslashes($personas_id)."'
                AND tpl.control=1
                AND l.fraccionamientos_id='".$fraccionamiento['id']."';
            ");
            foreach($lugares as &$lugar){
                $lugar['punto_accesos']=$this->db->search("punto_accesos","lugares_id='".$lugar['id']."'");
                $lugar['camaras']=$this->db->search("camaras","lugares_id='".$lugar['id']."'");
            }
            $fraccionamiento['lugares']=$lugares;
        }
        return $fraccionamientos;
    }
    
    
}