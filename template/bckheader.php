<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
	<?php load_lib_html("js_libraries_headers.php"); ?>
	<link rel="stylesheet" type="text/css" href="template/css/andon.css">
	
	
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><img src='template/img/doit_logo.png' /> </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      
	  
	  <ul class="nav navbar-nav">
		<?php if(strcasecmp("Bins",$args['c'])!=0){ ?>
			<li class="active"><a href="?">Andon System</a></li>
			<li><a href="?c=Alerts" target='_blank' >Monitor Global</a></li>
			<li><a href="?c=Alerts&m=dashboard" target='_blank'>Dashboard</a></li>
			<li><a href="?c=Alerts&m=report&dm=response_times" target='_blank'>Analizar</a></li>
			<li><a href="?c=main&m=manual" target='_blank'>Ayuda</a></li>
		<?php }else{ ?>
			<li class="active"><a href="?c=Bins&m=counting">Bin System</a></li>
			<!--<li><a href="?c=Bins&m=serials" target='_blank' >Serial Numbers</a></li>-->
			<li><a href="?c=Bins&m=serial_dashboard" target='_blank'>Dashboard</a></li>
			<li><a href="?c=Bins&m=serial_dashboard" target='_blank'>Completed</a></li>
			<li><a href="?c=Bins&m=order_messages" target='_blank'>Order Messages</a></li>
		<?php } ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> </a></li>
      </ul>
    </div>
  </div>
</nav>

<script>
S=new Stopwatch('<?php echo date("Y-m-d H:i:s"); ?>');
</script>


<div class="container-fluid text-center"> 
<!-- Header end -->