<?php if(strcasecmp($_GET['ajax'],"noheader")!=0){ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <link rel='shortcut icon' href='favicon.png' type='image/x-icon' />
	<?php load_lib_html("js_libraries_headers_b4.php"); ?>
	<link rel="stylesheet" type="text/css" href="template/css/andon.css" />
	<link rel="stylesheet" type="text/css" href="template/css/timeline.css" />
	<link rel="stylesheet" type="text/css" href="template/fontawesome/css/all.min.css" />
	<?php if($title){ 
		echo "<title>".$title."</title>";
	
	}else{
		echo "<title>".ucwords(str_replace("_"," ",$m))." - ".ucwords(str_replace("_"," ",$c))."</title>";
	}?>
	
</head>
<body>
<?php } ?>
<?php if(!$_GET['ajax']){ ?>
<nav class="mb-2 navbar navbar-expand-lg <?php if(getConfig("DB/TEST_ENVIRONMENT_NAME")){echo "navbar-light bg-light";}else{echo "navbar-dark bg-dark";}?>">
	<a class="navbar-brand" href="#"><?php 
	if(getConfig("DB/TEST_ENVIRONMENT_NAME")){
		echo "<h3 style='margin-top: 0.3em !important;'>".getConfig("DB/TEST_ENVIRONMENT_NAME")."</h3>";
	}else{
		echo "<img id='doitLogo' src='template/img/doit_logo.png' />";
	}?> </a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="myNavbar">
		<ul class="navbar-nav mr-auto">
		<?php if(strcasecmp("Bins",$args['c'])==0){ ?>
			<li class="nav-item active"><a class="nav-link" href="?c=Bins&m=counting">Bin System</a></li>
			<!--<li><a href="?c=Bins&m=serials" target='_blank' >Serial Numbers</a></li>-->
			
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Dashboards <span class="caret"></span></a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?c=Bins&m=serial_dashboard" target='_blank'>Dashboard</a>
					<a class="dropdown-item" href="?c=Bins&m=audit_dashboard" target='_blank'>Audit Dashboard <!--<span class='badge'>new!</span>--></a>
					<a class="dropdown-item" href="?c=Bins&m=picking_dashboard" target='_blank'>Picking Dashboard <!--<span class='badge'>new!</span>--></a>			
					<a class="dropdown-item" href="?c=Bins&m=performance_dashboard" target='_blank'>Performance Dashboard <span class='badge'>beta</span></a>			
					<a class="dropdown-item" href="?c=Bins&m=shipping_dashboard">Shipping Dashboard <span class='badge'>new!</span></a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Picklist <span class="caret"></span></a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?c=Bins&m=picklist&building=A5">A5 Picklist</a> 
					<a class="dropdown-item" href="?c=Bins&m=picklist&building=A1">A1 Picklist</a> 
					<a class="dropdown-item" href="?c=Bins&m=assign_picklist">Online Picklist</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Kanban <span class="caret"></span></a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?c=Bins&m=kanban">Location Kanban</a>
					<a class="dropdown-item" href="?c=Bins&m=receiving_kanban">Receiving Kanban</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Reports <span class="caret"></span></a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?c=Bins&m=workorders">Workorders</a>
					<a class="dropdown-item" href="?c=Bins&m=orders_report">Orders</a>
					<a class="dropdown-item" href="?c=Bins&m=serials_report">Serials</a>
					<a class="dropdown-item" href="?c=Bins&m=sorting_list">Sorting List</a>
					<a class="dropdown-item" href="?c=Bins&m=checkout_issues">Checkout Issues</a>
					<a class="dropdown-item" href="?c=Bins&m=inventory_aging">Inventory Aging <span class='badge'>new!</span></a>
					<a class="dropdown-item" href="?c=Bins&m=aging_by_order">Aging by Order <span class='badge'>new!</span></a>
					<a class="dropdown-item" href="?c=Bins&m=checkout_not_shipped">Checkout, not shipped <span class='badge'>new!</span></a>
					<a class="dropdown-item" href="?c=Bins&m=daily_shipping_report">Daily shipping report <span class='badge'>new!</span></a>
					<a class="dropdown-item" href="?c=Bins&m=remaining_report">Remaining Report <span class='badge'>beta</span></a>
					<a class="dropdown-item" href="?c=Bins&m=sct">Ship Complete Twice Candidates <span class='badge'>beta</span></a>
				</div>
			</li>
			
			<?php if(php_Auth::get("sys_admin")||php_Auth::get("user_admin")||php_Auth::get("bin_admin")||php_Auth::get("order_messages")){ ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Admin <span class="caret"></span></a>
				<div class="dropdown-menu">
					<?php if(php_Auth::get("bin_admin")){ ?>
						<a class="dropdown-item" href="?c=Bins&m=bin_manager">Bin Manager</a>
					<?php } ?> 
					<?php if(php_Auth::get("order_messages")){ ?>
						<a class="dropdown-item" href="?c=Bins&m=order_messages">Order Messages</a>
					<?php } ?> 
					<?php if(php_Auth::get("user_admin")){ ?>
						<a class="dropdown-item" href="?c=Bins&m=users_control">User Control</a>
					<?php } ?> 
					<?php if(php_Auth::get("sys_admin")){ ?>
						<a class="dropdown-item" href="?c=Bins&m=configurations">Configuration</a>
						<a class="dropdown-item" href="?c=Bins&m=jobs">Jobs</a>
					<?php } ?>
				</div>
			</li>
			<?php } ?>
			<!-- <li><a href="?c=Bins&m=help" target='_blank'>Help</a></li> -->
			<!--<li><a href="?c=Bins" target='_blank'>Dashboard</a></li>
			<li><a href="?c=Bins" target='_blank'>Completed</a></li>-->
			<!--<li><a href="?c=Bins&m=order_messages" target='_blank'>Order Messages</a></li>-->
			
		<?php }else if(strcasecmp("Attendance",$args['c'])==0 || strcasecmp("Medical_Attentions",$args['c'])==0
					|| strcasecmp("Medical_Profiles",$args['c'])==0 || strcasecmp("Medical_Tests",$args['c'])==0
					|| strcasecmp("Cases",$args['c'])==0 || strcasecmp("Contact_Networks",$args['c'])==0){ ?>
			<li class="nav-item <?php if(strcasecmp("Attendance",$args['c'])==0) { echo 'active'; }?>"><a class="nav-link" href="?c=Attendance">Attendance</a></li>
			<?php if(count($user->get("Attendance"))){ ?>
				<li class="nav-item dropdown <?php if(strcasecmp("Medical_Attentions",$args['c'])==0) { echo 'active'; }?>"><a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Medical <span class="caret"></span></a>
					<div class='dropdown-menu'>
						<a class="dropdown-item" href="?c=Medical_Attentions">Search Employee</a>
						<a class="dropdown-item" href="?c=Medical_Attentions&m=dashboard" >Shift Reports</a>
						<a class="dropdown-item" href="?c=Medical_Attentions&m=test_actions" >Test Actions Report</a>
						<a class="dropdown-item" href="?c=Medical_Profiles&m=report&dm=json" >Blank Report <span class='badge'>new!</span></a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=antibodies_station" >Antibody Station <span class='badge'>new!</span></a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=pcr_station" >PCR Station</a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=employees_without_pcr" >PCR Results Without Employee</a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=main_reports" >Main Test Reports</a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=raw_data" >Raw Data</a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=pending_pcr" >Pending PCR over 1 week</a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=daily_tests" >Daily Tests</a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=campus_summary_abc" >Campus Summary ABC</a>
						<a class="dropdown-item" href="?c=Medical_Tests&m=test_results" >Tests Results</a>
						<a class="dropdown-item" href="?c=Medical_Profiles&m=no_employee" >Profiles missing Employee ID</a>
						<a class="dropdown-item" href="?c=Attendance&m=symptoms" >Symptoms Report</a>
						<a class="dropdown-item" href="?c=Attendance&m=preeliminary_symptoms" >Preliminary Symptoms</a>
						<a class="dropdown-item" href="?c=Medical_Attentions&m=diagnostic_and_symptoms" >Diagnostics and Symptoms <span class='badge'>new!</span></a>
						<a class="dropdown-item" href="?c=Medical_Attentions&m=report" >Medical Attentions Report</a>
					</div>
				</li>
				<li class="nav-item dropdown <?php if(strcasecmp("Cases",$args['c'])==0) { echo 'active'; }?>"><a href='#' class="nav-link dropdown-toggle" data-toggle="dropdown">Cases <span class="caret"></span></a>
					<div class='dropdown-menu'>
						<a class="dropdown-item" href="?c=Cases">Search Employee</a>
						<a class="dropdown-item" href="?c=Cases&m=confirmed">Confirmed Report</a>
						<a class="dropdown-item" href="?c=Cases&m=notification_center">Notification Center</a>
						<a class="dropdown-item" href="?c=Cases&m=production_notification">Production Notification Report</a>
						<a class="dropdown-item" href="?c=Cases&m=dashboard" >Tests Dashboard</a>
						<a class="dropdown-item" href="?c=Cases_Tracking_Plans" >Cases Tracking Plans</a>
						<a class="dropdown-item" href="?c=Cases_Tracking&m=report" >Cases Tracking Report <span class='badge'>new!</span></a>
					</div>
				</li>
			<?php } ?>

			<?php if(count($user->get("CallCenter"))){ ?>
			<li class="nav-item <?php if(strcasecmp("Call_Center",$args['c'])==0) { echo 'active'; }?>"><a class="nav-link" href="?c=Call_Center&m=index">Call Center</a></li>
			<?php } ?>

			<?php if(count($user->get("Attendance"))){ ?>
			<li class="nav-item"><a class="nav-link" href="?c=Attendance&m=dashboard">Dashboard</a></li>
			<?php } ?>
		<?php }else if(
			strcasecmp($args['c'],"Alerts")==0 
			||(strcasecmp($args['c'],"Main")==0&&strcasecmp($args['m'],"login")!=0&&strcasecmp($args['m'],"repost")!=0)
			||(strcasecmp($args['c'],"")==0&&strcasecmp($args['m'],"login")!=0&&strcasecmp($args['m'],"repost")!=0)
		){ ?>
			<li class="nav-item active"><a class="nav-link" href="?">Andon System</a></li>
			<li class="nav-item"><a class="nav-link" href="?c=Alerts" target='_blank' >Monitor Global</a></li>
			<li class="nav-item"><a class="nav-link" href="?c=Alerts&m=dashboard" target='_blank'>Dashboard</a></li>
			<li class="nav-item"><a class="nav-link" href="?c=Alerts&m=report&dm=response_times" target='_blank'>Analizar</a></li>
			<li class="nav-item"><a class="nav-link" href="?c=main&m=manual" target='_blank'>Ayuda</a></li>
		<?php } ?>
		</ul>
		<ul class="nav navbar-nav navbar-right">
		<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> </a></li>
		</ul>
	</div>
</nav>

<script>
S=new Stopwatch('<?php echo date("Y-m-d H:i:s"); ?>');
</script>


<div id='main_template_content' class="container-fluid text-center"> 
<!-- Header end -->
<?php } ?>