<?php if(!$_GET['ajax']){ ?>
<!-- Footer -->
</div>
<?php 
if(isset($args['debug'])){
	echo "<h4>Debug info:</h4>";
	echo "<pre>".var_export($result,true)."</pre>";
}


?>
<footer class="container-fluid text-center">
  <p>Foxconn DoIT - (c)<?php echo date("Y");?> <br /> ID: <?php echo session_id();?> <br /> <?php 
	
	if(strcasecmp($args['m'],"login")!=0){
		if(strcasecmp($args['c'],"Bins")==0){		
			if(php_Auth::is_logged()){ 
				echo "<a href='".php_Auth::request_logout($args)."' class='btn btn-info'>Logout ".php_Auth::current_user()."</a>";
			}else{
				echo "<a href='".php_Auth::request_login($args)."' class='btn btn-info'>Login</a>";
			} 
		}else{
			// Andon
			if($user->is_logged()){ 
				echo "<a href='".php_AccessControl::logout_url($args)."' class='btn btn-info'>Logout ".$user->attr("id")."</a>";
			}else{
				echo "<a href='".php_AccessControl::login_url($args)."' class='btn btn-info'>Login</a>";
				if($sso_user=php_AD::get_sso_user()){
					$user_parts = explode("\\",$sso_user);
					echo " <a href='?c=main&m=login&requested_c=".$args['c']."&requested_m=".$args['m']."&login=Quick+login+as+".php_AD::get_sso_user()."' class='btn btn-success'>Quick login as ".$user_parts[count($user_parts)-1]."</a>";
				}
			} 
		}
		
	}
	
	?> <br /> Mem: <?php echo memory_get_usage(); ?></p>
</footer>
<script>
$( ".tabledit-edit-button").click(function(){var el=this;setTimeout(function(){$(el).closest("table").resize();},500);});
S.start();
//$( ".mysql_date" ).datepicker({"dateFormat":"yy-mm-dd"});
</script>
</body>
</html>
<?php } ?>