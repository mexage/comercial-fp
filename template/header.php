<?php if(strcasecmp($_GET['ajax'],"noheader")!=0){ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <link rel='shortcut icon' href='favicon.png' type='image/x-icon' />
	<?php load_lib_html("js_libraries_headers.php"); ?>
	<link rel="stylesheet" type="text/css" href="template/css/andon.css">
	<link rel="stylesheet" type="text/css" href="template/css/timeline.css">
	<link rel="stylesheet" type="text/css" href="template/fontawesome/css/all.min.css" />
	<link rel="stylesheet" type="text/css" href="template/fontawesome/css/menu.css" />
	<?php if($title){ 
		echo "<title>".$title."</title>";
	
	}else{
		echo "<title>".ucwords(str_replace("_"," ",$m))." - ".ucwords(str_replace("_"," ",$c))."</title>";
	}?>
	
</head>
<body>
<?php } ?>
<?php if(!$_GET['ajax']){ ?>
<!--<div style='height:100px;background-image:url(images/cherry/harmoni.png);'></div>-->
<nav class="navbar <?php if(getConfig("DB/TEST_ENVIRONMENT_NAME")){echo "navbar-default";}else{echo "navbar-inverse";}?>">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="?"><?php 
		
		$main_link = "?";
		if(!getConfig("DB/TEST_ENVIRONMENT_NAME")){
			echo "<img style='height:2em;max-width:50vw;' id='fplogo' src='images/fp/logo2.png' />";
		}else{
			echo "<h3 style='margin-top: 0.3em !important;'><img style='height:1.6em;max-width:50vw;' id='fplogo' src='images/fp/logo2.png' /> ";
			echo "".getConfig("DB/TEST_ENVIRONMENT_NAME")."</h3>";
		
		}?> </a>

    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      
	  
	  <ul class="nav navbar-nav">	
			<?php if(count($user->get("Reportes"))||count($user->get("Admin"))){?>
				<li><a href='#' class="dropdown-toggle" data-toggle="dropdown">M&oacute;dulos<span class="caret"></span></a>
					<ul class='dropdown-menu'>
						<li><a href="?c=clientes"><span class='glyphicon glyphicon-user'></span> Clientes</a></li>
						<li><a href="?c=clientes&m=add"><span class='glyphicon glyphicon-plus'></span> Agregar Cliente</a></li>
						<li><a href="?c=clientes&datasheet=jExcel"><span class='glyphicon glyphicon-th-list'></span> Clientes (Excel)</a></li>
						<li><a href="?c=pagos"><span class='glyphicon glyphicon-piggy-bank'></span> Pagos</a></li>
						<li><a href="?c=productos&datasheet=jExcel"><span class='glyphicon glyphicon-gift'></span> Productos (Excel)</a></li>
						<li><a href="?c=rutas"><span class='glyphicon glyphicon-road'></span> Rutas</a></li>
						<li><a href="?c=ventas"><span class='glyphicon glyphicon-shopping-cart'></span> Ventas</a></li>
					</ul>
				</li>
				<li><a href='#' class="dropdown-toggle" data-toggle="dropdown">Reportes<span class="caret"></span></a>
					<ul class='dropdown-menu'>
						<li><a href="?c=Ventas&m=diario"><span class='glyphicon glyphicon-list-alt'></span> Venta Diaria</a></li>
						<li><a href="?c=Clientes&m=estados"><span class='glyphicon glyphicon-list-alt'></span> Estados de Cuenta Clientes</a></li>
						<li><a href="?c=Ventas&m=report"><span class='glyphicon glyphicon-th'></span> Cubo de Ventas <span class='badge'>Nuevo!</span></a></li><li><a href="?c=Ventas&m=dashboard"><span class='glyphicon glyphicon-dashboard'></span> Tablero de ventas <span class='badge'>Nuevo!</span></a></li>
						<li><a href="?c=Pagos&m=diario"><span class='glyphicon glyphicon-list-alt'></span> Pagos Diario</a></li>
					</ul>
				</li>
			
			<?php } ?>
      </ul>
	  
      <ul class="nav navbar-nav navbar-right">
        <?php if($user->is_logged()){ ?>
        <li><a href="?c=main&m=logout"><span class="glyphicon glyphicon-log-out"></span> Logout <?php echo $user->attr("id");?></a></li>
        <?php }else{?>
          <li><a href="?c=main&m=login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        <?php }?>
      </ul>
    </div>
  </div>
</nav>

<script>
S=new Stopwatch('<?php echo date("Y-m-d H:i:s"); ?>');
</script>

<div id='main_box'>
<div id='main_template_content' class="container-fluid text-center"> 
<!-- Header end -->
<?php } ?>