<?php
// Template REGEX patterns App-Controller-Method calculates default template path:
// /template/*/header.php and /template/*/footer.php
// It can be overwritten by logic in Controller by changing php_Controller->template variable.

setConfig("TEMPLATE_PATHS",
	array(
		'/.*/i'=>'',
		'/.*-.*-app/i'=>'vuetify',
		'/.*-Contact_Networks-.*/i'=>'b4',
		'/.*-Attendance-.*/i'=>'b4',
	)
);

