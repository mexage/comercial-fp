<?php

class com_Device{
	
	public static function issues($device_types,$categories){
		
		if(!count($device_types))return;
		foreach($device_types as $device_type){
			//echo "<input class='btn btn-lg btn-".$r['css_class']."' type='submit' value=\"".str_replace('"',"'",$r['name'])."\" name='action' onclick='clearTimeout(autoRefresh);$(\"#create_reason\").val(prompt(\"Mensaje:\"));'/>";
			if(self::in_categories($device_type,$categories)){
			
			?>
			<div class="media" id='problemas-device-<?php echo $device_type['id']; ?>'>
				<div class="media-left">
					<img src="<?php echo $device_type['image']?>" class="media-object" style="width:200px">
				</div>
				<div class="media-body">
					<h4 class="media-heading"><?php echo $device_type['name']?></h4>
					<div class='btn-group-vertical' id='printer'>
						<?php self::categories_list($device_type,$categories); ?>
					</div>
				</div>
				
			</div>
			<?php
			}
			
			
		}
	
	}
	public static function in_categories($device_type,$categories){
		if(!count($categories))return false;
		foreach($categories as $category){
			if(strcasecmp($device_type['id'],$category['device_types_id'])==0 && $category['device_types_id']){
				return true;
			}
		}
		return false;
	}
	public static function categories_list($device_type,$categories){
		if(!self::in_categories($device_type,$categories))return;
		
		echo "<div class='btn-group-vertical'>";
		foreach($categories as $category){
			if(strcasecmp($device_type['id'],$category['device_types_id'])==0 && $category['device_types_id']){
				echo "<button class='btn btn-".$category['css_class']."' onclick='Buttons.select_report(".$category['id'].",\"".$category['name']."\",\"".$category['css_class']."\"); return false;'>".$category['name']."</button>";
			}
		}
		echo "</div>";
		
	}
	public static function button($device_type,$attr=array()){
		// Shows Andon button
		?>
		<button class='well img-responsive' type="button"<?php 
			echo self::explode_attr($attr);
		?>>
			<h4><?php echo $device_type['name']/*. " ".$device_type['id']*/;?></h4>
			<img class='img-responsive' src='<?php echo $device_type['image'];?>' />
		</button>
		<?php	
	}
	
	public static function dropdown($id,$device_types,$value="",$attr=array(),$label=""){
		if($label){
			echo "<div class='form-group'>"; 
			echo "<label>".$label."</label>";
		}
		
		$generated_id=$id."-".round(microtime(true) * 1000);		
		?>
		<select name='<?php echo $id; ?>' id='<?php echo $generated_id; ?>' <?php echo self::explode_attr($attr); ?>>
		
		<?php
			foreach($device_types as $device_type){
				$selected="";
				if(strcasecmp($value,$device_type['id'])==0){
					$selected=" selected='selected'";
				}
				
				echo "<option".$selected." value='".$device_type['id']."' data-style='background-size:contain;background-position:left top;background-image:url(&apos;".$device_type['image']."&apos;)'>".$device_type['name']."</option>";
			}
		?> 
		</select>
		<script>
			$.widget( "custom.iconselectmenu", $.ui.selectmenu, {
			  _renderItem: function( ul, item ) {
				var li = $( "<li>" ),
				  wrapper = $( "<div>", { text: item.label } );
		 
				if ( item.disabled ) {
				  li.addClass( "ui-state-disabled" );
				}
		 
				$( "<span>", {
				  style: item.element.attr( "data-style" ),
				  "class": "ui-icon " + item.element.attr( "data-class" )
				})
				  .appendTo( wrapper );
		 
				return li.append( wrapper ).appendTo( ul );
			  }
			  
			  
			});
			<?php echo "\$('#".$generated_id."')"; ?>.iconselectmenu({
				change:function( event, ui ) {
					<?php echo $attr['onchange']; ?>
				}				
			}).iconselectmenu( "menuWidget" ).addClass( "ui-menu-icons customicons" );
				
			
		</script>
		
		<?php
		if($label){
			echo "</div>";
		}
		return $generated_id;
	}
	private static function explode_attr($attr){
		$result = "";
		if($attr){
			foreach($attr as $k=>$v){
				$result.=" ".$k."='".$v."'";
			}
		}		
		return $result;
	}
	

}