<?php
class com_EmployeeSearch{
	static function dialog($id,$employee_id=""){
		$employee_text_id="employee_search_button_".$id;
		
		return '
			<div id="'.$id.'" class="modal fade" role="dialog">
				<div class="modal-dialog">
				<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Search Employee ID</h4>
				</div>
				<div class="modal-body">
				<p>
					Search:<input type="text" id="'.$employee_text_id.'" value="'.$employee_id.'" />
				</p>
				'.com_EmployeeSearch::autocomplete("#".$employee_text_id).'
				</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</div>

				</div>
			</div>
		';
	}
	static function autocomplete($css_query){
		return '<script>
			$( "'.$css_query.'" ).autocomplete({
			  source: function( request, response ) {
				$.ajax( {
				  url: "?c=Employees&m=_list&_data=json",
				  data: {
					  
					term: request.term
				  },
				  success: function( data ) {
					try{
						var o = JSON.parse(data);
						//{"id":"Caprimulgus europaeus","label":"European Nightjar","value":"European Nightjar"}
						var t = [];
						for(var i in o){
							var r ={};
							r.id=o[i].id;
							r.label=o[i].id+" "+o[i].name;
							r.value=o[i].id;
							t.push(r);
						}
						response( t );
					}catch(e){
						
					}
				  }
				} );
			  },
			  minLength: 2,
			  select: function( event, ui ) {
				//log( "Selected: " + ui.item.value + " aka " + ui.item.id );
			  },
			  change: function (event, ui){
				if (isNaN(this.value)){this.value = ""; }
			  }
			} ).keydown(
				function(e){
					if(e.keyCode == 13 ) {
						if(isNaN($(this).val())){
							e.preventDefault();
						}
					}
				}
			);
			
		</script>';
		
	}
	static function autocomplete_external($css_query,$field=""){
		if(!$field){
			$field = ltrim($css_query, "#");
		}
		return '<script>
			$( "'.$css_query.'" ).autocomplete({
			  source: function( request, response ) {
				$.ajax( {
				  url: "?c=Medical_Profiles&m=_list&_data=json",
				  data: {
					field: "'.$field.'",
					term: request.term
				  },
				  success: function( data ) {
					try{
						var o = JSON.parse(data);
						//{"id":"Caprimulgus europaeus","label":"European Nightjar","value":"European Nightjar"}
						var t = [];
						for(var i in o){
							var r ={};
							r.id=o[i].id;
							r.label=o[i].social_security_number+" "+o[i].name;
							r.value=o[i].id;
							t.push(r);
						}
						t.push({
							"id":"-1",
							"label":"New medical profile: "+request.term,
							"value":"-1"
							
						});
						response( t );
					}catch(e){
						
					}
				  }
				} );
			  },
			  minLength: 2,
			  select: function( event, ui ) {
				  if(ui.item.value==-1){
					  var term=ui.item.label.replace("New medical profile: ","");
					  term=term.trim();
					  
					  if(isNaN(term)){
						this.value = ""; 
						window.open("?c=Medical_Profiles&m=add&name="+term.toUpperCase());
					  }else{
						this.value = "";   
						window.open("?c=Medical_Profiles&m=add&social_security_number="+term);  
					  }
				  }
				//log( "Selected: " + ui.item.value + " aka " + ui.item.id );
			  },
			  change: function (event, ui){
				if (isNaN(this.value)){this.value = ""; }
			  }
			} ).keydown(
				function(e){
					if(e.keyCode == 13 ) {
						if(isNaN($(this).val())){
							e.preventDefault();
						}
					}
				}
			);
			
		</script>';
		
	}
	static function button($id){
		return '<button type="button" class="btn btn-info" data-toggle="modal" data-target="#'.$id.'"><span class="glyphicon glyphicon-search"></span></button>';
	}

}
/*

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
*/