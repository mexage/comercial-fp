<?php
class com_Card{
	static function card($title,$sections,$url=null,$size="6",$type="default"){
		$result = "";
		$result.="<div class='col-sm-".$size."'>";
		$result .= "<div class='panel panel-".$type."'>";
		
		$result .="<div class='panel-heading'><h3>".$title."</h3></div><div class='panel-body'>";
		foreach($sections as $subtitle=>$section){
			$result .="<h3>".$subtitle."</h3><pre>".$section."</pre>";
		}
		if(is_array($url)){
			$result.="<div class='panel-footer'><a class='btn btn-primary' href='".$url['href']."'>".$url['text']."</a></div>";
		}
		$result .="</div></div></div>";
		
		return $result;		
	}
	static function error($error){
		$size=6;
		$type="info";
		$result = "";
		$result.="<p class='col-sm-".$size."'>";
		$result .= "<div name='error' class='panel panel-".$type."'>";
		
		$result .="<div class='panel-heading'><h3>".$error['message']."</h3></div><div class='panel-body'>";
		
		$result .="<h3>Que paso?</h3><pre>".$error['cause']."</pre>";
		$result .="<h3>Que hago?</h3><pre>".$error['actions']."</pre>";
		
		if(is_array($url)){
			$result.="<div class='panel-footer'><a class='btn btn-primary' href='".$url['href']."'>".$url['text']."</a></div>";
		}
		$result .="</div></div></p>";
		
		return $result;		
	}

}