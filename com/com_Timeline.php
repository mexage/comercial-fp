<?php 

class com_Timeline{
	function draw($data,$title,$time,$details,$buttons=array(),$inverted=false,$icon="",$css_class=""){
		if(count($data)){
			echo '<ul class="timeline">';
			foreach($data as $row){
				echo com_Timeline::entry($row,$title,$time,$details,$buttons,$inverted,$icon,$css_class);
			}
			echo '</ul>';
		}else{
			echo "<div class='well'>No data.</div>";
		}
	}

	function entry($row,$title,$time,$details,$buttons=array(),$inverted=false,$icon="",$css_class=""){
	$li_class="";
	if($row){
		$title=php_Grid::replace($title,$row);
		$time=php_Grid::replace($time,$row);
		$details=php_Grid::replace($details,$row);
		$css_class=php_Grid::replace($css_class,$row);
		$icon=php_Grid::replace($icon,$row);
		$inverted=php_Grid::replace($inverted,$row);
	}
	if($inverted)$li_class=" class='timeline-inverted' ";
			?>
			<li <?php echo $li_class;?>>
				<?php if($css_class){ ?>
				<div class="timeline-badge <?php echo $css_class?>"><i class="glyphicon glyphicon-<?php echo $icon;?>"></i></div>
				<?php } ?>
			  <div class="timeline-panel">
				<div class="timeline-heading">
				  <h4 class="timeline-title"><?php echo $title;?></h4>
				</div>
				<div class="timeline-body">
				  <p><?php echo $details;?></p>
					<div class='row text-right'>
						<small class="text-muted" style='float:left'><i class="glyphicon glyphicon-time"></i> <?php echo $time;?></small>
						<?php if(count($buttons)){
							foreach($buttons as $key=>$url){
								$parts=explode(":",$key);
								$button_text=$key;
								$button_css="primary";
								
								if(count($parts)>=2){
									$button_css=$parts[0];
									$button_text=$parts[1];
								}
								if($row){
									$button_text=php_Grid::replace($button_text,$row);
									$url=php_Grid::replace($url,$row);
								}
								echo "<a class='label label-".$button_css."' href='".$url."'>".$button_text."</a>";
							}
						?>
						<?php }?>
						 
					</div>
				</div>
			  </div>
			</li>
			<?php
		}

	
}