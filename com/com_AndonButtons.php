<?php

class com_AndonButtons{
	public static function legacy($areas_id,$location,$categories){
		?>
		<div class='legacy'>
		<!--<form action='?c=alerts&m=create' method="POST">
			<input type='hidden' name='areas_id' value='<?php echo $areas_id; ?>' />
			<input type='hidden' name='location' value='<?php echo $location; ?>' />
			<input type='hidden' name='reason' value='' id='create_reason' />-->
			<?php
				foreach($categories as $r){
					if(!$r['device_types_id']){
						echo "<button class='btn btn-lg btn-".$r['css_class']."' type='submit' onclick='Buttons.select_legacy(\"".str_replace('"',"'",$r['name'])."\",\"".$r['css_class']."\");'>".str_replace('"',"'",$r['name'])."</button>";
					}
				}
			?>
		<!--</form>-->
		</div>
		<script>
		$(".chatbox").each(function(){this.scrollTop=this.scrollHeight;});
		</script>
		<?php
	}
	public static function quick_tickets($areas_id,$location){
		?>
		<a class='btn btn-lg btn-default' target="_blank" href='?c=Email&m=compose&areas_id=<?php echo $areas_id; ?>&location=<?php echo $location;?>'>Quick Tickets</a>
		<?php
	}
	public static function by_device_type($device_types,$categories){
		?>
		<div class='row'>
		<?php
		$n=1;
		foreach($device_types as $device){
			if(!com_Device::in_categories($device,$categories))continue;
			echo "<div class='col-xs-2'>";
			com_Device::button($device,array(
				"data-toggle"=>"modal",
				"data-target"=>"#report",
				"data-backdrop"=>"static",
				"onclick"=>'Buttons.generate_report("device-'.$device['id'].'"); return false;'
			));
			echo "</div>";
			if($n%6==0){
				echo "</div><div class='row'>";
			}
			$n++;
		}
		?> 
		</div>
		<script>
			var Buttons=new function(){
				this.categories = <?php echo json_encode($categories); ?>;
				this.validate_report=function(){
					var is_valid=true;
					if($("#create_empleado").val().trim()==""){
						is_valid=false;
						event.preventDefault();
						$("#create_empleado").focus();
					}
					$("#report .report-form").hide();
					$("#report .report-loading").slideDown();
					return is_valid;
				}
				this.generate_report=function (id){
					clearTimeout(autoRefresh);
					$("#report .media").hide();
					$("#report .modal-footer").hide();
					$("#report .modal-body").show();
					$("#report #problemas-"+id).show();
					setTimeout(function(){$("#report_reason").focus();},500);
				};
				this.cancel_report=function(){
					autoRefresh=setTimeout(function(){window.location.reload(true);},60000);
				}
				this.select_legacy=function(text,css){
					this.generate_report(0);
					this.select_report(0,text,css);
					$("#report").modal();
				}
				this.select_report=function(id,text,css){
					
					$("#report .modal-footer").slideDown();
					$("#report .modal-body").slideUp();	
					$("#reported_issue").val(text);
					
					$("#report_checklist").hide();
					$("#report_checklist div").html("");
					for(var c in this.categories){
						if(this.categories[c].name==text){
							$("#report_checklist div").html(this.categories[c].checklist);
							if($("#report_checklist div").text().trim()){
								$("#report_checklist").show();
							}
							break;
						}
					}
					
					$("#reported_issue").removeClass().addClass("btn").addClass("btn-"+css);
				};
				this.generate_solution =function(id){
					$("#close .media").hide();
					$("#close #cierre-"+id).show();
				}
			}
		</script>
		<?php
	}
	public static function report_dialog($areas_id,$location,$device_types,$categories){
			global $user;
		?>
			<!-- Modal -->
			<div id="report" class="modal fade" role="dialog">
			  <div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" onclick='Buttons.cancel_report();'>&times;</button>
					<h4 class="modal-title">Reportar Problema</h4>
				  </div>
				  <div class='report-form'>
					  <div class="modal-body">
						<?php com_Device::issues($device_types,$categories); ?>					
					  </div>
					  <div class="modal-footer">
						<form action='?c=alerts&m=create' method="POST" class='text-left' onsubmit='return Buttons.validate_report();'>
							<input type='hidden' name='areas_id' value='<?php echo $areas_id; ?>' />
							<input type='hidden' name='location' value='<?php echo $location; ?>' />
							<input type='text' name='action' id='reported_issue' readonly='readonly' onclick='return false;'>
							<br />
							<br />
							<div class='well' id='report_checklist'>							
								<label>Checklist</label>
								<div></div>
							</div>
							<fieldset class='form-group'>
								<label for='report_reason'>Mensaje:</label> 
								<input autocomplete='nope' type='text' class='form-control' id='report_reason' name='reason' />
							</fieldset>
							<fieldset class='form-group'>
								<label for='create_reason'>Empleado:</label> 
								<input type='number' class='form-control' id='create_empleado' name='employees_id' value='<?php echo $user->attr("employees_id");?>' />
							</fieldset>
							<fieldset class='text-right'>
								<input type="submit" class="btn btn-primary" value='Enviar' />
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</fieldset>
						</form>
					  </div>
				  </div>
				  <div class='report-loading' style='display:none;'>
					<div class="progress">
					  <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar"
					  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
						Enviando...
					  </div>
					</div>
				  </div>
				  
				</div>

			  </div>
			</div>
		<?php
	}
	
}