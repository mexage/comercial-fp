<?php 
class com_Dialog{
	static $count=0;
	static function shell($id,$title,$options=array()){
		$html="";
		
		$html.='<!-- com_Dialog::shell -->
			<div id="'.$id.'" class="modal fade" role="dialog">
			  <div class="modal-dialog  modal-lg">

				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">'.$title.'</h4>
				  </div>
				  <div class="modal-body">
					
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				</div>

			  </div>
			</div>
		';
		
		return $html;
	}
	static function ajax_load_links($css_selector,$dialog_id,$events=array()){
		
		$html='';
		$html.='<script> // com_Dialog::ajax_load_links
			$(document).ready(function(){
			  $("'.$css_selector.'").click(function(){
				  var url=$(this).attr("href")+"&ajax=noheader";
				  var loading="Loading...";
				  event.preventDefault();
				  '.$events['onClick'].'
				  
				  $("#'.$dialog_id.' .modal-body").html(loading);
				  $("#'.$dialog_id.'").modal("show");
					$.ajax({
						"url":url,
						"success":function(data){
							// onBeforeLoad
							'.$events['onBeforeLoad'].'
							
							$("#'.$dialog_id.' .modal-body").html(data);
							// onAfterLoad
							'.$events['onAfterLoad'].'
						}
					});
				
			  });
			  $("#'.$dialog_id.'").on("show.bs.modal", function(){
				'.$events['onShow'].'
			  });
			  $("#'.$dialog_id.'").on("hide.bs.modal", function(){
				'.$events['onHide'].'
			  });
			});
		</script>';
		return $html;
	}
	static function ajax_load_button($dialog_id,$text,$button_class,$url,$events=array()){
		$button_id=$dialog_id."_button_".self::$count++;
		$html='<a id="'.$button_id.'" href="'.$url.'" class="btn btn-'.$button_class.'">'.$text.'</a>';
		$html.='<script> // com_Dialog::ajax_load_button
			$(document).ready(function(){
			  $("#'.$button_id.'").click(function(){
				  var url=$("#'.$button_id.'").attr("href")+"&ajax=noheader";
				  var loading="Loading...";
				  event.preventDefault();
				  '.$events['onClick'].'
				  
				  $("#'.$dialog_id.' .modal-body").html(loading);
				  $("#'.$dialog_id.'").modal("show");
					$.ajax({
						"url":url,
						"success":function(data){
							// onBeforeLoad
							'.$events['onBeforeLoad'].'
							
							$("#'.$dialog_id.' .modal-body").html(data);
							// onAfterLoad
							'.$events['onAfterLoad'].'
						}
					});
				
			  });
			  $("#'.$dialog_id.'").on("show.bs.modal", function(){
				'.$events['onShow'].'
			  });
			  $("#'.$dialog_id.'").on("hide.bs.modal", function(){
				'.$events['onHide'].'
			  });
			});
		</script>';
		return $html;
	}
	static function to_ajax_form($events=array()){
		$code='';
		
		$code.="<script> // php_Dialog::to_ajax_form
		function to_ajax_form(dialog_id,saving_id){
		
		if(typeof saving_id==='undefined'){
			saving_id='loading';
		}
		
		var ajax_div =  $('#'+dialog_id+' .modal-body')[0];
		//if($.browser.msie && $.browser.version<='7.0'){
		//	return;
		//}
		$('#'+dialog_id+' modal-body a').each(
			function(index,value){
				var termina=value.href.substr(value.href.length-1);
				var has_target_blank = value.target=='_blank';
				var has_target_self = value.target=='_top';
				if(termina!='#' && !has_target_blank && !has_target_self){
					$(value).click(
						function(){
							event.preventDefault();
							//ajax_history.push(ajax_location_href);
							$('#'+saving_id).fadeIn('slow');
							
							$.post(this.href+'&ajax=noheader',null,
								function (data,status,http){
									ajax_div.innerHTML = data;
									var my_pos = $(ajax_div).position();
									var he = $(ajax_div).height() + my_pos.top;
									var ws = $(window).scrollTop();
									if(he<ws){
										$(window).scrollTop(my_pos.top);
									}
									var scr = $(ajax_div).find('script');
									scr.each(
										function(index,value){
											eval.call(window,value.innerHTML);
										}
									);
									var dialog_id=$(ajax_div).closest('.modal')[0].id;
									".$events['onAfterLoad']."
									
									$('#'+saving_id).fadeOut();
									 
								}					
							);


							return false;
						}
					);
				}
			});
		$('#'+dialog_id+' form').each(function(index,value){
			
			var ajax_form = $(value);
			if(ajax_form[0].target!='_top' && ajax_form[0].target!='_blank'){
				$(ajax_form).submit(
					function(){
						event.preventDefault();
						var my_data = $(this).serialize();
						var my_form = $(this)[0];
						if(this.mx_boton_seleccionado){
							my_data+='&'+escape(this.mx_boton_seleccionado.name)+'='+escape(this.mx_boton_seleccionado.value);
						}
						$('#'+saving_id).fadeIn('slow');
						//ajax_history.push(ajax_location_href);
						$.post($(my_form).attr('action')+'&ajax=noheader',my_data,
							function (data,status,http){
								ajax_div.innerHTML = data;
								var my_pos = $(ajax_div).position();
								var he = $(ajax_div).height() + my_pos.top;
								var ws = $(window).scrollTop();
								if(he<ws){
									$(window).scrollTop(my_pos.top);
								}
								$(ajax_div).find('script').each(
										function(index,value){
											eval.call(window,value.innerHTML);
										}
									);
								
								
								".$events['onAfterLoad']."
								
								$('#'+saving_id).fadeOut();
								
							}					
						);
						return false;
						
					}
				);
				var botones_submit = $(ajax_form).find(\"input[type='submit']\");
			
				botones_submit.click(
					function(){
						this.form.mx_boton_seleccionado = this;
					}
				);
			}
			
			


		
		});
		}</script>";
		return $code;
	}

}