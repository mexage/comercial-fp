<?php

class com_AndonCard{
	public static function card($alert){
		global $db;
		$device_types = $db->search("device_types","plants like '%".$alert['plant']."%'");
		echo "<script>S.add('"."a_".$alert['id']."','".$alert['start']."');</script>";
		
		?>
		<div class="panel panel-<?php echo $alert['status']['css_class']; ?>">
			<div class="panel-heading text-left">
			<h4 class='panel-title'>
				<?php if($alert['repeats']>1){echo '<span class="label label-danger">&times;'.$alert['repeats'].'</span>';} ?> #<?php echo $alert['id']; ?> <?php echo $alert['category_name']; ?>  <div style='float:right;margin-top:-8px;' class='btn btn-<?php echo $alert['status']['css_class']; ?>'><?php echo $alert['status']['name']; ?></div>
			</h4>
			</div>
				
			<div class='panel-body'>
				<div class='row'>
					<div class='col-lg-12'>					
				<?php 
					if($alert['reason']){
						echo "<p>".$alert['reason']."</p>";
					}
				?>
					</div>
				</div>
				<div class='row'>
					
						<table class='table'>
							<tr>
								<th>Equipo</th>
								<th>Mensajes</th>
								<th>Total</th>
								<th>Acciones</th>
							</tr>
							<?php
							foreach($alert['owners'] as $o){
								$ack="";
								if($o['received']){
									$ack="<small><span class='label label-default'><span title='Whatsapp: ".$o['received']."'>&#x2714;</span>";
									$ack.="</span></small> ";
								}
								if($o['acknowledged']){
									$ack="<small><span class='label label-info'><span title='WhatsApp: ".$o['received']."'>&#x2714;</span>";
									$ack.="<span title='Acknowledged: ".$o['acknowledged']."'>&#x2714;</span></span></small> ";
								}
								echo "<tr class='".$o['status']['css_class']."'>";
								$tipo="";
								if(strcasecmp($o['type']['name'],"En sitio")!=0){
									$tipo="<span class='badge'>".$o['type']['name']."</span>";
								}
								echo "<td><h3>".$o['owner']['name']."</h3>".$tipo."</td>";
								echo "<td><pre style='width:210px;max-height:100px;overflow-y:auto;' class='text-left chatbox'>".$ack.$o['message']."</pre><form action='?c=Alerts&m=responder&id=".$o['id']."&areas_id=".$alert['areas_id']."&location=".$alert['location']."' method='POST'><div class='input-group'><input class='form-control' type='text' name='message' /><div class='input-group-btn'><input class='btn btn-info' type='submit' value='Enviar'/></div></div></form></td>";
								
								
								echo "<td><span id='w_".$o['id']."'>".$o['start']."</span></td>";
								
								echo "<td>";
								?>
								<form action='?c=alerts&m=update' method='POST'>
									<div class="btn-group-vertical">
							
										<input type='hidden' value='<?php echo $alert['id']; ?>' name='alerts_id' />
										<input type='hidden' value='<?php echo $alert['areas_id']; ?>' name='areas_id' />
										<input type='hidden' value='<?php echo $alert['location']; ?>' name='location' />									
										<input type='hidden' value='<?php echo $o['id']; ?>' name='alerts_owners_id' />
										<input type='hidden' value='<?php echo $o['owners_id']; ?>' name='owners_id' />
										<input type='hidden' id='reason_<?php echo $o['id']; ?>' value='' name='reason' />
										<input type='hidden' id='employee_<?php echo $o['id']; ?>' value='' name='employees_id' />
										
										<?php if($o['status']['name']!="Descartado"){ ?>
											<?php if($o['status']['name']=="En Espera"){?>									
												<button data-toggle="modal"	data-target="#close_andon" data-backdrop="static" data-keyboard="false" class='btn btn-warning' onclick='Closure.show(<?php echo $alert['id']; ?>,<?php echo $o['id']; ?>,<?php echo $o['owners_id']; ?>,<?php echo $alert['failure__device_types_id']; ?>,"Revisando","warning"); return false;'>Revisando</button>
												
											<?php } ?>
											
											<?php if($o['status']['name']=="En Revision"){?>
												<!--<input class='btn btn-success' onclick='clearTimeout(autoRefresh);$("#reason_<?php echo $o['id']; ?>").val(prompt("Razon:"));' type='submit' value='Resuelto' name='action' />-->
												<button data-toggle="modal"	data-target="#close_andon" data-backdrop="static" data-keyboard="false" class='btn btn-success' onclick='Closure.show(<?php echo $alert['id']; ?>,<?php echo $o['id']; ?>,<?php echo $o['owners_id']; ?>,<?php echo $alert['failure__device_types_id']; ?>,"Resuelto","success"); return false;'>Resuelto</button>
											<?php } ?>
											
											<?php if(($o['status']['name']=="En Espera" && $alert['can_discard_pending']
													)||($o['status']['name']=="En Revision" && $alert['can_discard_active']) ){?>
												<!--<input class='btn btn-danger' type='submit' value='Descartar' name='action' />-->
												<button data-toggle="modal"	data-target="#close_andon" data-backdrop="static" data-keyboard="false" class='btn btn-danger' onclick='Closure.show(<?php echo $alert['id']; ?>,<?php echo $o['id']; ?>,<?php echo $o['owners_id']; ?>,<?php echo $alert['failure__device_types_id']; ?>,"Descartar","danger"); return false;'>Descartar</button>
											<?php } ?>
										<?php }else{
											echo "<a class='btn btn-default' href='#'>Descartado</a>";
										}
										echo  "<small id='o_".$o['id']."'>".$o['start']."</small>";
										
										?>
									</div>
								</form>
										
							<?php
								echo "<script>S.add('"."o_".$o['id']."','".$o['start']."','".$o['first']."');</script>";
								echo "<script>S.add('"."w_".$o['id']."','".$o['start']."','".$o['finish']."');</script>";
								
								
									echo "</td>";
								
								
								
								echo "</tr>";
								}
					
							?>
						</table>
					
				</div>
			</div>
			<div class='panel-footer'>
				
				
				<form action='?c=alerts&m=update' method='POST'>
					<input type='hidden' name='areas_id' value='<?php echo $alert['areas_id']; ?>' />
					<input type='hidden' name='location' value='<?php echo $alert['location']; ?>' />
					<input type='hidden' name='reason' value='' id='false_reason_<?php echo $o['id']; ?>'/>
					
	
					<input type='hidden' value='<?php echo $alert['id']; ?>' name='alerts_id' />
					<?php /*com_Device::dropdown(
						"andon_device_".$alert['id'],
						$device_types,
						$alert['failure_device_type']['id'],
						array("onchange"=>"Card.change_device(".$alert['id'].",this.value);")
					); */?>
					<?php 
						echo php_Field::dropdown(
							"device",
							$device_types,
							$alert['failure_device_type']['id'],
							0,
							"' onchange='Card.change_device(".$alert['id'].",this.value);' style='display:inline-block;width:25%",
							"device_".$alert['id']
						);
					?>
					<?php if($alert['failure_device_type']['id'] && isset($alert['failure_device_type']['image'])){ ?>
						
						<img style='width:50px;' src='<?php echo $alert['failure_device_type']['image'];?>' class='img-responsive img-thumbnail' /> 
					<?php } ?>
					<div class="btn-group">
						
						<button class='btn btn-primary' data-toggle="modal"
					data-target="#call_teams" data-backdrop="static" data-keyboard="false" onclick='Calls.start(<?php echo $alert['id']; ?>); return false;'>Llamar</button>
						<!--<input class='btn btn-default' onclick='clearTimeout(autoRefresh);$("#false_reason_<?php echo $o['id']; ?>").val( prompt("Razon:"));' type='submit' value='Falsa Alarma' name='action' />-->
						<button data-toggle="modal"	data-target="#close_andon" data-backdrop="static" data-keyboard="false" class='btn btn-default' onclick='Closure.show(<?php echo $alert['id']; ?>,<?php echo $o['id']; ?>,<?php echo $o['owners_id']; ?>,<?php echo $alert['failure__device_types_id']; ?>,"Falsa Alarma","default"); return false;'>Falsa Alarma</button>
					</div>
					<h4 class='visible-lg-inline-block visible-md-inline-block visible-sm-inline-block visible-xs-inline-block'>Tiempo Total:
							<span id='<?php echo "a_".$alert['id']; ?>' class='data'><?php echo $alert['created']; ?></span>
					</h4>
				</form>
				
			</div>
		</div>		
		<?php 
	}
	
	public static function close_dialog($areas_id,$location){
		global $db;
		$area=$db->get("areas",$areas_id);
		$device_types = $db->search("device_types","plants like '%".$area['plant']."%'");
		?>
		<div id='close_andon' class='modal fade'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'> 
					<button class='close'  data-toggle="modal" data-target="#close_andon" onclick='Closure.cancel();'>&times;</button><h4 class='modal-title'>Cerrar Andon</h4></div>
					<div class='modal-body'>
						<div class='close-form'>
						<form action="?c=alerts&m=update" method="POST" onsubmit='return Closure.validate();'>
							<div id="close_step_1">
								<input type='hidden' name='action' value='' />
								<input type='hidden' value='<?php echo $areas_id; ?>' name='areas_id' />
								<input type='hidden' value='<?php echo $location; ?>' name='location' />									
								<input type='hidden' value='' name='alerts_id' />
								<input type='hidden' value='' name='alerts_owners_id' />
								<input type='hidden' value='' name='owners_id' />
								
								
								<div class='btn btn-success closure-action'></div>
								
								<br />
								<br />
								<label>Empleado:</label>
								<input class='form-control' type='number' value='' name='employees_id' />
								<br />
								<div class='real_close'>									
									<label>Dispositivo/categoria con falla:</label>
									<?php
										echo php_Field::dropdown(
											"device_types_id",
											$device_types,
											"",0,"' onchange='Closure.select_device(this.value);"
										); ?>	
									<br />
									<label class='close-solution'>Solucion:</label>
									<div class="close-solution-list">
										
									</div>
									<input autocomplete="nope" class='form-control' type='text' name='reason' value='' id="close_message" />
									<br/>
								</div>
								<input class='btn-lg btn-primary' type='submit' value='Send'>
							</div>							
						</form>
					</div>							
					<div class='close-loading' style='display:none;'>
					<div class="progress">
					  <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar"
					  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
						Enviando...
					  </div>
					</div>
				  </div>
					</div>
				</div>
			</div>
		</div>
		<script>
			
			var Closure = new function(){
				this.solutions={};
				this.validate=function(){
					var action=$("#close_andon .closure-action").text();
					
					if(action=='Revisando' || action=='Descartar' || action=='Falsa Alarma' || action=='Resuelto'){
						if($("#close_andon input[name='employees_id']").val().trim()==""){
							event.preventDefault();
							$("#close_andon input[name='employees_id']").focus();							
							return false;
						}
					}
					
					if(action=='Descartar' || action=='Falsa Alarma' || action=='Resuelto'){
						if($("#close_andon input[name='reason']").val().trim()==""){
							event.preventDefault();
							$("#close_andon input[name='reason']").focus();							
							return false;
						}
					}
					
					$("#close_andon .close-form").hide();
					$("#close_andon .close-loading").slideDown();
					
					return true;
				};
				this.cancel=function(){
					autoRefresh=setTimeout(function(){window.location.reload(true);},60000);
				};
				this.select_device=function(id,val){
					$("#close_andon .close-solution-list").slideDown();
					$("#close_andon .close-solution-list").html("loading...");
					if(typeof val=='undefined'){
						$("#close_message").val("");
					}else{
						$("#close_message").val(val);
					}
					$.ajax({
						url:"?c=Solutions&m=propose&device_types_id="+id+"&_data=json",
						success:function(data){
							try{								
								Closure.solutions=JSON.parse(data);
								var html="";
								var cnt=0;
								var stripe="";
								for(var s in Closure.solutions){
									if(typeof Closure.solutions[s].name=="undefined")continue;
									cnt++;
									if(cnt%2){
										stripe=" list-group-item-info";
									}
									//html+="<div class='radio'><label><input type='radio' name='solution' value='"+s+"'>"+Closure.solutions[s].name+"</label></div>";
									//html+="<li class='list-group-item"+stripe+"' data-value='"+s+"'>"+Closure.solutions[s].name+"</li>";
									html+="<button class='btn btn-info' data-value='"+s+"'>"+Closure.solutions[s].name+"</button> ";
								}
								
								$("#close_andon .close-solution-list").html(html);
								$("#close_andon .close-solution-list button").click(function(event,ui){
									event.preventDefault();																		
									$("#close_andon select[name='device_types_id']").val(Closure.solutions[this.dataset.value].real__device_types_id);
									Closure.select_device(Closure.solutions[this.dataset.value].real__device_types_id,Closure.solutions[this.dataset.value].name);
								});
								if(!cnt){
									$("#close_andon .close-solution-list").slideUp();
								}
								
							}catch(e){
								alert(e);
								window.location.reload(true);
							}
						}
					});
					
				}
				
				this.show=function(alerts_id,alerts_owners_id,owners_id,device_types_id,action,css_class){
					clearTimeout(autoRefresh);
					Closure.select_device(device_types_id);
					$("#close_andon input[name='alerts_id']").val(alerts_id);					
					$("#close_andon input[name='action']").val(action);					
					$("#close_andon input[name='alerts_owners_id']").val(alerts_owners_id);					
					$("#close_andon input[name='owners_id']").val(owners_id);
					$("#close_andon select[name='device_types_id']").val(device_types_id);
					
					//$("#close_andon input[name='owners_id']").val("");
					$("#close_andon input[name='employees_id']").val("");
					
					$("#close_andon .closure-action").text(action).removeClass().addClass("closure-action btn btn-"+css_class);
					
					$("#close_andon .real_close").show();						
					
					if(action=='Revisando'){
						$("#close_andon .real_close").hide();
						$("#close_andon h4").text("Primera respuesta");
						
					}else if(action=='Descartar'){
						$("#close_andon h4").text("Descartar Andon");
						$("#close_andon label.close-solution").text("Razon:");
					}else if(action=='Resuelto'){
						$("#close_andon h4").text("Cerrar Andon");
						$("#close_andon label.close-solution").text("Solucion:");
					}else if(action='Falsa Alarma'){
						$("#close_andon label.close-solution").text("Razon:");
						$("#close_andon select[name='device_types_id']").val(0);
					}
					setTimeout(function(){
						$("#close_andon input[name='employees_id']").focus();
					},500);
					
					
				};
				
			}();
		</script>

		<?php
	}
	
	public static function call_dialog($areas_id,$location,$owners){
		?>
		<div id='call_teams' class='modal fade'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'> 
					<button class='close'  data-toggle="modal" data-target="#call_teams" onclick='Calls.cancel_call();'>&times;</button><h4 class='modal-title'>Call Team</h4></div>
					<div class='modal-body'>
						<div class='call-form'>
						<form action="?c=alerts&m=update" method="POST" onsubmit='return Calls.validate_dialog();'>
							<div id="call_step_1">
								<input type='hidden' name='action' value='Llamar' />
								<input type='hidden' name='areas_id' value='<?php echo $areas_id; ?>' />
								<input type='hidden' name='location' value='<?php echo $location; ?>' />
								<input type='hidden' id='call_teams_alerts_id' name='alerts_id' value='' />
								<label>Equipo</label>
								<!--<select class='form-control' id="call_team_id" name='owner'>
								<option value=''>Seleccione un equipo de soporte</option>-->
								<?php
									foreach($owners as $o){
										//echo "<input class='btn-lg btn-info' onclick='clearTimeout(autoRefresh);' type='submit' name='owner' value='".$o['name']."' />";
										echo "<a class='btn btn-lg btn-info' href='#' onclick='Calls.go_step(2);Calls.set_owner(\"".$o['name']."\"); return false;'>".$o['name']."</a>";
										//echo "<option value='".$o['name']."'>".$o['name']."</option>";
									}
								?>
								<!--</select>
								<input class='btn-lg btn-primary' type='submit' value='Llamar'>-->
							</div>
							<div id="call_step_2" style='display:none;'>
								<input class='btn btn-info' type='text' name='owner' id='call_owners_id' onclick='Calls.go_step(1); return false;' />
								<br />
								<label>Mensaje:</label><input autocomplete="nope" class='form-control' type='text' name='message' value='' id="call_message" /><br />
								<input class='btn-lg btn-primary' type='submit' value='Llamar'>
							</div>							
						</form>
						</div>
						<div class='call-loading' style='display:none;'>
					<div class="progress">
					  <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar"
					  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
						Enviando...
					  </div>
					</div>
				  </div>
					</div>
				</div>
			</div>
		</div>
		<script>
			var Calls=new function(){
				this.validate_dialog=function(){
					var is_valid=true;
					if($("#call_message").val().trim()==""){
						$("#call_message").focus();
						event.preventDefault();
					}
					
					if(is_valid){
						$("#call_teams .call-form").hide();
						$("#call_teams .call-loading").slideDown();
					}
					return is_valid;
				};
				this.start=function(id){
					clearTimeout(autoRefresh);
					$("#call_teams_alerts_id").val(id);
					
				}
				this.cancel_call=function(){
					this.go_step(1);
					autoRefresh=setTimeout(function(){window.location.reload(true);},60000);
				};
				this.set_owner=function(owner){
					$("#call_owners_id").val(owner);
				};
				this.go_step=function(step){
					switch(step){
						case(1):{
							$("#call_step_1").slideDown();
							$("#call_step_2").slideUp();							
						}break;
						case 2:{
							$("#call_step_1").slideUp();							
							$("#call_step_2").slideDown();
							setTimeout(function(){
								$("#call_message").focus();
							},500);
						}break;
					}
				}
			}();
			var Card=new function(){
				this.change_device=function(andon_id,device_type){
					$.ajax({
						"url":"?c=Alerts&m=change_device&id="+andon_id+"&device_types_id="+device_type+"&_data=json",
						"success":function(data){
							try{
								var resp=JSON.parse(data);
								if(resp[0]!="OK"){
									alert(resp[0]);
								}
							
								
							}catch(e){
								alert("Update failed: "+e);
							}
							window.location.reload(true);
						},
						"error":function(){
							alert("There was a problem with the request.");
							window.location.reload(true);
						}
					});
				};
			}();
		</script>
		<?php
	}
}