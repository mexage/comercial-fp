<?php
class com_Marker{
	static $x=0;
	static function icon_img($args){
		// Needs to be inside a div which is position:relative
		$args=array_merge(
			array(
				"x"=>0,
				"y"=>0,
				"background"=>"blue",
				"color"=>"white",
				"radius"=>"5px",
				"text"=>"&nbsp;",
				"id"=>"icon__".time().rand(),
				"image"=>"empty",
				"onclick"=>"",
				"onmouseup"=>""
				
			)
		,$args);
		return "<img onclick='".$args['onclick']."' onmouseup='".$args['onmouseup']."'src='images/markers/".$args['image'].".png' 
				class='".$args['class']."' style='"//background:". $args['background'] 
				.";overflow-y:hidden"
				.";overflow-x:hidden"
				
				//.";height:20px"
				//.";width:130px"
				//.";color:".$args['color']
				//.";border-radius:".$args['radius']
				.";position:absolute"
				.";left:".$args['x']."px"
				.";top:".$args['y']."px"
				//.";transform:rotate(-90deg)"// translateX(50%) translateY(-250%)"
				.";transform-origin: center left"
				.";padding:3px 5px;'"
				." id='".$args['id']."'"
				." />";
	}
	static function icon($args){
		self::$x+=15;
		// Needs to be inside a div which is position:relative
		$args=array_merge(
			array(
				"x"=>self::$x,
				"y"=>0,
				"background"=>"blue",
				"color"=>"white",
				"radius"=>"5px",
				"text"=>"&nbsp;",
				"id"=>"icon__".time().rand(),
				"image"=>"empty",
				"onclick"=>"",
				"onmouseup"=>"",
				"selected"=>false
				
			)
		,$args);//position: absolute;#929292
		if(strcasecmp($args['image'],'good')==0){
			$args['background']="#009900";
			$args['icon_text']="&#10004;";	
		}else if(strcasecmp($args['image'],'warning')==0){
			$args['background']="#ff9900";
			$args['icon_text']="!";
		}else if(strcasecmp($args['image'],'bad')==0){
			$args['background']="#ff0000";
			$args['icon_text']="&times;";
		}else if(strcasecmp($args['image'],'empty')==0){
			$args['background']="#0066ff";
			//$args['icon_text']="&times;";	
		}
		$border= ';border: 1px solid #00000070';
		if($args['disabled']){
			$args['background'].="99";	
			//$border= ';border: 1px dashed #ff0000';
		}
		//selected 0 0 9px 5px #00ffe7;
   
		return "<div onclick='".$args['onclick']."' onmouseup='".$args['onmouseup']."'src='images/markers/".$args['image'].".png' 
				class='".$args['class']."' style='"
				."background:". $args['background'] 
				.";position:absolute"
				.";left:".($args['x']-12)."px"
				.";top:".($args['y']-12)."px"
				//.";transform:rotate(-90deg)"// translateX(50%) translateY(-250%)"
				.";transform-origin: center left"
				.";height: 24px"
				.";width: 24px"
				.';border-radius: 15px'
				.";box-shadow: 2px 2px 2px #0000007a, inset -1px -2px #0000005c, inset 0px 2px 4px #ffffffa8"
				.(($args['selected'])?",0 0 9px 5px #00ffe7":"")
				.";color: white"
				.";font-weight: 900"
				.";text-shadow: 1px 1px 2px black;"
				.";font-size:24px"
				.";line-height:22px"
				.$border
				."'	id='".$args['id']."'"
				.">".$args['icon_text']."</div>";
	}
	static function vertical($args){
		// Needs to be inside a div which is position:relative
		$args=array_merge(
			array(
				"x"=>0,
				"y"=>0,
				"background"=>"blue",
				"color"=>"white",
				"radius"=>"5px",
				"text"=>"&nbsp;",
				"id"=>"marker__".time().rand(),
				"display"=>"block"
			)
		,$args);
		return "
			<div class='".$args['class']."' style='background:". $args['background'] 
				.";overflow-y:hidden"
				.";overflow-x:hidden"
				.";z-index:2"
				//.";height:20px"
				//.";width:130px"
				.";color:".$args['color']
				.";border-radius:".$args['radius']
				.";position:absolute"
				.";display:".$args['display']
				.";left:".$args['x']."px"
				.";top:".$args['y']."px"
				//.";transform:rotate(-90deg)"// translateX(50%) translateY(-250%)"
				.";transform-origin: center left"
				.";padding:3px 5px 5px 5px;'"
				." id='".$args['id']."'"
				.">"
				
				//."<div style='width:0;height:0;border-left: 5px solid transparent;border-right: 5px solid transparent;border-bottom: 5px solid ".$args['color'].";'></div>"
				
				."".$args['text'].""
				."<script>
					//setInterval(function(){
					//	$($('#".$args['id']." .label:visible')[0]).slideUp();
					//	if(!$('#".$args['id']." .label:visible').length){
					//		$('#".$args['id']." .label').slideDown();
					//	}
						
					//},2000);
				</script>"
				."</div>";
	}
	static function location($args,$location_row){
		global $db;
		$id=time().rand();
		$alerts = $db->search('alerts',"location='".$location_row['name']."' AND areas_id='".$location_row['areas_id']."' AND status_id in(1,2)");
		$db->fill($alerts);
		$own="";
		$total_color="empty";
		$owner_ids=array();
		if(count($alerts)){
			foreach($alerts as $a){
				$owners = $db->search('alerts_owners',"alerts_id=".$a['id']);
				if(strcasecmp($a['status']['css_class'],"danger")==0){
					$total_color='bad';
				}else if(
					  strcasecmp($a['status']['css_class'],"warning")==0
					&&strcasecmp($total_color,"bad")!=0
				){
					$total_color='warning';
				}
				
				
				$db->fill($owners);
				$own_h="<div class='label label-".$a['status']['css_class']."'>".$a['id'].": ".$a['categories']['name']." "."<span class='' id='a_".$a['id']."'></span><script>S.add('"."a_".$a['id']."','".$a['start']."','".$a['finish']."');</script>"."</div><br />";
				if(count($owners)){
					foreach($owners as $o){
						$owner_ids[$o['owners_id']]="own_".$o['owners_id'];
						$own_i.="<div class='label label-".$o['status']['css_class']."'>".$o['owners']['name']." "."<span class='' id='t_".$o['id']."'></span><script>S.add('"."t_".$o['id']."','".$o['start']."','".$o['first']."');</script>"."</div>";
					}
				}
				
				$own.=$own_h.$own_i;
			}
		}
		$this_moment = date_create();
		$last_seen = date_create($location_row['last_seen']);
		
		$interval = date_diff($last_seen,$this_moment);
		
		$minutes = $interval->format("%i")+($interval->format("%a")*24*60);
		$disabled=$minutes>10;
		$station_off="";
		if($disabled){
			$station_off=" Off: ".$minutes."min(s)";
		}
		return com_Marker::vertical(
				array(
					'text'=>"<div class='label label-primary'>".$location_row['areas']['name']."</div><br /><div class='label label-info'>".$location_row['name'].$station_off."</div><br />".$own,
					'display'=>'none',
					'x'=>$location_row['location_x']+15,
					'y'=>$location_row['location_y']-12,
					'background'=>$args['background'],
					'id'=>"marker__".$id,
					'class'=>'marker'
				)
			)
			.com_Marker::icon(array_merge($args,array(
				'onclick'=>'$(".marker").not($("#marker__'.$id.'")).slideUp();$("#marker__'.$id.'").slideToggle()',
				'x'=>$location_row['location_x'],
				'y'=>$location_row['location_y'],
				'image'=>$total_color,
				'class'=>'marker_icon marker_'.str_replace(array(" ","-"),"",$location_row['areas']['name']).' '.implode(" ",$owner_ids),
				'disabled'=>$disabled
				
			)));
	}
}