function Stopwatch(now){
	var me=this;
	this.difference=0;
	
	this.watches={};
	
	this.add=function(id,start,end){
		var o={"start":me.mysql_date(start)};
		if(end){
			o.end=me.mysql_date(end);
		}else{
			o.end=false;
		}
		me.watches[id]=o;
	};
	this.start=function(){
		me.update();
		setInterval(me.update,1000);
	};
	this.mysql_date=function(t){
		// Split timestamp into [ Y, M, D, h, m, s ]
		if(t=="0000-00-00 00:00:00")return false;
		t=t.split(/[- :]/);
		
		// Apply each element to the Date function
		return new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
		
	};
	this.format=function(duration){
		duration-=me.difference;
		var milliseconds = parseInt((duration%1000)/100)
            , seconds = parseInt((duration/1000)%60)
            , minutes = parseInt((duration/(1000*60))%60)
            , hours = parseInt((duration/(1000*60*60))%24)
			, days = parseInt((duration/(1000*60*60*24)));

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;
		var result = hours + ":" + minutes + ":" + seconds;
		if(days>0){
			result = days+ " dia(s) "+result;
		}
        return result ;
		
	};
	this.update=function(){
		for(var w in me.watches){
			var t;
			if(me.watches[w].end){
				t=me.watches[w].end.getTime()-me.watches[w].start.getTime();
			}else{
				t=(new Date()).getTime()-me.watches[w].start.getTime();
			}
			$("#"+w).html(me.format(t));
		}
	};
	this.difference=(new Date()).getTime()-this.mysql_date(now).getTime();
	if(Math.abs(this.difference)>1000){
		//alert("Time difference with server: "+this.difference);
	}else{
		//alert("Time OK ("+this.difference+")");
	}
}