<script src="lib/js/jquery/jquery-3.2.1.js"></script>
<script type="text/javascript" src="lib/js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript" src="lib/js/jquery/jquery.ui.touch-punch.min.js"></script>

<!--
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 --> 
<link rel="stylesheet" type="text/css" href="lib/js/jquery/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="lib/js/jquery/theme.css">



<link rel="stylesheet" href="lib/js/bootsrap/css/bootstrap.min.css">
<script src="lib/js/bootsrap/js/bootstrap.min.js"></script>
<!--  -->
<script src="stopwatch.js"></script>

<script type="text/javascript" src="lib/js/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="lib/js/bootstrap-multiselect/css/bootstrap-multiselect.css" type="text/css" />


<link rel="stylesheet" type="text/css" href="lib/js/pivottable/dist/pivot.css" />
<link rel="stylesheet" type="text/css" href="lib/js/webpivot_jsonly_free/Scripts/pivottable/nrecopivottableext.css" />
<link rel="stylesheet" type="text/css" href="lib/js/subtotal/dist/subtotal.css" />
<link rel="stylesheet" type="text/css" href="lib/js/DataTables/datatables.min.css"/>

<script type="text/javascript" src="lib/js/pivottable/dist/pivot.js"></script>
<script type="text/javascript" src="lib/js/pivottable/dist/custom_renderers.js"></script>
<script type="text/javascript" src="lib/js/pivottable/dist/export_renderers.js"></script>
<script type="text/javascript" src="lib/js/subtotal/dist/subtotal.js"></script>
<script type="text/javascript" src="lib/js/webpivot_jsonly_free/Scripts/pivottable/nrecopivottableext.js"></script>
<link rel="stylesheet" type="text/css" href="lib/js/c3/c3.min.css">
<script type="text/javascript" src="lib/js/d3/d3-3.5.6.min.js"></script>
<script type="text/javascript" src="lib/js/c3/c3.min.js"></script>
<script type="text/javascript" src="lib/js/pivottable/dist/c3_renderers.js"></script>
<script type="text/javascript" src="lib/js/pivottable/dist/d3_renderers.js"></script>

<script type="text/javascript" src="lib/js/js/barcode.js"></script>
<script type="text/javascript" src="lib/js/js/cookie.js"></script>
<script type="text/javascript" src="lib/js/js/MXFunctions.js"></script>
<script type="text/javascript" src="lib/js/jquery-tabledit/jquery.tabledit.min.js"></script>
<script type="text/javascript" src="lib/js/DataTables/dataTables.js"></script>
<script type="text/javascript" src="lib/js/nicedit/nicEdit.js"></script>


<script src="lib/js/jexcel/dist/jexcel.js"></script>
<link rel="stylesheet" href="lib/js/jexcel/dist/jexcel.css" type="text/css" />

<script src="lib/js/jsuites/dist/jsuites.js"></script>
<link rel="stylesheet" href="lib/js/jsuites/dist/jsuites.css" type="text/css" />



<link rel="stylesheet" type="text/css" href="lib/js/clockpicker/dist/jquery-clockpicker.min.css" />
<script src="lib/js/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="lib/js/js/localStorage.js"></script>
<script src="lib/js/js/qr.js"></script>

<script src="lib/js/vue/vue.js"></script>

<link rel="stylesheet" type="text/css" href="lib/js/jquery/jquery-combobox.css">
<script type="text/javascript" src="lib/js/jquery/jquery.combobox.js"></script>