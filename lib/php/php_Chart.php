<?php
class php_Chart{
	public static function div($id){
		$r="<div id='".$id."'>Loading...<br /><img src='images/bins/loader.gif' /></div>";
		return $r;
	}
	public static function ajax($url,$code){
		$r='<!-- php_Chart::ajax('.$url.') -->
		<script>
		$.ajax({
			url:"'.$url.'",
			dataType:"text",
			success:function(d){
				try{
					var ajax_data=JSON.parse(d);
				}catch(e){
					prompt(e.toString(),d);
				}
				'.$code.'
			}	
			
		});
		</script>';
		
		return $r;
	}
	public static function javascript($id,$cnf,$ui=false,$var="ajax_data"){
		$r='
			/**************************************************************
				php_Chart::javascript('.$id.'); UI: '.$ui.' VAR:'.$var.'
			***************************************************************/
			var temp_chart_function=function(ajax){
				var data;
				var cnf;
				try{
					data='.$var.';
					cnf='.$cnf.';
					var nrecoPivotExt = new NRecoPivotTableExtensions({
						drillDownHandler: function (dataFilter) { 
							console.log(dataFilter); 
						} });
					$.extend($.pivotUtilities.renderers,$.pivotUtilities.subtotal_renderers,
						$.pivotUtilities.c3_renderers,$.pivotUtilities.d3_renderers,$.pivotUtilities.custom_renderers,$.pivotUtilities.export_renderers);
					cnf.dataClass=$.pivotUtilities.SubtotalPivotData;
					cnf.aggregator = $.pivotUtilities.aggregators[cnf.aggregatorName](cnf.vals);
					';
		
			if($ui){
				$r.= '$("#'.$id.'").pivotUI(data,cnf,true);'."\n";
				$r.= 'var margin = $($("#'.$id.' td.pvtUnused")[0]).width()+$($("#'.$id.' td.pvtRows")[0]).width();';
			}else{
				$r.= 'cnf.renderer=nrecoPivotExt.wrapTableRenderer($.pivotUtilities.renderers[cnf.rendererName]),$.pivotUtilities.renderers[cnf.rendererName];'."\n";
				$r.= 'cnf.filter=function(record){
						var filterItemExcluded = false;
		
						for(attr in record){
						  var value = record[attr];
						  if(cnf.exclusions){
							  if (cnf.exclusions[attr]) {
								filterItemExcluded = (cnf.exclusions[attr].indexOf(value)>= 0);
							  }
						  }
						  if(cnf.inclusions){
							  if (cnf.inclusions[attr]) {
								filterItemExcluded = (cnf.inclusions[attr].indexOf(value)==-1);
							  } 
						  }
						  if(filterItemExcluded)return false;
						}
						return !filterItemExcluded;
					};
					';
				$r.= '$("#'.$id.'").pivot(data,cnf,true);'."\n";
				$r.= 'var margin=0;';
			}
		$r.='
					
					if($("#'.$id.'").text()=="An error occurred rendering the PivotTable UI."){
						$("#'.$id.'").addClass("alert");
						$("#'.$id.'").addClass("alert-danger");
					}else{
						setInterval(function(){
							if($("#'.$id.' div.c3").data("chart")){
								var new_width=($("#'.$id.'").width()-margin)*.9;
								if(new_width!=$("#'.$id.' div.c3").data("chart").width){
									$("#'.$id.' div.c3").data("chart").resize({width:new_width});
									$("#'.$id.' div.c3").data("chart").width=new_width;
								}
							}'.$var.'
						},1000);
						
					}
					
				}catch(e){
					$("#'.$id.'").addClass("alert");
					$("#'.$id.'").addClass("alert-danger");
					
					$("#'.$id.'").html("<h3>Data source error: " + e.toString()+"</h3><h3>Dump:</h3><pre>"+d+"</pre>");
				}
			}('.$var.');';
		return $r;
	}
	static function copy_button($id,$text="Copy Table",$class='info',$icon='copy'){
		if($icon){
			$icon='<span class="glyphicon glyphicon-'.$icon.'"></span> ';
		}
		return '
			<button class="btn btn-'.$class.'" onclick="selectAndCopy(\''.$id.'\');">'.$icon.$text.'</button>
			<script>
		function selectElementContents(el) {
			var body = document.body, range, sel;
			if (document.createRange && window.getSelection) {
				range = document.createRange();
				sel = window.getSelection();
				sel.removeAllRanges();
				try {
					range.selectNodeContents(el);
					sel.addRange(range);
				} catch (e) {
					range.selectNode(el);
					sel.addRange(range);
				}
			} else if (body.createTextRange) {
				range = body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		}
		
		function selectAndCopy(id) {
			var element=null;
			try{
				element=$("#"+id+" table.pvtTable")[0];
			}catch(e){
				
			}
			if(element===null)element=$(""+id+"")[0];
			selectElementContents(element);
			document.execCommand("copy");
			alert("Table selected!");
			
		}
		</script>';
	}
	static function drilldown_form($id,$c="",$m="report",$dm="json",$q=""){
		$html="
		<form target='_blank' id='".$id."' style='display:none;' method='POST' action='?c=".$c."&m=".$m."&dm=".$dm."'>
			<input type='hidden' name='c' value='".$c."' />
			<input type='hidden' name='m' value='".$m."' />
			<input type='hidden' name='dm' value='".$dm."' />
			<textarea name='query'></textarea>
			</form>";
		return $html;
	}
	static function drilldown_javascript($id,$rows=array(),$cols=array(),$renderer="Table",$aggregator="Count",$vals=array()){
		$rows_code="";
		$cols_code="";
		$vals_code="";
		
		if(count($rows_code))$rows_code='"'.implode('","',$rows).'"';
		if(count($cols_code))$cols_code='"'.implode('","',$cols).'"';
		if(count($vals_code))$vals_code='"'.implode('","',$vals).'"';
		
		
		$html='var query ={"cols":['.$rows_code.'],"rows":['.$rows_code.'],"vals":['.$vals_code.'],"inclusions":{},"exclusions":{},"rendererName":"'.$renderer.'","aggregatorName":"'.$aggregator.'","rendererOptions":{"localeStrings":{"renderError":"An error occurred rendering the PivotTable results.","computeError":"An error occurred computing the PivotTable results.","uiRenderError":"An error occurred rendering the PivotTable UI.","selectAll":"Select All","selectNone":"Select None","tooMany":"(too many to list)","filterResults":"Filter values","apply":"Apply","cancel":"Cancel","totals":"Totals","vs":"vs","by":"by"}}}
							
		for(var f in filters){
			query.inclusions[f]=[filters[f]];
			query.rows.push(f);
		}
		// Send form generated by php_Chart::drilldown_form();
		$("#'.$id.' textarea[name=query]").val(JSON.stringify(query));
		$("#'.$id.'").submit();';
		return $html;
	}
}
