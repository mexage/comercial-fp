<?php

class php_Grid{
	static function replace($string,$data){
		foreach($data as $k=>$v){
			$string=str_replace("<?=".$k."?>",$v,$string);
		}
		return $string;
	}
	static function draw($data,$rowlinks=array(),$hide_fields=array()){
		global $db;
		if(!$data || !is_array($data[0])){
			echo "<table class='table table-bordered table-striped table-condensed table-hover table-responsive'><tbody><tr><td>No data.</td></tr></tbody></table>";
			return;
		}
		if(!count($hide_fields)){
			$hide_fields=array("this is a value that should never happen on a field");
		}
		echo "<table class='table table-striped table-bordered'>";
		$keys=array_keys($data[0]);
		echo "<thead><tr>";
		
		foreach($keys as $k){
			
			if(array_search($k,$hide_fields)===false){
				echo "<th>".ucwords(str_replace("_"," ",$k))."</th>";
			}
		}
		
		if(count($rowlinks)){
			echo "<th>Actions</th>";
		}
		echo "</tr></thead><tbody>";
		$lookup_hash=array();
		foreach($keys as $k){
			if(array_search($k,$hide_fields)===false){
				if($table=$db->ftable($k)){
					if(intval($db->query("SELECT COUNT(*) c FROM ".$table.";")[0]['c'])<500){
						trace("Preloading hash for table ".$table);
						$lookup_hash[$table]=$db->hash($table);
					}else if(intval($db->query("SELECT COUNT(*) c FROM ".$table.";")[0]['c'])<20000){
						trace("Prepare table for dynamic loading ".$table);
						$lookup_hash[$table]=array();
					}			
				}
			}
		}
		foreach($data as $r){
			echo "<tr>";
			
			foreach($keys as $k){
				if(array_search($k,$hide_fields)===false){
					$d=$r[$k];
					if($table=$db->ftable($k)){
						if(isset($lookup_hash[$table])){
							if(!isset($lookup_hash[$table][$d])){
								$lookup_hash[$table][$d]=$db->get($table,$d);
							}
						}else{
							$d=array("id"=>$d,"name"=>"");
						}
						$d=$lookup_hash[$table][$d];
						if($d['name'] && $d['id']){
							echo "<td><a class='btn btn-info' href='?c=".$table."&m=view&id=".$d['id']."'> ".$d['name']." (".$r[$k].")</a></td>";
						}else if($d['id']){
							echo "<td><a class='btn btn-info' href='?c=".$table."&m=view&id=".$d['id']."'>".$r[$k]."</a></td>";
						}else if($d['name']){
							echo "<td><a class='btn btn-info' href='?c=".$table."&m=view&id=".$d['id']."'>".$d['name']."</a></td>";
						}else{
							echo "<td><a class='btn btn-info' href='#' > - </a></td>";
						}
					}else{
						// Todo: review idea, might not make sense, unless we add "dynamic" columns
						//echo "<td>".php_Grid::replace($d,$r)."</td>";
						echo "<td>".$d."</td>";
					}
				}
			}
		
			if(count($rowlinks)){
				echo '<td><div class="btn-group">';
				foreach($rowlinks as $t=>$u){
					$parts=explode(":",$t);
					$css_class="danger";
					$text = $t;
					$icon="";
					if(count($parts)>1){
						$css_class=$parts[1];
						$text=$parts[0];
						if(count($parts)>2){
							$icon="<span class='glyphicon glyphicon-".$parts[2]."'></span> ";
						}
					}
					$url_parts=explode(":",$u);
					if(strcasecmp($url_parts[0],"onclick")==0){
						
						$url_final=substr($u,8);
						echo "<a class='btn btn-".$css_class."' href='#' onclick='".php_Grid::replace($url_final,$r)."'>".$icon.$text."</a>";
					}else{
						echo "<a class='btn btn-".$css_class."' href='".php_Grid::replace($u,$r)."'>".$icon.$text."</a>";
					}
				}
				echo '</div></td>';
			}
			echo "</tr>";
		}
		
		echo "</tbody></table>";
	}

	static function dataTable($data, $tableEdit, $table){
		global $db;
		$r = php_Grid::draw($data);
		$r.="<script>";
		if(isset($tableEdit)){
			$url = "?c=".$tableEdit['c']."&m=".$tableEdit['m']."&_data=json";
			$r.='$(".table").Tabledit({
				url: "'.$url.'",
				columns: {
					identifier: [0, "id"],
					editable: [';
					$column_number=0;
					$acl_hash = $db->describe($table);
					foreach($data[0] as $key=>$val){
						if(strcasecmp($acl_hash[$key]['COLUMN_TYPE'],"tinyint(1)")==0){
							$r.='['.$column_number.',"'.$key.'",\'{"0": "0", "1": "1"}\'],';
						}
						if(strcasecmp($key,"All")==0){
							$r.="[".$column_number.",'all','{\"0\": \"Ignore\",\"1\": \"Disable all\", \"2\": \"Enable all\"}'],";
						}
						if(isset($tableEdit["editable"])){
							if(in_array($key,$tableEdit["editable"])){
								$r.='['.$column_number.',"'.$key.'"],';
							}
						}
						$column_number++;
					}
					$r=rtrim($r,',');
				$r.=']
				},
				restoreButton: false,
				onSuccess(data, textStatus, jqXHR){
					$(".table").resize();
					if (data[0] == "deleted"){
						alert("Deleted successfully.");
					} else {
						//alert("Updated successfully");
					}
				}
			});
			';
		}
		$r.='$(".table").DataTable({
				scrollX: true
			});
			$("#loading").html("");
			$("#loading").hide();
			$("#main_table").show();
			</script>';
		return $r;
	}
	function jExcel($table_name,&$data,$columns=array(),$events=array(),$defaults=array(),$function_name=""){
		$id="jExcel_".str_replace(" ","_",$table_name)."_".round(microtime(true)*100);
		
		$class_name=str_replace(" ","_",ucwords(str_replace("_"," ",explode(" ",$table_name)[0])));
		if(!isset($events['onbeforedeleterow'])){
			$events['onbeforedeleterow']="function(instance,y, x, value) {
				var id=-1;
				for(var c in this.columns){
					if(this.columns[c].name=='id'){
						id=c;
						break;
					}
				}
				var success=false;
				if(id>=0 || (typeof Number.isNaN(Number.parseInt(id)) && id!='*')){
					if(this.data[y][id]!='' && this.data[y][id]!='*' ){ 
						$.ajax({
							url:'?c=".$class_name."&m=remove&id='+this.data[y][id]+'&action=ajax&_data=json',
							async: false,
							success:function(adata){
								try{
									var o = JSON.parse(adata);
									success=true;
								}catch(exd){
									prompt(exd,adata);
								}
							}
							
							
						});
					}
				}
				return success;
			}
			";
			
		}
		
		if(!isset($events['onchange'])){
			$events['onchange']="function(instance, cell, x, y, value) {
			
				var id=-1;
				for(var c in this.columns){
					if(this.columns[c].name=='id'){
						id=c;
						break;
					}
				}
				if(id>=0){
					if(this.data[y][id]!='' && this.data[y][id]!='*' ){ 
						$.ajax({
							url:'?c=".$class_name."&m=edit&id='+this.data[y][id]+'&'+this.columns[x].name+'='+escape(value)+'&action=edit&_data=json',
							async: false,
							success:function(adata){
								try{
									var o = JSON.parse(adata);
								}catch(exedit){
									prompt(exedit,adata);
								}
								//alert(adata);
							}
							
							
						});
					}else{					
						if(this.data[y][id]!='*'){
							$('#'+instance.id).jexcel('setValueFromCoords',id,y,'*',true);							
							for(var c in this.columns){
								if(typeof this.meta.defaults[this.columns[c].name]!='undefined' && c!=x){
									$('#'+instance.id).jexcel('setValueFromCoords',c,y,this.meta.defaults[this.columns[c].name],true);
								}
							}
						}										
						this.meta.dirty=true;
					}					
				};
			}";
		}
		if(!isset($events['onselection'])){
			$events['onselection']="function(instance, cell, y, x, value) {
				".$events['beforeselection']."
				if(this.meta.dirty){
					var id=-1;
					var c;
					for(c in this.columns){
						if(this.columns[c].name=='id'){
							id=c;
							break;
						}
					}
					
					if(this.data[y][id]!='*'){
						for(var r in this.data){
							if(this.data[r][id]=='*'){
								var row={};
								for(var f in this.columns){
									if(f!=id && this.columns[f].name!=''){
										row[this.columns[f].name]=this.data[r][f];							
									}
								}
								$.ajax({
									url:'?c=".$class_name."&m=add&_data=json&action=save',
									method:'POST',
									async:false,
									data:row,
									success:function(adata){
										try{
											var o=JSON.parse(adata);
											if(typeof o.id=='undefined'){
												prompt('Record could not be created. ',adata);
											}else{
												$('#'+instance.id).jexcel('setValueFromCoords',id,r,o.id,true);
											}
											
										}catch(e){
											prompt(e,adata);
										}
									}							
								});
							}
						}
						this.meta.dirty=false;
					}				
				}
			}";
			
			unset($events['beforeselection']);
		}
		$data_table=array();
		if(count($data)){
			foreach($data as $row){ // get first row
				break;
			}
			if(!count($row)){
				echo "<div class='well'>Error: First row in data table doesn't have columns...</div>";
				return;
			}
			if(!count($columns)){
				
				foreach($row as $column=>$val){
					$new_column=array(
						"title"=>trim(ucwords(str_replace("_"," ",$column))),				
					);
															
					$columns[]=$new_column;
				}
			}
			
			
			
			?>
			<!-- BEGIN: jExcel(<?php echo $table_name;?> => <?php echo $id;?>) -->
			<div id='<?php echo $id."_div"; ?>' style='width:100%;height:100%'></div>
			<script>
			var <?php echo $id; ?> ;
			console.log("jExcel <?php echo $id; ?>");
			//$(document).ready(function(){
			<?php if(!$function_name){ ?>$(document).ready(function(){<?php }
			else{echo $function_name."=function(){"; } ?>
				<?php echo $id; ?> = jexcel($(<?php echo '"#'.$id.'_div"';?>)[0],{
					tableWidth:"100%",
					defaultColWidth:200,
					allowInsertColumn:false,
					meta:{dirty:false,defaults:<?php echo json_encode($defaults,JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_UNESCAPED_UNICODE);?>},
					search:true,
					pagination:300,
					//tableOverflow:true,
					//lazyLoading:true,
					//loadingSpin:true,
					data:<?php 
						/* TODO: check if name is being used...
						$cnt=0;
						foreach($data as $row){
							$dt=array();
							foreach($row as $c){
								$dt[]=$c;
							}
							echo json_encode($dt,JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_UNESCAPED_UNICODE);
							$cnt++;
							if($cnt<count($data)){
								echo ",";
							}
						}*/
						echo json_encode($data,JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_UNESCAPED_UNICODE);
						
				?>,
					columns:<?php							
						echo json_encode($columns,JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_UNESCAPED_UNICODE);
					?><?php
						if(count($events)){
							foreach($events as $ev=>$code){
								echo ", \n				'".$ev."':".$code;
							}
						}
					?>
				
				})
				<?php 
				if(!$function_name){ 
					echo "});";
				}else{ 
					echo "};";
				} 
				?>
				
				
				console.log("Finish <?php echo $id; ?>");
			
			</script>
			<?php
		}else{
			echo "<div class='well'>No data.</div>";
		}
		?><!-- END: jExcel(<?php echo $table_name;?> => <?php echo $id;?>) --><?php
		return $id;
	}
}