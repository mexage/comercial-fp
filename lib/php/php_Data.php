<?php
class php_Data{
	public static function AreMissing($row,$fields){
		$required=array();
		foreach($fields as $field){
			if(!isset($row[$field])){
				$required[]=$field;
			}
		}
		return $required;
	}
	public static function AddAllSlashes($row){
		foreach($row as &$r){
			$r=addslashes($r);
		}
		return $row;
	}
}