<?php
class php_Fetcher{
	public $db_connection,$fetch_function,$callable_function,$result_pointer;
	__construct($fetchable_result,$function,$fetch_func="mysqli_fetch_assoc",$db_conn=null){
		$this->$db_connection = $db_conn;
		if(!$this->$db_connection){
			global $db;
			$this->$db_connection=$db->dbc;
		}
		$this->$fetch_function = $fetch_func;
		$this->$callable_function = $function;
		$this->$result_pointer = $fetchable_result;
	}
	function fetch(){
		$result = $this->$fetch_function($this->result_pointer);
		if($result===FALSE){
			return FALSE;
		}
		return $this->$callable_function($result);
	}
}