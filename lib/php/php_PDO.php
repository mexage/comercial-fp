<?php

class php_PDO{
	public $dbc;
	
	function php_PDO($conn){
		$this->dbc=new PDO($conn,"","");
	}
	function PDO_search($table,$criteria="",$sort="",$limit=5000){
		if($sort){
			$sort=" ORDER BY ".$sort;
		}
		if(!$criteria){
			return $this->dbc->query("SELECT * FROM ".$table." ".$sort." LIMIT ".$limit);	
		}else{
			return $this->dbc->query("SELECT * FROM ".$table." WHERE ".$criteria." ".$sort." LIMIT ".$limit);
		}
	}
	function PDO_insert($table,$row){
		foreach($row as &$c){
			$c=addslashes($c);
		}
		$q="INSERT INTO ".$table."(".implode(",",array_keys($row)).") VALUES ('".implode("','",$row)."')";
		$this->dbc->exec($q);
		return $this->PDO_get($table,$this->dbc->lastInsertId());
	}
	function PDO_del($table,$id){
		$this->dbc->exec("DELETE FROM ".$table." WHERE id='".$id."'");
	}
	function PDO_upd($table,$id,$row){
		$q="UPDATE ".$table." SET ";
		$fields=array();
		foreach($row as $k=>$v){
			$fields[]=$k."='".addslashes($v)."'";
		}
		$q.=implode(",",$fields);
		$q.=" WHERE id='".addslashes($id)."'";
		
		$this->dbc->exec($q);
		
		return $this->PDO_get($table,$id);
	}
	function PDO_set($table,$id,$field,$value){
		return $this->PDO_upd($table,$id,array($field=>$value));
	}
	function PDO_get($table,$id,$field=null){
		$q="SELECT * FROM ". $table . " WHERE id='".addslashes($id)."';";
		$r=$this->dbc->query($q);
		if($field){
			return $r[0][$field];
		}else{
			return $r[0];
		}
	}
	function query($q){
		trace("Query ".$q);
		if(!$r=mysqli_query($this->dbc,$q)){
			trace("MySQLi error:".mysqli_error($this->dbc));
			return null;
		}
		$result=array();
		while($row=mysqli_fetch_assoc($r)){
			$result[]=$row;
		}
		trace("Fetched ".count($result)." row(s).");
		return $result;
	}
	function PDO_hash($table,$key="id",$filter="1"){
		trace("Hash ".$table."[".$key."] WHERE ".$filter);
		if(!$r=$this->dbc->query("SELECT * FROM ".$table." WHERE ".$filter)){
			return null;
		}
		$result=array();
		while($row=$r->fetchAll(PDO::FETCH_ASSOC)){
			$result[$row[$key]]=$row;
		}
		if(count($result)<20){
			trace("Hash keys:[".implode(",",array_keys($result))."]");
		}else{
			trace("Hash keys:[".count($result)." values.]");
		}
		
		return $result;
	}
	static function phpdate($str){
		if(strcmp($str,"0000-00-00 00:00:00")==0
			||strcmp($str,"0000-00-00")==0
			||strcmp($str,"00:00:00")==0){
			return 0;
		}
		$has_time=strpos($str,":")!==false;
		$has_date=strpos($str,"-")!==false;
		
		if($has_time && $has_date){
			$parts=explode(" ",$str);
			$d=explode("-",$parts[0]);
			$t=explode(":",$parts[1]);			
		}else if($has_time){
			$d=array(0,0,0);
			$t=explode(":",$str);			
		}else if($has_date){
			$d=explode("-",$str);
			$t=array(0,0,0);
		}else{
			return 0;
		}
		if(count($t)!=3 || count($d)!=3){
			return 0;
		}
		return mktime($t[0],$t[1],$t[2],$d[1],$d[2],$d[0]);	
	}
	function datediff(&$table,$name,$field1,$field2,$unit="d",$integers=true){
		$format=array(
			"d"=>1/60/60/24,
			"h"=>1/60/60,
			"m"=>1/60,
			"s"=>1
			
		);
		$factor=$format[$unit];
		foreach($table as $k=>$r){
			
			$t1=self::phpdate($r[$field1]);
			$t2=self::phpdate($r[$field2]);
			if($t1<=0 || $t2<=0){
				$d="";
			}else{
				$d=($t1-$t2)*$factor;
				if($integers){
					$d=floor($d);
				}
			}
			$table[$k][$name]=$d;
		}
	}
	function datesplit(&$table,$field,$types){
		foreach($table as $k=>$r){
			foreach($types as $t=>$f){
				$table[$k][$field."__".$t]=date($f,self::phpdate($r[$field]));
			}
		}
	}
	
}