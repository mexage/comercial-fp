<?php
class php_Field{
	static function number($name,$value="",$label_type=1,$css_class="",$id="",$settings=array("type"=>"number")){
		return self::text($name,$value,$label_type,$css_class,$id,$settings);
	}
	static function password($name,$value="",$label_type=1,$css_class="",$id="",$settings=array("type"=>"password")){
		return self::text($name,$value,$label_type,$css_class,$id,$settings);
	}
	static function date($name,$value="",$label_type=1,$css_class="",$id="",$settings=array("type"=>"date")){
		return self::text($name,$value,$label_type,$css_class,$id,$settings);
	}
	static function readonly($name,$value="",$label_type=1,$css_class="",$id="",$settings=array("type"=>"readonly")){
		return self::text($name,$value,$label_type,$css_class,$id,$settings);
	}
	static function text($name,$value="",$label_type=1,$css_class="",$id="",$settings=array("type"=>"text")){
		if(!$id){
			$id=strtolower(str_replace(" ","_",$name));
		}
		if(strcasecmp($settings['type'],"hidden")==0){
			$label_type=0;
		}
		if(strcasecmp($settings['type'],"textarea")!=0){
			$open ="<input value='".$value."' ";
			$close="/>";
		}else{
			$open ="<textarea ";
			$close =">".$value."</textarea>";
		}
		
		$input=$open." type='".$settings['type']."' name='".strtolower(str_replace(" ","_",$name))."' class='form-control ".$css_class."' id='".$id."' ".$close;
		if(strcasecmp($settings['type'],'readonly')==0){
			$input = "<span  name='".strtolower(str_replace(" ","_",$name))."' class='form-control ".$css_class."' id='".$id."'>".$value."</span>";
		}
		switch($label_type){
			case 1:{
				return "<div class='form-group'><label for='".$id."'> ".$name." </label>".$input."</div>";
			}break;
			case 2:{
				return "<label for='".$id."'> ".$name.$input." </label>";
			}break;
			default:{
				return $input;
			}
		}
		
	}
	static function textarea($name,$value,$label_type=1,$css_class="",$id="",$settings=array("type"=>"textarea")){
		return self::text($name,$value,$label_type,$css_type,$id,$settings);
	}
	static function richText($name,$value,$label_type=1,$css_class="",$id="",$settings=array("type"=>"textarea")){
		if(!$id){
			$id=strtolower(str_replace(" ","_",$name));
		}
		$html = self::textarea($name,$value,$label_type,$css_class,$id,$settings);
		$html .= "<script>new nicEditor({fullPanel : true,maxHeight : 400,width:600}).panelInstance('".$id."');</script>";
		
		return $html;
	}
	static function hidden($name,$value){
		return php_Field::text($name,$value,0,"","",array("type"=>"hidden"));
	}
	static function submit($name,$value="Submit",$css_class="btn btn-primary"){
		return "<input class='".$css_class."' type='submit' name='".$name."' value='".$value."' />";
	}
	
	static function dropdown($name,$list,$value="",$label_type=1,$css_class="",$id=""){
		if(!$id){
			$id=strtolower(str_replace(" ","_",$name));
		}
		$select="<select name='".strtolower(str_replace(" ","_",$name))."' class='form-control ".$css_class."' id='".$id."' >";
		if($list){
			
			foreach($list as $r){
				$selected="";
				if($r['id']==$value){
					$selected="selected='true'";
				}
				$select.="<option value='".$r['id']."' ".$selected.">".$r['name']." (".$r['id'].")"."</option>";
			}
		}
		$select.="</select>";
		/*else{
			$assoc=array_keys($arr) !== range(0, count($arr) - 1);
		}*/
		
		
		
		switch($label_type){
			case 1:{
				return "<div class='form-group'><label for='".$id."'>".$name." </label> ".$select."</div>";
			}break;
			case 2:{
				return "<label for='".$id."'>".$name.$select." </label>";
			}break;
			default:{
				return $select;
			}
		}
		
	}
	static function link_dropdowns($source_id,$dest_id,$ajax_url="",$events=array()){
		
		$dest_table=strtolower($dest_id);
		
		if(strcasecmp(substr($dest_table,-3),"_id")==0){
			$dest_table=substr($dest_table,0,-3);
		}
		
		$source_table=strtolower($source_id);		
		
		$pieces=explode("__",$source_table);
		if(count($pieces)==2){
			$source_table=$pieces[1];
		}
		$source_field=$source_table;
		if(strcasecmp(substr($source_table,-3),"_id")==0){
			$source_table=substr($source_table,0,-3);
		}
		
		
		
		
		if(!$ajax_url){
			$ajax_url="'?c=".$dest_table."&m=_list&fn_0=".$source_field."&fv_0='+$('#".$source_id."').val()+'&_data=json'";
		}
		
		$result='<script>
			$("#'.$source_id.'").change(function(){
			$("#'.$dest_id.'").html("");
			$("#'.$dest_id.'").slideUp();

			'.$events['beforeAJAX'].'
			$.ajax({
				"url":'.$ajax_url.',
				
				"success":function(data){
					try{
						var o=JSON.parse(data);
						
						var html="";
						'.$events['beforeHTML'].'
						for(var i in o){
							html+="<option val=\""+o[i].id+"\">"+o[i].name+" ("+o[i].id+")</option>";
						}
						'.$events['afterHTML'].'
						$("#'.$dest_id.'").html(html);
						$("#'.$dest_id.'").slideDown();
						'.$events['onFinish'].'
					}catch(e){
						'.$events['onCatch'].'
					}
				},
				"error":function(){
					'.$events['onError'].'
				}
			});
		});	
		$("#'.$source_id.'").change();
		</script>';
		
		return $result;
		
	}
	static function combobox($name,$list,$value="",$label_type=1,$css_class="",$id=""){
		if(!$id){
			$id=strtolower(str_replace(" ","_",$name));
		}
		
		$code="";
		
		$code='<script>$( function() {
				$.widget( "custom.combobox", {
				  _create: function() {
					this.wrapper = $( "<span>" )
					  .addClass( "custom-combobox" )
					  .insertAfter( this.element );
			 
					this.element.hide();
					this._createAutocomplete();
					this._createShowAllButton();
				  },
			 
				  _createAutocomplete: function() {
					var selected = this.element.children( ":selected" ),
					  value = selected.val() ? selected.text() : "";
			 
					this.input = $( "<input>" )
					  .appendTo( this.wrapper )
					  .val( value )
					  .attr( "title", "" )
					  .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
					  .autocomplete({
						delay: 0,
						minLength: 0,
						source: $.proxy( this, "_source" )
					  })
					  .tooltip({
						classes: {
						  "ui-tooltip": "ui-state-highlight"
						}
					  });
			 
					this._on( this.input, {
					  autocompleteselect: function( event, ui ) {
						ui.item.option.selected = true;
						this._trigger( "select", event, {
						  item: ui.item.option
						});
						this.element.trigger("change");
					  },
			 
					  autocompletechange: "_removeIfInvalid"
					});
				  },
			 
				  _createShowAllButton: function() {
					var input = this.input,
					  wasOpen = false;
			 
					$( "<a>" )
					  .attr( "tabIndex", -1 )
					  .attr( "title", "Show All Items" )
					  //.tooltip()
					  .appendTo( this.wrapper )
					  .html("<span class=\"ui-button-icon ui-icon ui-icon-triangle-1-s\"></span><span class=\"ui-button-icon-space\"> </span>")
					  .removeClass( "ui-corner-all" )
					  .addClass( "ui-button ui-widget ui-button-icon-only custom-combobox-toggle ui-corner-right custom-combobox-toggle ui-corner-right" )
					  .on( "mousedown", function() {
						wasOpen = input.autocomplete( "widget" ).is( ":visible" );
					  })
					  .on( "click", function() {
						input.trigger( "focus" );
			 
						// Close if already visible
						if ( wasOpen ) {
						  return;
						}
			 
						// Pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
					  });
				  },
			 
				  _source: function( request, response ) {
					var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
					response( this.element.children( "option" ).map(function() {
					  var text = $( this ).text();
					  if ( this.value && ( !request.term || matcher.test(text) ) )
						return {
						  label: text,
						  value: text,
						  option: this
						};
					}) );
				  },
			 
				  _removeIfInvalid: function( event, ui ) {
			 
					// Selected an item, nothing to do
					if ( ui.item ) {
					  return;
					}
			 
					// Search for a match (case-insensitive)
					var value = this.input.val(),
					  valueLowerCase = value.toLowerCase(),
					  valid = false;
					this.element.children( "option" ).each(function() {
					  if ( $( this ).text().toLowerCase() === valueLowerCase ) {
						this.selected = valid = true;
						return false;
					  }
					});
			 
					// Found a match, nothing to do
					if ( valid ) {
					  return;
					}
			 
					// Remove invalid value
					this.input
					  .val( "" )
					  .attr( "title", value + " did not match any item" )
					  .tooltip( "open" );
					this.element.val( "" );
					this._delay(function() {
					  this.input.tooltip( "close" ).attr( "title", "" );
					}, 2500 );
					this.input.autocomplete( "instance" ).term = "";
				  },
			 
				  _destroy: function() {
					this.wrapper.remove();
					this.element.show();
				  }
				});
			 
				$( "#'.$id.'" ).combobox();
				
			  } );</script>
			';
		
		return self::dropdown($name,$list,$value,$label_type,$css_class,$id).$code;
	}
	static function start_form($class,$method,$get_parameters=array(),$http_method="POST"){
		$parameters="";
		if(count($get_parameters)){
			
			$parr=array();
			foreach($get_parameters as $k=>$v){
				$parr[]=$k."=".urlencode($v);
			}
			$parameters="&".implode("&",$parr);
		}
		return "<form action='?c=".$class."&m=".$method.$parameters."' method='".$http_method."'>";
		
	}
	static function end_form(){
		return "</form>";
	}
	static function sub_table($parent_class,$child_class,$data_table,$actions=array(),$hidden=array()){		
		return php_Grid::draw($data_table, $actions, $hidden);
	}
	static function checkbox($name,$list,$value="",$label_type=1,$css_class="",$id=""){
		if(!$id){
			$id=strtolower(str_replace(" ","_",$name));
		}
		$code="
			<script>
			$('#".$id."')
				.hide()
				.after('<input class=\"".$id." chkbox\" type=\"checkbox\" ".
					(
					($value)?
						"checked=\"true\""
						:"\"\""
					)." onchange=\"$(\'#".$id."\').val((this.checked)?\'1\':\'0\');$(\'#".$id."\').trigger(\'change\');\" />');
			</script>";
			
		//[0].selectedIndex=
		return self::dropdown($name,$list,$value,$label_type,$css_class,$id).$code;
	}
	
}

?>