<?php
class php_DFMS{
	
	public $odbc,$conn,$res;
	
	
	function php_DFMS($odbc=""){
		if(!$odbc)$odbc=getConfig("DB/DFMS_ODBC");
		$this->odbc=$odbc;
		$this->open_connection();
	}
	
	function open_connection(){
		
		$this->close_connection();
		
		if($this->odbc){
			$this->conn=odbc_connect($this->odbc,getConfig("DB/DFMS_USER"),getConfig("DB/DFMS_PWD"));
		}else{
			if(!$this->conn = odbc_connect(getConfig("DB/DFMS_ODBC"),getConfig("DB/DFMS_USER"),getConfig("DB/DFMS_PWD"))){$this->conn = odbc_connect(getConfig("DB/DFMS_ODBC1"),getConfig("DB/DFMS_USER"),getConfig("DB/DFMS_PWD"));}
		}
		return $this->conn;
	}
	function close_connection(){
		if($this->conn){
			odbc_close($this->conn);
			unset($this->conn);
		}
		
	}
	
	function exec($sql){
		$retries = 1;
		while(!$this->conn && $retries--){
			sleep(1);
			//if(!$this->conn)
				//trace("Attempt to connect- retries left: ".$retries);
			$this->open_connection();		
		}
		if(!$this->conn){
			trace("CONNECTION ERROR ".odbc_errormsg());
			return false;
		}
		
		if($this->res=odbc_exec($this->conn,$sql)){
			trace("EXEC: ".$sql);
		}else{
			trace("ERROR: ".odbc_errormsg($this->conn)." SQL:".$sql);
		}
		return $this->res;
	}
	function fetch(){
		return odbc_fetch_array($this->res);
	}
	
	function query($sql){
		$result = array();
		$this->exec($sql);
		if(!$this->res){
			//trace("ODBC ERROR: ".odbc_errormsg($this->conn)." ".$sql);
			return false;
		}
		
		while($row=$this->fetch()){
			$result[]=$row;
		}
		trace("Fetched ".count($result)." records.");
		return $result;
	}
	function hash($table,$key,$filter=""){
		$result = array();
		
		$sql = "SELECT * FROM ".$table."";
		
		if($filter){
			$sql.=" WHERE ".$filter;
		}
		
		$this->exec($sql);
		if(!$this->res){
			//trace("ODBC ERROR: ".odbc_errormsg($this->conn)." ".$sql);
			return false;
		}
		
		while($row=$this->fetch()){
			$result[$row[$key]]=$row;
		}
		trace("Fetched ".count($result)." row(s)");
		return $result;
	}
	
	
}