<?php
class php_Controller{
	public $db,$title,$user,$has_access,$message="",$message_class="info",$template="";
	function __construct($args = NULL){
		global $db;
		global $user;
		$this->db = $db;
		$this->user = $user;
		$this->has_access=true;
		if(isset($_SESSION['message'])){
			$this->message=$_SESSION['message'];
		}
		if(isset($_SESSION['message_class'])){
			$this->message_class=$_SESSION['message_class'];
		}
		
		$templates=getConfig("TEMPLATE_PATHS");
		if(count($templates)){
			foreach($templates as $pattern=>$tmp){
				if(preg_match($pattern,$args['a']."-".$args['c']."-".$args['m'])){
					//trace($pattern."=>".$args['a']."-".$args['c']."-".$args['m']." = true");
					$this->template=$tmp;
				}else{
					//trace($pattern."=>".$args['a']."-".$args['c']."-".$args['m']." = false");
				}
			}
		}
	}
	function _generate_messages(){
		if($this->message){
			$_SESSION['message']="";
			$_SESSION['message_class']="";
			return "<div class='alert alert-".$this->message_class."'>".$this->message."</div>";
		}else{
			return "";
		}
	}
	function set_message($msg,$msg_class="info"){
		$_SESSION['message']=$msg;
		$_SESSION['message_class']=str_ireplace("primary","info",$msg_class);
		
		$this->message=$msg;
		$this->message_class=$msg_class;
	}
	function get_message(){
		return array("message"=>$_SESSION['message'],"message_class"=>$_SESSION['message_class']);
	}
	function index($args,$filter="1",$order="",$limit=5000){
		$result = array();
		$table_name=strtolower($args['c']);
		$result[$table_name] = $this->db->search($table_name,$filter,$order,$limit);
		
		return $result;
	}
	function view($args){
		$result = array();
		if(strcasecmp($args['action'],"edit")==0){
			return "?c=".$args['c']."&m=edit&id=".$args['id'];
		}
		$table_name=strtolower($args['c']);
		$result[$table_name] = $this->db->get($table_name,$args['id']);

		return $result;
	}
	
	function add($args){
		$result = array();
		$table_name=strtolower($args['c']);
		if(strcasecmp($args['action'],"save")==0){
			$row=array();
			$fields=$this->db->describe($table_name);
			foreach($fields as $field){
				// Special auto-columns
				if(strcasecmp($field["COLUMN_NAME"],"created_by__users_id")==0 && !isset($args['created_by__users_id'])){
					// Created by
					$args['created_by__users_id']=$this->user->attr("id");
				}
				
				// Only non-empty and non-autoincrement columns
				if(!isset($args[$field["COLUMN_NAME"]])
					|| stripos($field["EXTRA"],"AUTO_INCREMENT")===true				
				){
					// Omit auto incremental columns
					continue;
				}
				
				// Copy the value
				$row[$field["COLUMN_NAME"]]=$args[$field["COLUMN_NAME"]];
			}
			
			if(count($row=$this->db->insert($table_name,$row))){
				$this->set_message("Record was added correctly.","success");
				if(isset($args['_data'])){
					return $row;
				}else{
					if(isset($row['id'])){
						return "?c=".$args['c']."&m=edit&id=".$row['id'];
					}else{
						// not auto inc
						return "?c=".$args['c']."&m=edit&id=".$args['id'];
					}
				}
			}else{
				trace("Could not save ".var_export($row,true));
				if(isset($args['_data'])){
					return array();
				}else{
					$this->set_message("Could not save. ". $row,"danger");				
				}
			}
			
		}
		
		$result[$table_name] = $this->db->get($table_name,$args['id']);
		
		
		return $result;
	}
	
	function edit($args){
		$result = array();
		$table_name=strtolower($args['c']);
		if(strcasecmp($args['action'],"save")==0 ||strcasecmp($args['action'],"edit")==0){
			$row=array();
			$fields=$this->db->describe($table_name);
			foreach($fields as $field){
				// Special auto-columns
				if(strcasecmp($field["COLUMN_NAME"],"modified_by__users_id")==0 && !isset($args['modified_by__users_id'])){
					// Modified by
					$args[$field["COLUMN_NAME"]]=$this->user->attr("id");
				}else if(strcasecmp($field["COLUMN_NAME"],"modified")==0 && !isset($args['modified'])){
					// Modified
					$args[$field["COLUMN_NAME"]]=date("Y-m-d H:i:s");
				}
				
				
				if(!isset($args[$field["COLUMN_NAME"]])){					
					continue;
				}
					
				$row[$field["COLUMN_NAME"]]=$args[$field["COLUMN_NAME"]];
				
			}
			
			if(strcasecmp($args['action'],"save")==0){
				if($this->db->upd($table_name,$args['id'],$row)){
					$this->set_message("Record was edited correctly.","success");	
					return "?c=".$args['c']."&m=".$args['m']."&id=".$args['id'];				
				}else{
					$this->set_message("Could not save.","danger");				
				}
			}else if(strcasecmp($args['action'],"edit")==0){
				if($this->db->upd($table_name,$args['id'],$row)){
					return array("Updated correctly");
				}
			}
			
		}
		
		$result[$table_name] = $this->db->get($table_name,$args['id']);
		
		$result['slave_tables']=array();
		$external=$this->db->external_links($args['c']);
		$result['external']=$external;
		foreach($external as $row){
			$result['slave_tables'][$row['TABLE_NAME']." ".$row['COLUMN_NAME']]=array(
				"description"=>$row				
			);				
			$result[$row['TABLE_NAME']." ".$row['COLUMN_NAME']]=$this->db->search($row['TABLE_NAME'],"`".$row['COLUMN_NAME']."`='". addslashes($result[$table_name]['id'])."'","",200);
		}
		
		
		
		return $result;
	}
	function _list($args){
		
		$table_name=strtolower($args['c']);
		$args['term']=addslashes($args['term']);
		$mask = str_ireplace(" ","%",$args['term']);
		if($args['term']){
			$by_id=$this->db->search($table_name,"id = '".$args['term']."'");
			$name_match=$this->db->search($table_name,"name ='".$args['term']."'");
			$name_starts_with=$this->db->search($table_name,"name like '".$args['term']."%'");
			$name_contains=$this->db->search($table_name,"name like '%".$mask."%'");
			if(isset($args['field'])){
				$by_field=$this->db->search($table_name,$args['field']." like '%".$args['term']."%'");
			}else{
				$by_field=array();
			}
			
			$words = explode(" ",$args['term']);
			$name_and_mask = array();
			
			if(count($by_id)+count($by_field)+count($name_match)+count($starts_with)+count($name_contains)==0){
				$and_mask = "name like '%".implode("%' AND name like '%",$words)."%'";
				$name_and_mask=$this->db->search($table_name,$and_mask);
			}
			
			
			$name_or_mask=array();
			if(count($by_id)+count($by_field)+count($name_match)+count($starts_with)+count($name_contains)+count($name_and_mask)==0){
				// Last resort
				
				$or_mask = "name like '%".implode("%' OR name like '%",$words)."%'";
				
				$name_or_mask=$this->db->search($table_name,$or_mask,"",10);
			}
			
			$result=array_merge($by_id,$by_field,$name_match,$name_starts_with,$name_contains,$name_and_mask,$name_or_mask);
		}else{
			$fi=0;
			$filters=array("1");
			$columns=$this->db->describe($table_name);
			while(isset($columns[$args['fn_'.$fi]])){
				$operator=(isset($args["fo_".$fi]))?$args["fo_".$fi]:"=";
				if(strcasecmp($operator,"!")==0){
					$operator.="=";
				}
				$filters[]=$args['fn_'.$fi].$operator."'".addslashes($args['fv_'.$fi])."'";
				$fi++;
			}
			
			$result=$this->db->search($table_name,implode(" AND ",$filters));		
		}
		return $result;
	}
	function _require_access($keys){
		$permissions=array();
		if(is_string($keys)){
			$permissions[]=$keys;
		}else{
			$permissions=$keys;
		}
		foreach($permissions as $key){
			$has=$this->user->get($key);
			if(!count($has)){
				if($this->api_access()){
					$this->has_access=true;
					return true;
				}
			}else{
				$this->has_access=true;
				return true;
			}
			
		}
		$this->has_access=false;
		$this->set_message("Need '".implode("' or '",$permissions)."' permission.","danger");
		return false;
	}

	function api_access(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		if(strcasecmp($ip,"::1")==0){
			$ip="127.0.0.1";
		}
		$access=$this->db->search("api_permissions","ip='".$ip."' AND controller='".$_REQUEST['c']."' AND method='".$_REQUEST['m']."'");
		if($access!=null && $access[0]['value']){
			return true;
		}else {
			return false;
		}
	}

	function remove($args){
		// not implemented by default.
	}
	function json($args){
		$result=$this->db->search($args['c']);
		$this->db->fill($result,"name");
		return $result;
	}
	
	
}