<?php
class php_Form {
	public static function draw($id,$action,$method,$fields,$defaults=array(),$attr=array()){
		
	}
	public static function get_fields($table){
		$result=array();
		$fields = php_mysqli::describe($table);
		foreach($fields as $name=>$desc){
			if(strcasecmp($desc['DATA_TYPE'],"LONGTEXT")==0){
				$fields[$name]="textarea";
			}else if(strcasecmp($desc['DATA_TYPE'],"DATETIME")==0){
				$fields[$name]="date";
			}else if(strcasecmp($desc['DATA_TYPE'],"TIMESTAMP")==0){
				$fields[$name]="readonly";
			}else if(strcasecmp($desc['DATA_TYPE'],"TINYINT")==0){
				$fields[$name]="checkbox";
			}else if(strcasecmp($desc['DATA_TYPE'],"INT")==0){
				$fields[$name]="number";
			}else{
				$fields[$name]="text";				
			}
		}
		return $result;
	}
	public static function field($name,$type,$default){
		$result="";
		$id=strtolower(str_replace(" ","_",$name));
		$human_name=ucwords(str_replace("_"," ",$name));
		if(strcasecmp($type,"textarea")==0){
			$result.=php_Field::textarea($name,$default,1,"","",array("type"=>$type));
		}else if(strcasecmp($type,"readonly")==0){
			$result.="<div class='form-group'><label for='".$id."'><span class='label label-default'>".$name."</span></label></div>";
		}else if(strcasecmp($type,"hidden")==0){
			$result.=php_Field::hidden($name,$default);
		}else{
			$result.=php_Field::text($name,$default,1,"","",array("type"=>$type));
		}
		return $result;
	}
	public static function add_options($id,$options){
		
	}
	
}