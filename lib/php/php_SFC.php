<?php
class php_SFC{
	
	public $odbc,$conn,$res;
	
	
	function php_SFC($odbc=""){
		if(!$odbc)$odbc=getConfig("DB/SFC_ODBC");
		$this->odbc=$odbc;
		$this->open_connection();
	}
	
	function open_connection(){
		
		$this->close_connection();
		
		if($this->odbc){
			$this->conn=@odbc_connect($this->odbc,"doit","foxconn123");
		}else{
			if(!$this->conn = @odbc_connect(getConfig("DB/SFC_ODBC"),"doit","foxconn123")){$this->conn = odbc_connect(getConfig("DB/SFC_ODBC1"),"doit","foxconn123");}
		}
		return $this->conn;
	}
	function close_connection(){
		if($this->conn){
			odbc_close($this->conn);
			unset($this->conn);
		}
		
	}
	
	function exec($sql){
		$retries = 3;
		while(!$this->conn && $retries--){
			sleep(1);
			//if(!$this->conn)
			$this->open_connection();
		}
		if(!$this->conn){
			trace("CONNECTION ERROR ".odbc_errormsg());
			//return false;
		}
		if($this->res=odbc_exec($this->conn,$sql)){
			trace("EXEC: ".$sql);
		}else{
			trace("ERROR: ".odbc_errormsg($this->conn)." SQL:".$sql);
		}
		return $this->res;
	}
	function fetch(){
		return odbc_fetch_array($this->res);
	}
	
	function query($sql){
		$result = array();
		$this->exec($sql);
		if(!$this->res){
			//trace("ODBC ERROR: ".odbc_errormsg($this->conn)." ".$sql);
			return false;
		}
		
		while($row=$this->fetch()){
			$result[]=$row;
		}
		trace("Fetched ".count($result)." records.");
		
		return $result;
	}
	function hash($table,$key,$filter=""){
		$result = array();
		
		$sql = "SELECT * FROM ".$table."(nolock)";
		
		if($filter){
			$sql.=" WHERE ".$filter;
		}
		
		$this->exec($sql);
		if(!$this->res){
			//trace("ODBC ERROR: ".odbc_errormsg($this->conn)." ".$sql);
			return false;
		}
		
		while($row=$this->fetch()){
			$result[$row[$key]]=$row;
		}
		trace("Fetched ".count($result)." row(s)");
		return $result;
	}
	
	
}