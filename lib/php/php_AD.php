<?php

class php_AD{
	static public $domains=array(
		'MX'=>array(
			'host'=>"MXSVR-DC02.mx.PCEBG.COM",		//mx.pcebg.com
			'group'=>"dc=mx,dc=pcebg,dc=com",
			'prefix'=>'MX\\',
			'search_dn'=>'CN=Otrs Search,OU=Special,OU=IT,OU=Users,OU=Building A2,OU=Juarez,OU=Mexico,DC=mx,DC=PCEBG,DC=COM',
			'search_password'=>'SJuserRead98',
			'base_dn'=>"DC=mx,DC=PCEBG,DC=COM"

		),
		'FOXCONN-NA'=>array(
			'host'=>"JZRODC01.na.foxconn.com",		//na.foxconn.com
			'group'=>"dc=na,dc=foxconn,dc=com",
			'prefix'=>'FOXCONN-NA\\',
			'search_dn'=>'CN=OTRS SEARCH,OU=JZ - IT,OU=JZ - Users,OU=MX_Juarez_CMMSG_Site01,DC=na,DC=foxconn,DC=com',
			'search_password'=>'12345678',
			'base_dn'=>"DC=na,DC=foxconn,DC=com"
		)
	);
	static public $sso_user=false;
	static function get_sso_user(){
		if(self::$sso_user===false){
			$IP=php_Auth::get_IP();
			
			if(
				strcasecmp(substr($IP,0,3),"10.")==0
				||strcasecmp(substr($IP,0,4),"192.")==0			
				||strcasecmp($IP,"::1")==0			
			){
				exec("wmic /failfast:500 /node:$IP COMPUTERSYSTEM Get UserName", $user);
				if($user[1]){
					foreach(self::$domains as $domain=>$config){
						if(strcasecmp(substr($user[1],0,strlen($domain)+1),$domain."\\")==0){
							self::$sso_user=$user[1];
							return $user[1];
						}
					}			
				}
			}
			self::$sso_user="";
			return "";		
		}else{
			return self::$sso_user;
		}
		
		
	}
	static function validate_login($usr,$pwd){		
		//SSO
		// Needs special setup. See https://stackoverflow.com/questions/20994329/apache-how-to-get-remote-user-variable
		
		$user_no_domain=explode("\\",php_AD::get_sso_user());
		$user_no_domain=$user_no_domain[count($user_no_domain)-1];
		trace($user_no_domain);
		if(
			strcasecmp($usr,$user_no_domain)==0
			&&strcasecmp($pwd,"")==0
			&&strcasecmp(php_AD::get_sso_user(),"")!=0
		){
			// validate domain
			foreach(self::$domains as $domain=>$config){
				if(strcasecmp(substr(php_AD::get_sso_user(),0,strlen($domain)+1),$domain."\\")==0){
					return true;
				}
			}
			trace("exiting SSO");
			return false;
		}
		
		if(!$pwd || !$usr){
			return false;
		}		
		
		foreach(self::$domains as $domain){
			//Connecting...
			$user=$domain['prefix'].$usr;
			
			$ldapconn = ldap_connect($domain['host']);
			@ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
			@ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);

			

			//Authenticating...
			//file_put_contents("cred.txt",$ldapconn."\n".$dn."\n".$password);
			
			$bind_search = @ldap_bind($ldapconn,$domain['search_dn'],$domain['search_password']);
			
			$filter="(samaccountname=".$usr.")";
			
			$res = @ldap_search($ldapconn, $domain['base_dn'], $filter);
			$first = @ldap_first_entry($ldapconn, $res);
			$dn = @ldap_get_dn($ldapconn, $first);
			
			if(!$dn){
				
				$bind=false;
			}else{
				$bind = @ldap_bind($ldapconn,$dn,$pwd);
			}
			
			//Only works for MX
			//$bind=@ldap_bind($ldapconn,$user,$pwd);
			if($bind){
				$_SESSION["bins"]=array();
				$_SESSION["bins"]['user']=$user;
				@ldap_close($ldapconn);
				return true;
			}else{
				@ldap_close($ldapconn);
				
			}
		}
		return false;
		
	}
}