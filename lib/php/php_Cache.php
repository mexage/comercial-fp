<?php
class php_Cache{
	public $controller;
	public $module;
	public $id;
	public $mode;
	public $path;
	function __construct($c,$m,$i,$t){
		$this->controller=$c;
		$this->module=$m;
		$this->id=$i;
		$this->mode=$t;
		$this->path="app/".$c."/cache/";
		@mkdir($this->path,0700,true);
		file_put_contents($this->path."index.php","");
		$this->path.=$c.".".$m.".".$i.".".$t;
	}
	function exists(){
		return file_exists($this->path);
	}
	function age(){
		if(file_exists($this->path)){
			return (time()-filemtime($this->path))/60;
		}else{
			return 99999999;
		}
	}
	function modified($format="Y-m-d H:i:s"){
		return date($format, filemtime($this->path));
	}
	function get_raw(){
		if(!$this->exists()){
			trace("Get chache ".$this->mode." ".$this->path." empty!");
			return null;
		}else{
			trace("Get chache ".$this->mode." ".$this->path);
			return file_get_contents($this->path);
		}
	}
	function get(){
		$raw=$this->get_raw();
		;
		if(strcasecmp("serial",$this->mode)==0){
			
			return unserialize($raw);
		}else if(strcasecmp("json",$this->mode)==0){
			return json_decode($raw,true);
		}else{			
			return $raw;
		}
	}
	function set_raw($raw){
		trace("Setting cache ".$this->path." (".count($raw).")");
		file_put_contents($this->path,$raw);
	}
	function set($data){
		if(strcasecmp("serial",$this->mode)==0){
			$this->set_raw(serialize($data));
		}else if(strcasecmp("json",$this->mode)==0){
			$this->set_raw(json_encode($data,JSON_UNESCAPED_UNICODE|JSON_PARTIAL_OUTPUT_ON_ERROR));
		}else{
			$this->set_raw($data);
		}
	}

}