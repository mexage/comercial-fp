<?php

class php_Auth{
	public static $DOIT_WEBPAGE_PROXY = '10.13.0.44';
	public static $acl;
	function __construct(){
	}
	public static function get($field){
		// Usage: if(!php_Auth::get("login")){ return "?c=Bins&m=login&requested_c=Bins&requested_m=counting"; } => true/false 1/0
		if(!self::$acl){
			self::$acl = self::get_acl(self::current_user());			
		}
		if(!self::$acl){
			return null;
		}	
		return self::$acl[$field];			
	}
	public static function request_login($args){
		return "?c=Bins&m=login&requested_c=".$args['c']."&requested_m=".$args['m'];
	}
	public static function request_logout($args){
		return "?c=Bins&m=logout&requested_c=".$args['c']."&requested_m=".$args['m'];
	}
	
	public static function current_user(){
		if(self::is_logged()){
			return $_SESSION['bins']['user'];
		}else{
			return '';
		}
	}
	public static function is_logged(){
		return isset($_SESSION['bins']['user']);
	}	
	public static function add_user($user){
		global $db;
		$users = self::get_acl($user);		
		if(count($users)){
			return true;
		}else{
			return false;
		}
	}
	public static function set_acl($user_id,$acl){
		global $db;
		return $db->upd("bin_acl",$user_id,$acl);
	}
	public static function clean_user($user){
		$user=trim($user);
		if(is_numeric($user)){
			$user=ltrim($user,"0");
		}
		return addslashes($user);
	}
	public static function get_acl($user){
		if($user == null){ return false; }
		global $db;
		$users = $db->query("SELECT * FROM bin_acl WHERE username='".self::clean_user($user)."'");
		if(!count($users)){ // Default
			$deft = $db->search("bin_acl", "username = 'default'", "", "1");
			unset($deft[0]['id']);
			unset($deft[0]['message']);
			unset($deft[0]['created']);
			$deft = array_replace($deft[0], array("username"=>$user));
			$users = $db->insert("bin_acl", $deft);
			//$users = $db->insert("bin_acl",array("username"=>self::clean_user($user),"save"=>1,"login"=>1,"transfer_pallet"=>1,"load_pallet"=>1));
		}
		return $users[0];
	}
	public static function is_proxy($proxy){
		return strcasecmp(trim($_SERVER['REMOTE_ADDR']),$proxy) == 0;
	}
	public static function set_session($session_arr){
		$_SESSION['bins']=$session_arr;
	}
	public static function get_session_id(){
		return session_id();
	}
	public static function get_session(){
		if(isset($_SESSION['bins'])){
			return $_SESSION['bins'];
		}else{
			return false;
		}
	}
	public static function logout(){
		unset($_SESSION['bins']);
	}
	public static function login($user,$pwd=""){
		
		if(self::is_logged()){
			self::logout();
		}
		$ad_validated = false;
		
		if(!is_numeric(trim($user))){
			
			if(php_AD::validate_login($user,$pwd)){
				
				$acl = self::get_acl($user);
				//trace(var_export($acl,true));
				if(self::is_proxy(self::$DOIT_WEBPAGE_PROXY) && !$acl["remote_login"]){
					self::logout();
					return false;
				}
				if(!$acl['login']){
					self::logout();
					return false;
				}
				//$session = array();
				$_SESSION['bins']=array();
				$_SESSION['bins']['user'] = trim($user);
				$_SESSION['bins']['acl'] = $acl;
				self::actualizar_login($acl['id']);	
				
				return true;
			}
		}else{
			if(self::is_proxy(self::$DOIT_WEBPAGE_PROXY)){//doit.foxconn.com
				self::logout();
				return false;	
			}else{			
				$user=ltrim(trim($user),"0");
				trace("NOT PROXY");
				$acl = self::get_acl($user);
				if(!empty($acl) && $acl['login']){
					trace("NOT EMPTY");
					$_SESSION['bins']=array();
					$_SESSION['bins']['user'] = trim($user);
					$_SESSION['bins']['acl'] = $acl;								
					self::actualizar_login($acl['id']);	
					return true;
				}else{
					self::logout();
					return false;
				}	
			}
		}
		self::logout();
		return false;
	}
	public static function actualizar_login($id){
		global $db;
		$ip = self::get_IP();
		$dat = array("last_login"=>date("Y-m-d H:i:s"), "last_ip"=>$ip, "last_session"=>session_id());
		return $db->upd("bin_acl",$id, $dat);
	}
	public static function get_IP(){
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
    		return $_SERVER['HTTP_CLIENT_IP'];
   
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    		return $_SERVER['HTTP_X_FORWARDED_FOR'];
   
		return $_SERVER['REMOTE_ADDR'];
	}
	public static function check_permits(){
		
		return $_SESSION['bins']['acl'];
	}
}

/*echo "<pre>";
echo "Session = ".session_id()."\n";
echo "IP :".php_Auth::get_IP()."\n";
echo "islogged = ".php_Auth::is_logged()."\n";
echo "logout = ".php_Auth::logout()."\n";
echo "islogged = ".php_Auth::is_logged()."\n";
echo "clean_user 0123 = ".php_Auth::clean_user("0123")."\n";
echo "clean_user ' guillermo.morales = '".php_Auth::clean_user(" guillermo.morales")."'\n";
echo "get_session = '".php_Auth::get_session()."'\n";
//echo "get_acl ='".php_Auth::get_acl("8506")."'\n";
echo "is_proxy 10.13.0.44 = '".php_Auth::is_proxy("10.13.0.44")."'\n";
echo "is_proxy  = '".php_Auth::is_proxy("")."'\n";
echo "identify_by_name : '".php_Auth::identify_by_name('edgar')."'\n";*/


?>