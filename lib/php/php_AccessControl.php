<?php

class php_AccessControl{
	public $user;
	public $permissions;
	function __construct(){
		if(isset($_SESSION['doit'])){
			if(isset($_SESSION['doit']['AccessControl'])){
				if(
					isset($_SESSION['doit']['AccessControl']['user'])
					&&isset($_SESSION['doit']['AccessControl']['permissions'])){
					$this->user=$_SESSION['doit']['AccessControl']['user'];
					$this->permissions=$_SESSION['doit']['AccessControl']['permissions'];
				}else{
					unset($_SESSION['doit']);
				}
			}else{
				unset($_SESSION['doit']);
			}
		}
	}
	static function pass_hash($password){
		$pwd_hash=sha1(getConfig("SECURITY/SALT").$password);
		return $pwd_hash;
	}
	function login($username,$password){
		global $db;
		$user_no_domain=explode("\\",$username);
		if(!$username)return false;
		$username=$user_no_domain[count($user_no_domain)-1];
		$pwd_hash=php_AccessControl::pass_hash($password);
		$u=$db->search("users","id='".addslashes($username)."' AND pass_hash='".$pwd_hash."'");
		if(!count($u)){
			$s=$db->search("sessions","id='".addslashes($username)."' AND token='".$password."'");
			// TODO: add something to check if token matches the session to check only one can use it at the same time.
			if(count($s)){
				$u=$db->search("users","id='".addslashes($username)."'");
			}
		}
		if(!count($u))return false;
		
		$this->user=$u[0];
			
		$this->permissions=$db->hash("user_permissions","permission","users_id='".$this->user['id']."'");
					
		$_SESSION['doit']=array(
			"AccessControl"=>array(
				"user"=>$this->user,
				"permissions"=>$this->permissions
			)
		);
		// Session
		// Generate token and save to allow for session recovery
		$token=sha1(var_export($_SERVER,true));
		setcookie("token",$token,time()+3600*24*365);
		setcookie("user",$username,time()+3600*24*365);
		$session=$db->get("sessions",addslashes($username));
		if($session){
			$db->del("sessions",$username);
		}
		$db->insert("sessions",array(
			"id"=>$username,
			"session"=>session_id(),
			"token"=>$token,
			"data"=>""	
		));
		
		return true;
	}
	
	function logout(){
		global $db;
		unset($_SESSION['doit']);
		unset($_SESSION['bins']);
		setcookie("token","",time()-3600);
		setcookie("user","",time()-3600);
		$db->del("sessions",$this->attr("id"));
	}
	function is_logged(){
		return isset($_SESSION['doit']);
	}
	function attr($field){
		return $this->user[$field];
	}
	function get($key){
		trace("Check for " . $key ." in permissions(".$key."=>".var_export($this->permissions,true).")");
		trace("User: ".var_export($this->user,true));
		return $this->permissions[$key];
	}
	function query($mask){
		global $db;		
		return $db->search("user_permissions","users_id='".$this->user['username']."' LIKE '".addslashes($mask)."'");		
	}
	public static function logout_url($args){
		return "?m=logout&requested_c=".$args['c']."&requested_m=".$args['m'];
	}
	public static function login_url($args){
		return "?m=login&requested_c=".$args['c']."&requested_m=".$args['m'];
	}
	
	
}