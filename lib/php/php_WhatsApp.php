<?php
class php_WhatsApp{
	static function send($id,$phone,$message){
		global $db;
		if(!trim($phone)){
			trace("Ignoring message, no phone provided. '".$message."'");
			return;
		}
		$row=array(
			"name"=>addslashes($phone),
			"message"=>addslashes($message)
		);
		$db->insert("whatsapp_messages",$row);
	}
	
}
?>