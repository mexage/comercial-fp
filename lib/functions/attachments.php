<?php

function get_attachment_folder($controller,$id){
	if(!preg_match('/^[a-z_0-9]*$/i',$controller)){
		return false;
	}
	if(!preg_match('/^[a-z_0-9]*$/i',$id)){
		return false;
	}
	return "s/".strtolower($controller)."/".strtolower($id)."/";
}

function ensure_attachment_folder($controller,$id){
	if(!preg_match('/^[a-z_0-9]*$/i',$controller)){
		return false;
	}
	if(!preg_match('/^[a-z_0-9]*$/i',$id)){
		return false;
	}
	
	if(!file_exists("s/")){
		mkdir("s/");
		file_put_contents("s/index.php","");
	}
	if(!file_exists("s/".strtolower($controller)."/")){
		mkdir("s/".strtolower($controller)."/");
		file_put_contents("s/".strtolower($controller)."/index.php","");
	}
	if(!file_exists("s/".strtolower($controller)."/".strtolower($id)."/")){
		mkdir("s/".strtolower($controller)."/".$id."/");
		file_put_contents("s/".strtolower($controller)."/".$id."/index.php","");
	}
}
function attachment_filename($upload_field,$controller,$id){
	$pathinfo=(pathinfo($_FILES[$upload_field]["name"]));
	
	$filename= $pathinfo['basename'];		
	
	return $filename;
}
function store_attachment($upload_field,$controller,$id,$filename="",$valid=array("png","jpg","gif","jpeg","pdf","xls","xlsx","ppt","pptx","doc","docx","zip","rar","7z","xlsm")){
	if(!preg_match('/^[a-z_0-9]*$/i',$controller)){
		trace("Invalid controller: '".$controller."'");
		return false;
	}
	if(!preg_match('/^[a-z_0-9()\-\.]*$/i',$id)){
		trace("Invalid id: '".$id."'");
		return false;
	}
	
	if(!isset($_FILES[$upload_field])){
		trace("No file on ".$upload_field);
		return false;
	}
	
	$pathinfo=(pathinfo($_FILES[$upload_field]["name"]));
	
	if(!$filename){
		$filename= $pathinfo['basename'];		
	}
	
	
	if(!preg_match('/^[a-z_0-9. ]*$/i',$filename)){
		trace("Invalid filename: '".$filename."'");
		return false;
	}
	
	$target_file = get_attachment_folder($controller,$id).$filename;
	
	
	if(array_search(strtolower($pathinfo['extension']),$valid)===false){
		trace("Invalid extension: ". var_export($pathinfo,true));
		return false;
	}else{
		ensure_attachment_folder($controller,$id);
		trace("Saving to ".$target_file);
		trace("pathinfo: ".var_export($pathinfo,true));
		trace("Files: ".var_export($_FILES,true));
		return move_uploaded_file($_FILES[$upload_field]["tmp_name"],$target_file);
	}
}

function read_attachment($controller,$id,$file){
	
	$path=get_attachment_folder($controller,$id).$file;
	
	if($path===false)exit;
	
	if (file_exists($path)) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($path).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($path));
		readfile($path);
		exit;
	}else{
		exit;
	}
}
function dir_attachment($controller,$id){
	$directory=get_attachment_folder($controller,$id);
	if(!file_exists($directory)){
		return false;
	}
	$d = dir($directory);
	
	$file_list=array();
	while (false !== ($entry = $d->read())) {
		if(
			strcasecmp($entry,".")!=0
			&&strcasecmp($entry,"..")!=0
			&&strcasecmp($entry,"index.php")!=0
		
		){
			$file_list[]=array("c"=>$controller,"id"=>$id,"name"=>$entry,"size"=>filesize($directory.$entry),"modified"=>filemtime($directory.$entry));
		}
	}
	$d->close();
	return $file_list;
}
