<?php 

// para cargar clases
function __autoload($c){
	$file="";
	
	if(!preg_match('/^[a-z_0-9]*$/i',$c)){
		trace("Anti-hack: ".$c."\n".var_export($_SERVER,true));
		die("El nombre de la clase es inválido.");
		mail(getConfig("WebmasterEmail"),"INTENTO HACKEO","Se intent� hacer hack:".$c."\n".var_export($_SERVER,true));
	}
	// Librería php_	
	if(substr($c,0,4)=="php_"){
		$file="lib/php/".$c.".php";
	}else
	// Modelos
	if(substr($c,0,4)=="mod_"){
		$file="app/".substr($c,4)."/".$c.".php";
	}else // Componentes
	if(substr($c,0,4)=="com_"){
		$file="com/".$c.".php";
	}else{
		// Controladores
		$file= "app/".str_replace(" ","_",ucwords(str_replace("_"," ",$c)))."/index.php";
	}
	if(!file_exists($file)){
		trace("ERROR during autoload file:".$file,true);
	}else{
		require_once $file;
	}
	
}
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}
?>