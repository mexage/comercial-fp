<?php
function do_HTTP_POST($url,$data,$method="GET"){

	//$url = 'http://10.13.9.47/otrs/index.pl';
	//$data = array('q' => 'test');

	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => $method,
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	if ($result === FALSE) { /* Handle error */ }
	return $result;
}