<?php

function download($file_path,$target_name){
	if(!$file_path){
		trace("No File Path");
		exit;
	}
	if(!file_exists($file_path)){
		trace("File '".$file_path."' does not exist.");
		exit;
	}
	
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="'.$target_name.'"');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($file_path));
	readfile($file_path);
	exit;

}