<?php

function getConfig($path){
	global $db;
	$value = $GLOBALS['cnf'][$path];
	if(!isset($GLOBALS['cnf'][$path])){
		$result = $db->query("SELECT value FROM configurations WHERE path='".$path."' LIMIT 1");
		$value = $result[0]['value'];
	}
	return $value;
}
function setConfig($path,$val){
	$GLOBALS['cnf'][$path]=$val;
}
function saveConfig($path,$val,$comment=""){
	global $db;
	$user = php_Auth::current_user();
	if($user != "" && $path != ""){
		$search_path = $db->query("SELECT * FROM configurations WHERE path='".$path."' LIMIT 1");
		$str_date = strftime("%Y-%m-%d %H:%M");
		if(count($search_path)){
			if($search_path[0]['value'] != $val || $search_path[0]['comment'] != $comment){
				if($comment != "" && $search_path[0]['comment'] != $comment){
					$updated_comment = $comment;
				} else {
					$updated_comment = $search_path[0]['comment'];
				}
				$new_log = $search_path[0]['log']."\nUser: ".$user." changed value: ".$search_path[0]['value']." to: ".$val." at: ".$str_date;
				$updated_path = array(
					"value" => $val,
					"comment" => $updated_comment,
					"log" => $new_log
				);
				$db->upd("configurations",$search_path[0]['id'],$updated_path);
				
			} 
		} else {
			$log = "User: ".$user." created value: ".$val." at: ".$str_date;
			$new_path = array(
				"path"=>$path,
				"value"=>$val,
				"comment"=>$comment,
				"log"=>$log
			);
			$db->insert("configurations",$new_path);
		}
		setConfig($path,$val);
	}
}