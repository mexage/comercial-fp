<?php
function encode_html_mail_message($message){
	$message=iconv("UTF-8", "ISO-8859-1//TRANSLIT", $message);
	$max_width=998;
	$result="";
	$char_end=0;
	$char_start=0;
	$col=0;
	while($char_start<=strlen($message)){
		$char_end=$char_start+$max_width;
		while($char_end>$char_start && strcasecmp(substr($message,$char_end,1),">")!=0){
			$char_end--;
		}
		if($char_end==$char_start){
			$char_end=min($char_start+$max_width,strlen($message)+1);
		}
		$result.=substr($message,$char_start,$char_end-$char_start+1)."\r\n";
		$char_start=$char_end+1;
	}
	
	return $result;
}