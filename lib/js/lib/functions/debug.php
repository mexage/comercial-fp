<?php
function trace($msg){
	if(getConfig("DEBUG/LOG/ENABLED")){
		/*if(getConfig("DEBUG/LOG/SESSIONS")){
			if(array_search(session_id(),getConfig("DEBUG/LOG/SESSIONS"))===FALSE){
				return;
			}
		}*/
		if(strcasecmp($_GET['c'],"bins")!=0){
			return;
		}
		if(!file_exists(getConfig("DEBUG/LOG/PATH"))){
			mkdir(getConfig("DEBUG/LOG/PATH"),0777,true);
		}
		$log_file=getConfig("DEBUG/LOG/PATH").date("YmdH")."_trace.log";
		$stack=debug_backtrace();
		
		file_put_contents($log_file,session_id()." ".basename($stack[1]["file"]).":".$stack[1]["line"]." ".$stack[1]["function"]."(".$parametros."); // ". $msg." | ".date("r")."\n",FILE_APPEND);		
		file_put_contents($log_file,stack_summary($stack,2)."\n",FILE_APPEND);
		
	}
}
function stack_summary($stack,$from=1){
	$result = "";
	for($l = $from; $l<getConfig("DEBUG/LOG/STACK/LEVEL")+$from;$l++){
		if(count($stack[$l]["args"])){
			$parametros=implode(",",$stack[$l]["args"]);
		}else{
			$parametros="";
		}
		if(isset($stack[$l])){
			//basename
			$result.=str_repeat("\t",$l).($stack[$l]["file"]).":".$stack[$l]["line"]." ".$stack[$l]["function"]."(".$parametros.");\n";
		}else{
			break;
		}
		
	}
	if(count($stack)>getConfig("DEBUG/LOG/STACK/LEVEL")+$from){
		$result.="\n... Total ". count($stack)-$from;
	}
	return $result;
}