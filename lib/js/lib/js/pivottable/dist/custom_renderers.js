(function() {
  var callWithJQuery;

  callWithJQuery = function(pivotModule) {
    if (typeof exports === "object" && typeof module === "object") {
      return pivotModule(require("jquery"));
    } else if (typeof define === "function" && define.amd) {
      return define(["jquery"], pivotModule);
    } else {
      return pivotModule(jQuery);
    }
  };
  
  callWithJQuery(function($) {
    return $.pivotUtilities.custom_renderers = {
      "Vertical Slicer": function(pivotData, opts) {
        var agg, colAttrs, colKey, colKeys, defaults, i, j, k, l, len, len1, len2, len3, len4, len5, m, n, r, result, row, rowAttr, rowAttrs, rowKey, rowKeys, text;
        defaults = {
          localeStrings: {}
        };
        opts = $.extend(true, {}, defaults, opts);
        if(opts.id){			
			$("#"+opts.id+" .pvtRows").hide();
			$("#"+opts.id+" .pvtVals").hide();
			$("#"+opts.id+" .pvtCols").hide();
		}
		
        return "";
		},
	"Horizontal Slicer": function(pivotData, opts) {
        var agg, colAttrs, colKey, colKeys, defaults, i, j, k, l, len, len1, len2, len3, len4, len5, m, n, r, result, row, rowAttr, rowAttrs, rowKey, rowKeys, text;
        defaults = {
          localeStrings: {}
        };
        opts = $.extend(true, {}, defaults, opts);
        if(opts.id){			
			$("#"+opts.id+" .pvtRows").hide();
			$("#"+opts.id+" .pvtVals").hide();
			$("#"+opts.id+" .pvtCols").hide();
			$("#"+opts.id+" .pvtUnused li").css('display','inline');
			
		}
		
        return "";
		},
	"JSON": function(pivotData, opts) {
        var agg, colAttrs, colKey, colKeys, defaults, i, j, k, l, len, len1, len2, len3, len4, len5, m, n, r, result, row, rowAttr, rowAttrs, rowKey, rowKeys, text;
        defaults = {
          localeStrings: {}
        };
        opts = $.extend(true, {}, defaults, opts);
        rowKeys = pivotData.getRowKeys();
        if (rowKeys.length === 0) {
          rowKeys.push([]);
        }
        colKeys = pivotData.getColKeys();
        if (colKeys.length === 0) {
          colKeys.push([]);
        }
        rowAttrs = pivotData.rowAttrs;
        colAttrs = pivotData.colAttrs;
        result = [];
        row = [];
        for (i = 0, len = rowAttrs.length; i < len; i++) {
          rowAttr = rowAttrs[i];
          row.push(rowAttr);
        }
        if (colKeys.length === 1 && colKeys[0].length === 0) {
          row.push(pivotData.aggregatorName);
        } else {
          for (j = 0, len1 = colKeys.length; j < len1; j++) {
            colKey = colKeys[j];
            row.push(colKey.join("-"));
          }
        }
        result.push(row);
        for (k = 0, len2 = rowKeys.length; k < len2; k++) {
          rowKey = rowKeys[k];
          row = [];
          for (l = 0, len3 = rowKey.length; l < len3; l++) {
            r = rowKey[l];
            row.push(r);
          }
          for (m = 0, len4 = colKeys.length; m < len4; m++) {
            colKey = colKeys[m];
            agg = pivotData.getAggregator(rowKey, colKey);
            if (agg.value() != null) {
              row.push(agg.value());
            } else {
              row.push("");
            }
          }
          result.push(row);
        }
        text = JSON.stringify(result);
		if(opts.data_var==undefined){
			return $("<textarea>").text(text).css({
			  width: ($(window).width() / 2) + "px",
			  height: ($(window).height() / 2) + "px"
			});
		}else{
			eval(opts.data_var+"="+text);
			return "";
		}
		},	
	"Custom Text":function(pivotData, opts) {
        var agg, colAttrs, colKey, colKeys, defaults, i, j, k, l, len, len1, len2, len3, len4, len5, m, n, r, result, row, rowAttr, rowAttrs, rowKey, rowKeys, text;
        defaults = {
          localeStrings: {},
		  conditionalFormat:function(value,type,row,column,data){return "";}
        };
        opts = $.extend(true, {}, defaults, opts);
        rowKeys = pivotData.getRowKeys();
        if (rowKeys.length === 0) {
          rowKeys.push([]);
        }
        colKeys = pivotData.getColKeys();
        if (colKeys.length === 0) {
          colKeys.push([]);
        }
        rowAttrs = pivotData.rowAttrs;
        colAttrs = pivotData.colAttrs;
        result = [];
        row = [];
        for (i = 0, len = rowAttrs.length; i < len; i++) {
          rowAttr = rowAttrs[i];
          row.push(rowAttr);
        }
        if (colKeys.length === 1 && colKeys[0].length === 0) {
          row.push(pivotData.aggregatorName);
        } else {
          for (j = 0, len1 = colKeys.length; j < len1; j++) {
            colKey = colKeys[j];
            row.push(colKey.join("-"));
          }
        }
        result.push(row);
        for (k = 0, len2 = rowKeys.length; k < len2; k++) {
          rowKey = rowKeys[k];
          row = [];
          for (l = 0, len3 = rowKey.length; l < len3; l++) {
            r = rowKey[l];
            row.push(r);
          }
          for (m = 0, len4 = colKeys.length; m < len4; m++) {
            colKey = colKeys[m];
            agg = pivotData.getAggregator(rowKey, colKey);
            if (agg.value() != null) {
              row.push(agg.value());
            } else {
              row.push("");
            }
          }
          result.push(row);
        }
        
		
		text= "";
		// Header
		r = result[0];
		text+="<div class='pivot_custom header_row'>";
		for(c in r){
			var orig = {
				"text":r[c]
			};
        	var loaded= $.extend(true, {}, orig, opts.conditionalFormat(r[c],"header",0,r[c],r));
			loaded["class"]+=" pivot_custom header "+result[0][c].replace(/[^0-9a-z]/gi, '_');
			text+=$("<div>",loaded)[0].outerHTML;
			//text+="<div class='pivot_custom header "+result[0][c].replace(/[^0-9a-z]/gi, '_')+"' style='"+opts.conditionalFormat(r[c],"header",0,r[c],r)+"'>"+r[c]+"</div>";
		}
		text+="</div>\n";
		
		// Data
		for (n = 1, len5 = result.length; n < len5; n++) {
			r = result[n];
			text+="<div class='pivot_custom row "+((n&1)?"odd ":"even ")+((n==1)?"first ":"")+((n==len5-1)?"last ":"")+"'>";
			for(c in r){
				var o ={};
				for(co in result[0]){
					o[result[0][co].replace(/[^0-9a-z]/gi, '_')]=r[co];
				}
				var orig = {
					"text":r[c],
				};
				var loaded= $.extend(true, {}, orig, opts.conditionalFormat(r[c],"data",n,result[0][c].replace(/[^0-9a-z]/gi, '_'),o));
				loaded["class"]+=" pivot_custom column "+result[0][c].replace(/[^0-9a-z]/gi, '_');
				var res=$("<div>",loaded)[0].outerHTML;
				text+=res;
				//text+="<div class='pivot_custom column "+result[0][c].replace(/[^0-9a-z]/gi, '_')+"' style='"+opts.conditionalFormat(r[c],"data",n,result[0][c].replace(/[^0-9a-z]/gi, '_'),o)+"'>"+r[c]+"</div>";
			}
			text+="</div>\n";
		
		}
		
        return text;
		 //$("<textarea>").text(text).css({
          //width: ($(window).width() / 2) + "px",
          //height: ($(window).height() / 2) + "px"
        //});
      }
    };
  });

}).call(this);
