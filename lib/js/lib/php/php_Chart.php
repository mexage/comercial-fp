<?php
class php_Chart{
	public static function div($id){
		$r="<div id='".$id."'>Loading...<br /><img src='images/bins/loader.gif' /></div>";
		return $r;
	}
	public static function ajax($url,$code){
		$r='<!-- php_Chart::ajax('.$url.') -->
		<script>
		$.ajax({
			url:"'.$url.'",
			success:function(d){
				try{
					var ajax_data=JSON.parse(d);
				}catch(e){
					prompt(e.toString(),d);
				}
				'.$code.'
			}	
			
		});
		</script>';
		
		return $r;
	}
	public static function javascript($id,$cnf,$ui=false,$var="ajax_data"){
		$r='
			/**************************************************************
				php_Chart::javascript('.$id.'); UI: '.$ui.' VAR:'.$var.'
			***************************************************************/
			var temp_chart_function=function(ajax){
				var data;
				var cnf;
				try{
					data='.$var.';
					cnf='.$cnf.';
					
					$.extend($.pivotUtilities.renderers,$.pivotUtilities.subtotal_renderers,
						$.pivotUtilities.c3_renderers,$.pivotUtilities.d3_renderers,$.pivotUtilities.custom_renderers,$.pivotUtilities.export_renderers);
					cnf.dataClass=$.pivotUtilities.SubtotalPivotData;
					cnf.aggregator = $.pivotUtilities.aggregators[cnf.aggregatorName](cnf.vals);
					';
		
			if($ui){
				$r.= '$("#'.$id.'").pivotUI(data,cnf,true);'."\n";
				$r.= 'var margin = $($("#'.$id.' td.pvtUnused")[0]).width()+$($("#'.$id.' td.pvtRows")[0]).width();';
			}else{
				$r.= 'cnf.renderer=$.pivotUtilities.renderers[cnf.rendererName];'."\n";
				$r.= 'cnf.filter=function(record){
						var filterItemExcluded = false;
		
						for(attr in record){
						  var value = record[attr];
						  if(cnf.exclusions){
							  if (cnf.exclusions[attr]) {
								filterItemExcluded = (cnf.exclusions[attr].indexOf(value)>= 0);
							  }
						  }
						  if(cnf.inclusions){
							  if (cnf.inclusions[attr]) {
								filterItemExcluded = (cnf.inclusions[attr].indexOf(value)==-1);
							  } 
						  }
						  if(filterItemExcluded)return false;
						}
						return !filterItemExcluded;
					};
					';
				$r.= '$("#'.$id.'").pivot(data,cnf,true);'."\n";
				$r.= 'var margin=0;';
			}
		$r.='
					
					if($("#'.$id.'").text()=="An error occurred rendering the PivotTable UI."){
						$("#'.$id.'").addClass("alert");
						$("#'.$id.'").addClass("alert-danger");
					}else{
						setInterval(function(){
							if($("#'.$id.' div.c3").data("chart")){
								$("#'.$id.' div.c3").data("chart").resize({width:($("#'.$id.'").width()-margin)*.9});
							}
						},1000);
						
					}
					
				}catch(e){
					$("#'.$id.'").addClass("alert");
					$("#'.$id.'").addClass("alert-danger");
					
					$("#'.$id.'").html("<h3>Data source error: " + e.toString()+"</h3><h3>Dump:</h3><pre>"+data+"</pre>");
				}
			}(ajax_data);';
		return $r;
	}
}
