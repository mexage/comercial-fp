<?php
class php_Field{
	static function text($name,$value="",$label_type=1,$css_class="",$id="",$settings=array("type"=>"text")){
		if(!$id){
			$id=strtolower(str_replace(" ","_",$name));
		}
		if(strcasecmp($settings['type'],"hidden")==0){
			$label_type=0;
		}
		if(strcasecmp($settings['type'],"textarea")!=0){
			$open ="<input value='".$value."' ";
			$close="/>";
		}else{
			$open ="<textarea ";
			$close =">".$value."</textarea>";
		}
		
		$input=$open." type='".$settings['type']."' name='".strtolower(str_replace(" ","_",$name))."' class='form-control ".$css_class."' id='".$id."' ".$close;
		if(strcasecmp($settings['type'],'readonly')==0){
			$input = "<span  name='".strtolower(str_replace(" ","_",$name))."' class='form-control ".$css_class."' id='".$id."'>".$value."</span>";
		}
		switch($label_type){
			case 1:{
				return "<div class='form-group'><label for='".$id."'> ".$name." </label>".$input."</div>";
			}break;
			case 2:{
				return "<label for='".$id."'> ".$name.$input." </label>";
			}break;
			default:{
				return $input;
			}
		}
		
	}
	static function textarea($name,$value,$label_type=1,$css_class="",$id="",$settings=array("type"=>"textarea")){
		return self::text($name,$value,$label_type,$css_type,$id,$settings);
	}
	static function hidden($name,$value){
		return php_Field::text($name,$value,0,"","",array("type"=>"hidden"));
	}
	static function submit($name,$value="Submit",$css_class=""){
		return "<input class='".$css_class."' type='submit' name='".$name."' value='".$value."' />";
	}
	
	static function dropdown($name,$list,$value="",$label_type=1,$css_class="",$id=""){
		if(!$id){
			$id=strtolower(str_replace(" ","_",$name));
		}
		$select="<select name='".strtolower(str_replace(" ","_",$name))."' class='form-control ".$css_class."' id='".$id."' >";
		if($list){
			
			foreach($list as $r){
				$selected="";
				if($r['id']==$value){
					$selected="selected='true'";
				}
				$select.="<option value='".$r['id']."' ".$selected.">".$r['name']." (".$r['id'].")"."</option>";
			}
		}
		$select.="</select>";
		/*else{
			$assoc=array_keys($arr) !== range(0, count($arr) - 1);
		}*/
		switch($label_type){
			case 1:{
				return "<div class='form-group'><label for='".$id."'>".$name." </label> ".$select."</div>";
			}break;
			case 2:{
				return "<label for='".$id."'>".$name.$select." </label>";
			}break;
			default:{
				return $select;
			}
		}
	}
	static function start_form($class,$method,$get_parameters=array(),$http_method="POST"){
		$parameters="";
		if(count($get_parameters)){
			
			$parr=array();
			foreach($get_parameters as $k=>$v){
				$parr[]=$k."=".urlencode($v);
			}
			$parameters="&".implode("&",$parr);
		}
		return "<form action='?c=".$class."&m=".$method.$parameters."' method='".$http_method."'>";
		
	}
	static function end_form(){
		return "</form>";
	}
}

?>