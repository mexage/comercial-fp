<?php

class php_Auth{

	function __const(){
	}
	public static function acl_read($field){
		$acl = get_session();
		if(!$acl){
			return null;
		}else{
			return $acl['acl'][$field];
		}
	}
	
	public static function is_logged(){
		return isset($_SESSION['bins']['user']);
	}
	
	public static function add_user($user){
		global $db;
		
		$users=self::get_user($user);		
		if(count($users)){		
			return true;
		}else{
			return false;
		}
	}
	public static function set_acl($user_id,$acl){
		global $db;
		
		return $db->upd("bin_acl",$user_id,$acl);
	}
	public static function clean_user($user){
		$user=trim($user);
		if(is_numeric($user)){
			$user=ltrim($user,"0");
		}
		return addslashes($user);
	}
	public static function get_user($user){
		global $db;
		$users=$db->query("SELECT * FROM bin_acl WHERE username='".self::clean_user($user)."'");
		if(!count($users)){
			$users=array($db->insert("bin_acl",array("username"=>self::clean_user($user),"save"=>1,"login"=>1)));
		}
		return $users[0];
	}
	public static function is_proxy($proxy){
		return strcasecmp(trim($_SERVER['REMOTE_ADDR']),$proxy)==0;
	}
	public static function set_session($session_arr){
		$_SESSION['bins']=$session_arr;
	}
	public static function get_session(){
		if(isset($_SESSION['bins'])){
			return $_SESSION['bins'];
		}else{
			return false;
		}
	}
	public static function logout(){
		unset($_SESSION['bins']);
	}
	public static function login($user,$pwd){
		
		if(self::is_logged()){
			return true;
		}
		$ad_validated=false;
		
		if(is_numeric(trim($user))){
			$user=ltrim(trim($user),"0");
		}else{
			if(php_AD::validate_login($user,$pwd)){
				$session = array();
				$session['user']=trim($user);
				$session['acl']=self::get_user($user);
				
				if(!$session['acl']){
					self::create_user($user);
				}
				
				if(self::is_proxy("10.13.0.44")){//doit.foxconn.com
					
				}
				
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
}

/*echo "<pre>";
echo "islogged =".php_Auth::is_logged()."\n";
echo "logout =".php_Auth::logout()."\n";
echo "islogged =".php_Auth::is_logged()."\n";
echo "clean_user 0123=".php_Auth::clean_user("0123")."\n";
echo "clean_user ' guillermo.morales='".php_Auth::clean_user(" guillermo.morales")."'\n";
echo "get_session ='".php_Auth::get_session()."'\n";
//echo "get_user ='".php_Auth::get_user("8506")."'\n";
echo "is_proxy 10.13.0.44 ='".php_Auth::is_proxy("10.13.0.44")."'\n";
echo "is_proxy  ='".php_Auth::is_proxy("")."'\n";
*/


?>