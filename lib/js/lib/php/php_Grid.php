<?php

class php_Grid{
	static function replace($string,$data){
		foreach($data as $k=>$v){
			$string=str_replace("<?=".$k."?>",$v,$string);
		}
		return $string;
	}
	static function draw($data,$rowlinks=array(),$hide_fields=array()){
		global $db;
		if(!$data || !is_array($data[0])){
			echo "<table class='table table-striped'><tbody><tr><td>No data.</td></tr></tbody></table>";
			return;
		}
		echo "<table class='table table-striped'>";
		$keys=array_keys($data[0]);
		echo "<thead><tr>";
		foreach($keys as $k){
			
			if(array_search($k,$hide_fields)===false){
				echo "<th>".ucwords(str_replace("_"," ",$k))."</th>";
			}
		}
		if(count($rowlinks)){
			echo "<th>Actions</th>";
		}
		echo "</tr></thead><tbody>";
		foreach($data as $r){
			echo "<tr>";
			foreach($keys as $k){
				if(array_search($k,$hide_fields)===false){
					$d=$r[$k];
					if($table=$db->ftable($k)){
						$d=$db->get($table,$d);
						echo "<td><a class='btn btn-info' href='?c=".$table."&m=edit&id=".$d['id']."'> ".$d['name']." (".$d['id'].")</a></td>";
					}else{
						echo "<td>".$d."</td>";
					}
				}
			}
			if(count($rowlinks)){
				echo '<td><div class="btn-group">';
				foreach($rowlinks as $t=>$u){
					echo "<a class='btn btn-danger' href='".php_Grid::replace($u,$r)."'>".$t."</a>";
				}
				echo '</td>';
			}
			echo "</tr>";
		}
		
		echo "</tbody></table>";
	}
}