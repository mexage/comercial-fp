<?php

class php_mysqli{
	public $dbc;
	
	function php_mysqli($conn){
		$this->dbc=$conn;
	}
	function execute($q){
		trace("Execute ".$q);
		if(!$r=mysqli_query($this->dbc,$q)){
			trace("MySQLi error:".mysqli_error($this->dbc));
		}else{
			trace("Finished");
		}
		return $r;
	}
	function insert($table,$row){
		foreach($row as &$c){
			$c=addslashes($c);
		}
		$q="INSERT INTO ".$table."(".implode(",",array_keys($row)).") VALUES ('".implode("','",$row)."')";
		$this->execute($q);
		return $this->get($table,mysqli_insert_id($this->dbc));
	}
	function del($table,$id){
		$this->execute("DELETE FROM ".$table." WHERE id='".$id."'");
	}
	function upd($table,$id,$row){
		$q="UPDATE ".$table." SET ";
		$fields=array();
		foreach($row as $k=>$v){
			$fields[]=$k."='".addslashes($v)."'";
		}
		$q.=implode(",",$fields);
		$q.=" WHERE id='".addslashes($id)."'";
		
		$this->execute($q);
		
		return $this->get($table,$id);
	}
	function _batch($table,$filter,$change){
		$q="UPDATE ".$table." SET ".$change;
		
		$q.=" WHERE ".$filter."'";
		return $this->execute($q);
	}
	function my($parent,$children,$id,$filter="true",$child_field=null,$parent_field="id"){
		if($child_field==null){
			$child_field=$parent."_id";
		}
		
		$filter=$parent.".id='".addslashes($id)."' AND (".$filter.")";
		$q="SELECT ".$children.".* FROM ". $parent . " INNER JOIN ". $children ." ON ".$parent.".".$parent_field."=".$children.".".$child_field." WHERE ".$filter.";";
		$r=$this->query($q);
		return $r;
	}
	function ftable($field){
		// split fields like asignado_a__empleados_id and get last part
		$parts=explode("__",$field);				
		$last=$parts[count($parts)-1];
		// detect _id
		if(strcasecmp(substr($last,strlen($last)-3,3),"_id")==0){
			
			$table=substr($last,0,strlen($last)-3);
		}else{
			$table=false;
		}
		return $table;
	}
	function fill(&$data,$field=null,$type="TABLE"){
		if($data==NULL)return;
		if(strcasecmp($type,"TABLE")==0){
			$debug=getConfig("DEBUG/LOG/ENABLED");
			if($debug)trace("Temporarily suppressing fill queries logs for performance.");
			setConfig("DEBUG/LOG/ENABLED",false);
			foreach($data as &$r){
				$this->fill($r,$field,"ROW");
			}
			setConfig("DEBUG/LOG/ENABLED",$debug);
			
		}
		if(strcasecmp($type,"ROW")==0){
			//if(is_array($data)){
				foreach($data as $k=>&$r){
					/*// split fields like asignado_a__empleados_id and get last part
					$parts=explode("__",$k);				
					$last=$parts[count($parts)-1];
					// detect _id
					if(strcasecmp(substr($last,strlen($last)-3,3),"_id")==0){
						
						$table=substr($last,0,strlen($last)-3);
						$data[$table]=$this->get($table,$r);					
					}*/				
					if($table=$this->ftable($k)){
						if(!$field){
							$data[$table]=$this->get($table,$r);
						}else{
							$o=$this->get($table,$r);
							$data[$table]=$o[$field];
						}
					}
					
				}
			//}
		}
	}
	
	function extend(&$data,$model,$fields=array("name"),$type="TABLE"){
		if($data==NULL || !is_array($data)){
			trace("ERROR INVAID DATA for extend: '".$data."'");
			return;
		}
		
		if(strcasecmp($type,"TABLE")==0){
			$debug=getConfig("DEBUG/LOG/ENABLED");
			if($debug)trace("Temporarily suppressing extend queries logs for performance.");
			setConfig("DEBUG/LOG/ENABLED",false);
			foreach($data as &$r){
				$this->extend($r,$model,$fields,"ROW");
			}
			setConfig("DEBUG/LOG/ENABLED",$debug);
			
		}
		if(strcasecmp($type,"ROW")==0){
			foreach($data as $k=>&$r){
				/*// split fields like asignado_a__empleados_id and get last part
				$parts=explode("__",$k);				
				$last=$parts[count($parts)-1];
				// detect _id
				if(strcasecmp(substr($last,strlen($last)-3,3),"_id")==0){
					
					$table=substr($last,0,strlen($last)-3);
					$data[$table]=$this->get($table,$r);					
				}*/				
				if($table=$this->ftable($k)){
					if($table==$model){
						$o=$this->get($table,$r);
						$fpp=explode("__",$k);
						if(count($fpp)>1){
							$pref=$fpp[0]."__";
						}else{
							$pref="";
						}
						foreach($fields as $f){
							$data[$pref.$model."__".$f]=$o[$f];
						}
					}
				}
				
			}
		}
	}
	
	function set($table,$id,$field,$value){
		return $this->upd($table,$id,array($field=>$value));
	}
	function get($table,$id,$field=null){
		$q="SELECT * FROM ". $table . " WHERE id='".addslashes($id)."';";
		$r=$this->query($q);
		if($field){
			return $r[0][$field];
		}else{
			return $r[0];
		}
	}
	function query($q){
		trace("Query ".$q);
		if(!$r=mysqli_query($this->dbc,$q)){
			trace("MySQLi error:".mysqli_error($this->dbc));
			return null;
		}
		$result=array();
		while($row=mysqli_fetch_assoc($r)){
			$result[]=$row;
		}
		trace("Fetched ".count($result)." row(s).");
		return $result;
	}
	function search($table,$criteria="",$sort="",$limit=5000){
		if($sort){
			$sort=" ORDER BY ".$sort;
		}
		if(!$criteria){
			return $this->query("SELECT * FROM ".$table." ".$sort." LIMIT ".$limit);	
		}else{
			return $this->query("SELECT * FROM ".$table." WHERE ".$criteria." ".$sort." LIMIT ".$limit);
		}
	}
	

	function hash($table,$key="id",$filter="1"){
		trace("Hash ".$table."[".$key."] WHERE ".$filter);
		if(!$r=mysqli_query($this->dbc,"SELECT * FROM ".$table." WHERE ".$filter)){
			trace("MySQLi error:".mysqli_error($this->dbc));
			return null;
		}
		$result=array();
		while($row=mysqli_fetch_assoc($r)){
			$result[$row[$key]]=$row;
		}
		if(count($result)<20){
			trace("Hash keys:[".implode(",",array_keys($result))."]");
		}else{
			trace("Hash keys:[".count($result)." values.]");
		}
		
		return $result;
	}
	function describe($table){	
		$my_db=$this->query("SELECT database() AS d");
		return $this->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='".$my_db[0]["d"]."' AND TABLE_NAME='".$table."';");
		/*$d=$this->query("DESCRIBE ".$table);
		foreach($d as &$r){
			$p=explode("(",$r["Type"]);
			if(count($p)==1){
				$s="";
			}else{
				$r["Type"]=$p[0];
				$s=substr($p[1],0,strlen($p[1])-1);
			}
			$r["Size"]=$s;
		}
		return $d;*/
	}
	function external_links($table){
		$my_db=$this->query("SELECT database() AS d");
		return $this->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='".$my_db[0]["d"]."' AND COLUMN_NAME LIKE '%".$table."_id' GROUP BY TABLE_NAME, COLUMN_NAME;");		
	}
	function tables(){
		$my_db=$this->query("SELECT database() AS d");
		return $this->query("SELECT * FROM information_schema.tables WHERE TABLE_SCHEMA='".$my_db[0]["d"]."';");
		
	}
	static function phpdate($str){
		if(strcmp($str,"0000-00-00 00:00:00")==0
			||strcmp($str,"0000-00-00")==0
			||strcmp($str,"00:00:00")==0){
			return 0;
		}
		$has_time=strpos($str,":")!==false;
		$has_date=strpos($str,"-")!==false;
		
		if($has_time && $has_date){
			$parts=explode(" ",$str);
			$d=explode("-",$parts[0]);
			$t=explode(":",$parts[1]);			
		}else if($has_time){
			$d=array(0,0,0);
			$t=explode(":",$str);			
		}else if($has_date){
			$d=explode("-",$str);
			$t=array(0,0,0);
		}else{
			return 0;
		}
		if(count($t)!=3 || count($d)!=3){
			return 0;
		}
		return mktime($t[0],$t[1],$t[2],$d[1],$d[2],$d[0]);
		
		
	}
	function datediff(&$table,$name,$field1,$field2,$unit="d",$integers=true){
		$format=array(
			"d"=>1/60/60/24,
			"h"=>1/60/60,
			"m"=>1/60,
			"s"=>1
			
		);
		$factor=$format[$unit];
		foreach($table as $k=>$r){
			
			$t1=self::phpdate($r[$field1]);
			$t2=self::phpdate($r[$field2]);
			if($t1<=0 || $t2<=0){
				$d="";
			}else{
				$d=($t1-$t2)*$factor;
				if($integers){
					$d=floor($d);
				}
			}
			$table[$k][$name]=$d;
		}
	}
	function datesplit(&$table,$field,$types){
		foreach($table as $k=>$r){
			foreach($types as $t=>$f){
				$table[$k][$field."__".$t]=date($f,self::phpdate($r[$field]));
			}
		}
	}
	
}