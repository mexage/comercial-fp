MXFunctions=new (function(){
	var self=this;
	this.consolidateOpts=function(opts){
		if($.isArray(opts)){
			var o = {};
			for(var e in opts){
				o=$.extend(true,{},o, opts[e]);
			}
			opts=o;
		}		
		return opts;
	};
	this.COUNTIFS=function(data,opts,filter){
		var c=0;
		opts=this.consolidateOpts(opts);
		for(var r in data){
			if(self.isMatchingRecord(data[r],opts)){
				if(filter){
					if(!filter(data[r])){
						continue;
					}
				}
				c++;
			}
		}
		return c;
	};
	this.SUMIFS=function(data,sum_col,opts,filter){
		var s=0;
		opts=this.consolidateOpts(opts);
		for(var r in data){
			if(this.isMatchingRecord(data[r],opts)){
				if(filter){
					if(!filter(data[r])){
						continue;
					}
				}
				s+=(data[r][sum_col]-0);
			}
		}
		return s;
	};
	this.values=function(data,field,in_array){
		var result={};
		var result_arr=[];
		for(var r in data){
			if(!result[data[r][field]]){
				result[data[r][field]]=1;
			}else{
				result[data[r][field]]++;
			}
		}
		if(in_array){
			for(var r in result){
				result_arr.push(r);
			}
			result=result_arr;
		}
		return result;
	};
	this.eachMatching=function(data,opts,filter,aggrFunc){
		var x;
		opts=this.consolidateOpts(opts);
		for(var r in data){
			if(this.isMatchingRecord(data[r],opts)){
				if(filter){
					if(!filter(data[r])){
						continue;
					}
				}
				x=aggrFunc(x,data[r],opts);
			}
		}
		return x;
	};
	this.isMatchingRecord=function(record,opts){
		var filterItemExcluded = false;
		
		for(attr in record){
		  var value = record[attr];
		  if(opts.exclusions){
			  if (opts.exclusions[attr]) {
				filterItemExcluded = (opts.exclusions[attr].indexOf(value)>= 0);
			  }
		  }
		  if(opts.inclusions){
			  if (opts.inclusions[attr]) {
				filterItemExcluded = (opts.inclusions[attr].indexOf(value)==-1);
			  } 
		  }
		  if(filterItemExcluded)return false;
		}
		return !filterItemExcluded;
	};

});