Attribute VB_Name = "DoIT"
Option Explicit

Private Const INTERNET_AUTODIAL_FORCE_ONLINE As Long = 1
Private Const INTERNET_OPEN_TYPE_PRECONFIG  As Long = 0
Private Const INTERNET_DEFAULT_HTTP_PORT    As Long = 80
Private Const INTERNET_SERVICE_HTTP         As Long = 3
Private Const INTERNET_FLAG_RELOAD          As Long = &H80000000
Private Const HTTP_ADDREQ_FLAG_REPLACE      As Long = &H80000000
Private Const HTTP_ADDREQ_FLAG_ADD          As Long = &H20000000
 
Private Declare PtrSafe Function InternetAutodial Lib "wininet.dll" (ByVal dwFlags As Long, ByVal dwReserved As Long) As Long
Private Declare PtrSafe Function InternetOpen Lib "wininet.dll" Alias "InternetOpenA" (ByVal sAgent As String, ByVal lAccessType As Long, ByVal sProxyName As String, ByVal sProxyBypass As String, ByVal lFlags As Long) As Long
Private Declare PtrSafe Function InternetConnect Lib "wininet.dll" Alias "InternetConnectA" (ByVal hInternetSession As Long, ByVal sServerName As String, ByVal nServerPort As Integer, ByVal sUsername As String, ByVal sPassword As String, ByVal lService As Long, ByVal lFlags As Long, ByVal lContext As Long) As Long
Private Declare PtrSafe Function HttpOpenRequest Lib "wininet.dll" Alias "HttpOpenRequestA" (ByVal hHttpSession As Long, ByVal sVerb As String, ByVal sObjectName As String, ByVal sVersion As String, ByVal sReferer As String, ByVal something As Long, ByVal lFlags As Long, ByVal lContext As Long) As Long
Private Declare PtrSafe Function HttpAddRequestHeaders Lib "wininet.dll" Alias "HttpAddRequestHeadersA" (ByVal hHttpRequest As Long, ByVal sHeaders As String, ByVal lHeadersLength As Long, ByVal lModifiers As Long) As Long
Private Declare PtrSafe Function HttpSendRequest Lib "wininet.dll" Alias "HttpSendRequestA" (ByVal hHttpRequest As Long, ByVal sHeaders As String, ByVal lHeadersLength As Long, ByVal sOptional As String, ByVal lOptionalLength As Long) As Long
Private Declare PtrSafe Function InternetCloseHandle Lib "wininet.dll" (ByVal hInet As Long) As Long
 
Public Function DoIT_PostFile(sUrl As String, sFileName As String, id As String) As Boolean
    Const STR_APP_NAME  As String = "Uploader"
    Dim hOpen           As Long
    Dim hConnection     As Long
    Dim hRequest        As Long
    Dim sHeader         As String
    Dim sBoundary       As String
    Dim nFile           As Integer
    Dim baData()        As Byte
    Dim sPostData       As String
    Dim sHttpServer     As String
    Dim lHttpPort       As Long
    Dim sUploadPage     As String
 
    '--- read file
    nFile = FreeFile
    Open sFileName For Binary Access Read As nFile
    If LOF(nFile) > 0 Then
        ReDim baData(0 To LOF(nFile) - 1) As Byte
        Get nFile, , baData
        sPostData = StrConv(baData, vbUnicode)
    End If
    Close nFile
    '--- parse url
    sHttpServer = sUrl
    If InStr(sHttpServer, "://") > 0 Then
        sHttpServer = Mid$(sHttpServer, InStr(sHttpServer, "://") + 3)
    End If
    If InStr(sHttpServer, "/") > 0 Then
        sUploadPage = Mid$(sHttpServer, InStr(sHttpServer, "/"))
        sHttpServer = Left$(sHttpServer, InStr(sHttpServer, "/") - 1)
    End If
    If InStr(sHttpServer, ":") > 0 Then
        On Error Resume Next
        lHttpPort = CLng(Mid$(sHttpServer, InStr(sHttpServer, ":") + 1))
        On Error GoTo 0
        sHttpServer = Left$(sHttpServer, InStr(sHttpServer, ":") - 1)
    End If
    '--- prepare request
    If InternetAutodial(INTERNET_AUTODIAL_FORCE_ONLINE, 0) = 0 Then
        GoTo QH
    End If
    hOpen = InternetOpen(STR_APP_NAME, INTERNET_OPEN_TYPE_PRECONFIG, vbNullString, vbNullString, 0)
    If hOpen = 0 Then
        GoTo QH
    End If
    hConnection = InternetConnect(hOpen, sHttpServer, IIf(lHttpPort <> 0, lHttpPort, INTERNET_DEFAULT_HTTP_PORT), vbNullString, vbNullString, INTERNET_SERVICE_HTTP, 0, 0)
    If hConnection = 0 Then
        GoTo QH
    End If
    hRequest = HttpOpenRequest(hConnection, "POST", sUploadPage, "HTTP/1.0", vbNullString, 0, INTERNET_FLAG_RELOAD, 0)
    If hRequest = 0 Then
        GoTo QH
    End If
    '--- prepare headers
    sBoundary = "3fbd04f5-b1ed-4060-99b9-fca7ff59c113"
    sHeader = "Content-Type: multipart/form-data; boundary=" & sBoundary & vbCrLf
    If HttpAddRequestHeaders(hRequest, sHeader, Len(sHeader), HTTP_ADDREQ_FLAG_REPLACE Or HTTP_ADDREQ_FLAG_ADD) = 0 Then
        GoTo QH
    End If
    '--- post data
    sPostData = "--" & sBoundary & vbCrLf & _
        "Content-Disposition: multipart/form-data; name=""" & id & """; filename=""" & CleanFilename(Mid$(sFileName, InStrRev(sFileName, "\") + 1)) & """" & vbCrLf & _
        "Content-Type: application/octet-stream" & vbCrLf & vbCrLf & _
        sPostData & vbCrLf & _
        "--" & sBoundary & "--"
    If HttpSendRequest(hRequest, vbNullString, 0, sPostData, Len(sPostData)) = 0 Then
        GoTo QH
    End If
    '--- success
    DoIT_PostFile = True
QH:
    If hRequest <> 0 Then
        Call InternetCloseHandle(hRequest)
    End If
    If hConnection <> 0 Then
        Call InternetCloseHandle(hConnection)
    End If
    If hOpen <> 0 Then
        Call InternetCloseHandle(hOpen)
    End If
End Function


''''''''''''


Public Function DoIT_GetParams() As Variant
    Dim l
    Dim el
    l = Split(ActiveWorkbook.FullName, "\")
    el = Split(l(UBound(l)), ".")
    DoIT_GetParams = el
End Function
Public Function DoIT_Rows(DB As Range, Table As String) As Integer
    Dim SecType As Range
    Set SecType = DoIT_SectionType(DB, Table)
    DoIT_Rows = SecType(2, 1)
End Function

Public Function DoIT_Read(DB As Range, Table As String, Optional Variable As String, Optional Index As Integer) As Range
    Dim Data As Range
    Dim SecType As Range
    Dim row As Integer
    Dim col As Integer
    
    Set Data = GetSection(DB, Table)
    
    If Data Is Nothing Then
        Set DoIT_Read = Nothing
        Exit Function
    End If
    
    Set SecType = DoIT_SectionType(DB, Table)
    
    Select Case SecType
        Case "table"
            If IsNumeric(Variable) Then
                col = Val(Variable)
            Else
                col = Application.WorksheetFunction.Match(Variable, Range(Data(0, 1), Data(0, Data.Columns.Count)), 0)
            End If
            
            row = Index
            Set DoIT_Read = Data(row, col)
            Exit Function
        Case "structure"
            If IsNumeric(Variable) Then
                row = Val(Variable)
            Else
                row = Application.WorksheetFunction.Match(Variable, Range(Data(1, 1), Data(Data.rows.Count, 1)), 0)
            End If
            col = 2
            
            Set DoIT_Read = Data(row, col)
            Exit Function
        Case "value"
            Set DoIT_Read = Data(1, 1)
            Exit Function
    End Select
    
    Set DoIT_Read = Nothing
End Function

Public Function DoIT_SectionType(DB As Range, Table As String) As Range
    Dim Start As Range
    On Error Resume Next
    Set Start = DB.Worksheet.Cells.Find(What:="$result[" & Table & "]", LookIn:= _
        xlValues, LookAt:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext _
        , MatchCase:=True, SearchFormat:=False)
    On Error GoTo 0
    Set DoIT_SectionType = Start(2, 1)
End Function

Private Function GetSection(DB As Range, Table As String) As Range
    Dim Start As Range
    Dim rows As Integer
    Dim cols As Integer
    On Error GoTo not_found
    Set Start = DB.Worksheet.Cells.Find(What:="$result[" & Table & "]", LookIn:= _
        xlValues, LookAt:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext _
        , MatchCase:=True, SearchFormat:=False)
    On Error GoTo 0
    Select Case Start(2, 1)
        Case "table"
            rows = Start(3, 1)
            cols = Start(4, 1)
            Set GetSection = Range(Start(6, 1), Start(5 + rows, 1 + cols))
            Exit Function
        Case "structure"
            rows = Start(3, 1)
            Set GetSection = Range(Start(4, 1), Start(3 + rows, 2))
            Exit Function
        Case "value"
            Set GetSection = Start(3, 1)
            Exit Function
    End Select
not_found:
    Set GetSection = Nothing
End Function

Public Sub DoIT_GetData(ByVal URL As String, ByRef DB As Range)

    Dim id As Integer

    With DB.Worksheet.QueryTables.Add(Connection:= _
        "URL;" & URL, _
        Destination:=DB)
        '.CommandType = 0
        .Name = URL
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .BackgroundQuery = False
        .RefreshStyle = xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = True
        .RefreshPeriod = 0
        .WebSelectionType = xlEntirePage
        .WebFormatting = xlWebFormattingNone
        .WebPreFormattedTextToColumns = True
        .WebConsecutiveDelimitersAsOne = True
        .WebSingleBlockTextImport = False
        .WebDisableDateRecognition = False
        .WebDisableRedirections = False
        .Refresh BackgroundQuery:=False
    End With

End Sub

Private Function CleanFilename(filename As String) As String
    Dim valid As String
    Dim n As Integer
    Dim c As String
    valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz. 0123456789_"
    For n = 1 To Len(filename)
        c = Mid(filename, n, 1)
        If InStr(1, valid, c) > 0 Then
            CleanFilename = CleanFilename & c
        End If
    Next n
    
End Function


